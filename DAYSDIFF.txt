000200 IDENTIFICATION DIVISION.                                         00020025
000300 PROGRAM-ID. DAYSDIFF.                                            00030034
000400 AUTHOR.  DENNIS M REED.                                          00040025
000500 DATE-WRITTEN.  JAN 1993.                                         00050025
000600* MODIFIED FROM CODE WRITTEN BY:                                  00060025
000700*   HOLLY CARTWRIGHT - TIERRA SYSTEMS INC.                        00070025
000800*   AUG 1992.                                                     00080025
000900 DATE-COMPILED.                                                   00090025
001000*REMARKS.                                                         00100025
001100******************************************************************00110025
001200* INPUT PARMETERS:                                                00120074
001201*                                                                 00120174
001210*   1ST-CCYYMMDD-S OR 1ST-00YYMMDD-S FORMAT: PIC S9(08) COMP-3    00121075
001300*   2ND-CCYYMMDD-S OR 2ND-00YYMMDD-S FORMAT: PIC S9(08) COMP-3    00130075
001310*                                                                 00131074
001400* OUTPUT PARAMETER:                                               00140074
001410*                                                                 00141074
001420*   DIFF                         FORMAT: PIC S9(08)               00142074
001700*                                                                 00170035
001720* IF CC = 00 AND YY >  79, CC IS SET TO 19                        00172071
001730* IF CC = 00 AND YY <= 79, CC IS SET TO 20                        00173071
001731*                                                                 00173174
001732* DIFF = NUM-OF-DAYS(1ST-CCYYMMDD) - NUM-OF-DAYS(2ND-CCYYMMDD)    00173274
001740*                                                                 00174056
001800* NUM-OF-DAYS(CCYYMMDD) = ( CCYY * 365 ) + DDD                    00180074
001900*                         CALCULATED FROM THE CCYYDDD             00190074
002000*                         JULIAN DATE OF CCYYMMDD                 00200074
002013*                                                                 00201356
002020* NOTE:  THAT THE CCYY * 365 CALCULATION IS AN APPROXIMATION      00202074
002030*        WHICH DOES NOT TAKE LEAP YEARS INTO ACCOUNT.             00203074
002100******************************************************************00210025
002300 ENVIRONMENT DIVISION.                                            00230025
002400 CONFIGURATION SECTION.                                           00240025
002500 INPUT-OUTPUT SECTION.                                            00250025
002600 FILE-CONTROL.                                                    00260025
002700 DATA DIVISION.                                                   00270026
002800 WORKING-STORAGE SECTION.                                         00280025
002900 01  1ST-CCYYMMDD        PIC 9(08) VALUE 0.                       00290075
002910 01  FILLER REDEFINES 1ST-CCYYMMDD.                               00291075
003000     03  1ST-CC          PIC 9(02).                               00300075
003100     03  1ST-YY          PIC 9(02).                               00310075
003200     03  1ST-MM          PIC 9(02).                               00320075
003300     03  1ST-DD          PIC 9(02).                               00330075
003400 01  2ND-CCYYMMDD        PIC 9(08) VALUE 0.                       00340075
003410 01  FILLER REDEFINES 2ND-CCYYMMDD.                               00341075
003500     03  2ND-CC          PIC 9(02).                               00350075
003600     03  2ND-YY          PIC 9(02).                               00360075
003700     03  2ND-MM          PIC 9(02).                               00370075
003800     03  2ND-DD          PIC 9(02).                               00380075
003900 01  DIFF-9              PIC 9(08)- VALUE ZEROS.                  00390077
003931                                                                  00393144
003940 01  RESULT                     PIC 9(08) VALUE 0.                00394067
003941                                                                  00394146
003950 01  LEAP-YEAR-IND              PIC 9(01) VALUE 0.                00395050
003951                                                                  00395144
003960 01  MONTH-SUB                  PIC 99 VALUE 0.                   00396050
003970 01  DAYS-IN-MONTHS.                                              00397039
003980     03  JANUARY                PIC 9(02) VALUE 31.               00398039
003990     03  FEBRUARY               PIC 9(02) VALUE 00.               00399067
003991     03  MARCH                  PIC 9(02) VALUE 31.               00399139
003992     03  APRIL                  PIC 9(02) VALUE 30.               00399239
003993     03  MAY                    PIC 9(02) VALUE 31.               00399339
003994     03  JUNE                   PIC 9(02) VALUE 30.               00399439
003995     03  JULY                   PIC 9(02) VALUE 31.               00399539
003996     03  AUGUST                 PIC 9(02) VALUE 31.               00399639
003997     03  SEPTEMBER              PIC 9(02) VALUE 30.               00399739
003998     03  OCTOBER                PIC 9(02) VALUE 31.               00399839
003999     03  NOVEMBER               PIC 9(02) VALUE 30.               00399939
004000     03  DECEMBER               PIC 9(02) VALUE 31.               00400039
004001 01  FILLER REDEFINES DAYS-IN-MONTHS.                             00400146
004002     03  DAYS-IN-MONTH          PIC 9(02) OCCURS 12.              00400245
004010                                                                  00401025
004100 01  CALENDAR-CCYYMMDD           PIC 9(08) VALUE 0.               00410065
004110 01  FILLER REDEFINES CALENDAR-CCYYMMDD.                          00411065
004120     03  CALENDAR-CCYY           PIC 9(04).                       00412065
004130     03  FILLER REDEFINES CALENDAR-CCYY.                          00413065
004200         05  CALENDAR-CC         PIC 9(02).                       00420065
004300         05  CALENDAR-YY         PIC 9(02).                       00430065
004400     03  CALENDAR-MM             PIC 9(02).                       00440065
004500     03  CALENDAR-DD             PIC 9(02).                       00450065
004510                                                                  00451039
004600 01  JULIAN-CCYYDDD.                                              00460065
004610     03  JULIAN-CCYY             PIC 9(04) VALUE 0.               00461067
004620     03  FILLER REDEFINES JULIAN-CCYY.                            00462067
004700         05  JULIAN-CC           PIC 9(02).                       00470068
004900         05  JULIAN-YY           PIC 9(02).                       00490068
005000     03  JULIAN-DDD              PIC 9(03) VALUE 0.               00500067
005100 01  JULIAN-CCYYDDD-R REDEFINES JULIAN-CCYYDDD                    00510065
005200                                 PIC 9(07).                       00520049
006610                                                                  00661039
006700 01  1ST-CCYYDDD         PIC 9(07) VALUE 0.                       00670050
006800 01  FILLER REDEFINES 1ST-CCYYDDD.                                00680046
006900     03  1ST-CCYY        PIC 9(04).                               00690035
007000     03  1ST-DDD         PIC 9(03).                               00700035
007010                                                                  00701039
007100 01  2ND-CCYYDDD         PIC 9(07) VALUE 0.                       00710050
007200 01  FILLER REDEFINES 2ND-CCYYDDD.                                00720046
007300     03  2ND-CCYY        PIC 9(04).                               00730035
007400     03  2ND-DDD         PIC 9(03).                               00740035
007500                                                                  00750035
007600 01  1ST-TOTAL-DAYS      PIC 9(10) VALUE 0.                       00760059
007700 01  2ND-TOTAL-DAYS      PIC 9(10) VALUE 0.                       00770059
007800                                                                  00780035
007900 LINKAGE SECTION.                                                 00790029
008000 01  1ST-CCYYMMDD-S      PIC S9(08) COMP-3.                       00800075
008100 01  2ND-CCYYMMDD-S      PIC S9(08) COMP-3.                       00810075
008200 01  DIFF                PIC S9(08).                              00820034
008300                                                                  00830039
008500 PROCEDURE DIVISION USING 1ST-CCYYMMDD-S 2ND-CCYYMMDD-S DIFF.     00850075
008600 DAYS-DIFF.                                                       00860049
008700     MOVE 1ST-CCYYMMDD-S TO 1ST-CCYYMMDD.                         00870075
008900     MOVE 1ST-CCYYMMDD TO CALENDAR-CCYYMMDD.                      00890071
008910     IF 1ST-CC = 00                                               00891071
009000       IF CALENDAR-YY > 79                                        00900071
009100         ADD 19000000 TO CALENDAR-CCYYMMDD                        00910071
009200       ELSE                                                       00920071
009300         ADD 20000000 TO CALENDAR-CCYYMMDD.                       00930071
009600     PERFORM CONVERT-DATE.                                        00960025
009700     MOVE JULIAN-CCYYDDD TO 1ST-CCYYDDD.                          00970065
009900     COMPUTE 1ST-TOTAL-DAYS = ( 1ST-CCYY * 365 ) + 1ST-DDD.       00990058
010100                                                                  01010034
010110     MOVE 2ND-CCYYMMDD-S TO 2ND-CCYYMMDD.                         01011075
010200     MOVE 2ND-CCYYMMDD TO CALENDAR-CCYYMMDD.                      01020071
010210     IF 2ND-CC = 00                                               01021071
010300       IF CALENDAR-YY > 79                                        01030071
010400         ADD 19000000 TO CALENDAR-CCYYMMDD                        01040071
010500       ELSE                                                       01050071
010600         ADD 20000000 TO CALENDAR-CCYYMMDD.                       01060071
010900     PERFORM CONVERT-DATE.                                        01090034
011000     MOVE JULIAN-CCYYDDD TO 2ND-CCYYDDD.                          01100065
011200     COMPUTE 2ND-TOTAL-DAYS = ( 2ND-CCYY * 365 ) + 2ND-DDD.       01120058
011400                                                                  01140035
011500     COMPUTE DIFF = 1ST-TOTAL-DAYS - 2ND-TOTAL-DAYS.              01150058
011600     GOBACK.                                                      01160032
011700                                                                  01170025
011800 CONVERT-DATE.                                                    01180025
012000     MOVE 0 TO JULIAN-CCYYDDD-R.                                  01200065
012100     PERFORM CHECK-FOR-LEAP-YEAR.                                 01210060
012400     IF LEAP-YEAR-IND = 0                                         01240025
012500        MOVE 29 TO FEBRUARY                                       01250025
012600     ELSE                                                         01260025
012700        MOVE 28 TO FEBRUARY.                                      01270025
012800     PERFORM GET-NUM-DAYS                                         01280039
012900        VARYING MONTH-SUB FROM 1 BY 1                             01290025
013000           UNTIL MONTH-SUB = CALENDAR-MM.                         01300065
013100     ADD CALENDAR-DD     TO JULIAN-DDD.                           01310065
013200     MOVE CALENDAR-CCYY TO JULIAN-CCYY.                           01320067
013300                                                                  01330056
013400 CHECK-FOR-LEAP-YEAR.                                             01340060
013500     DIVIDE CALENDAR-CCYY BY 4                                    01350065
013600       GIVING RESULT REMAINDER LEAP-YEAR-IND.                     01360056
013700     IF LEAP-YEAR-IND = 0                                         01370056
013800        AND                                                       01380056
013801        CALENDAR-YY = 00                                          01380165
013810       DIVIDE CALENDAR-CCYY BY 400                                01381065
013820         GIVING RESULT REMAINDER LEAP-YEAR-IND.                   01382056
014900                                                                  01490025
015000 GET-NUM-DAYS.                                                    01500039
015100     ADD DAYS-IN-MONTH(MONTH-SUB) TO JULIAN-DDD.                  01510065
