       IDENTIFICATION DIVISION.
       PROGRAM-ID.    FGARD10.
       AUTHOR.        UCSD ACT.
       DATE-WRITTEN.  DECEMBER 1994.
       DATE-COMPILED.
      ***************************************************************
      *                  M O D I F I C A T I O N S
      ***************************************************************
      ***************************************************************
      ***************************************************************
      ***************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           C01 IS TOP-OF-PAGE.

       SOURCE-COMPUTER.
       OBJECT-COMPUTER.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINT-FILE              ASSIGN TO UT-S-REPORT1.
           SELECT JVOUCHER-IN-FILE        ASSIGN TO UT-S-JVOUCHER.

       DATA DIVISION.
       FILE SECTION.
      ****************************************************************
      *  REPORT PRINT FILE
      ****************************************************************
       FD  PRINT-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 132 CHARACTERS
           DATA RECORD IS PRINT-RECORD.

       01  PRINT-RECORD.
           03  PRINT-LINE                   PIC X(132).

      ****************************************************************
      *  JOURNAL TYPE 0007
      ****************************************************************
       FD  JVOUCHER-IN-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 132 CHARACTERS
           DATA RECORD IS JV-RECORD.

       01  JV-RECORD           PIC X(132).

       WORKING-STORAGE SECTION.
       01  FILLER                       PIC X(034)
           VALUE 'BEGIN WORKING STORAGE FOR FGARD10'.

       01  WS-FIRST-TIME-SW             PIC X(01) VALUE 'Y'.
           88  FIRST-TIME VALUE 'Y'.

       01  WS-FIRST-TIME-SW2            PIC X(01) VALUE 'Y'.
           88  FIRST-TIME2 VALUE 'Y'.

       01  WS-EOF-SW                    PIC X(01) VALUE 'N'.
           88  EOF-SW     VALUE 'Y'.
      *
       01  WS-PAGE-CTR                  PIC 9(06) VALUE ZERO.
       01  WS-LINE-CTR                  PIC 9(06) VALUE ZERO.
      *
       01  WS-ACCEPT-DATE.
           03  WS-ACCEPT-DATE-YY        PIC X(02) VALUE SPACES.
           03  WS-ACCEPT-DATE-MM        PIC X(02) VALUE SPACES.
           03  WS-ACCEPT-DATE-DD        PIC X(02) VALUE SPACES.
      *
       01  WS-REPORT-DATE.
           03  WS-REPORT-DATE-MM        PIC X(02) VALUE SPACES.
           03  FILLER                   PIC X(01) VALUE '/'.
           03  WS-REPORT-DATE-DD        PIC X(02) VALUE SPACES.
           03  FILLER                   PIC X(01) VALUE '/'.
           03  WS-REPORT-DATE-YY        PIC X(02) VALUE SPACES.
      *
       01  WS-CURRENT-TIME              PIC 9(06).
       01  WS-CURRENT-REDF REDEFINES WS-CURRENT-TIME.
           03  WS-CURRENT-TIME-HH       PIC 9(02).
           03  WS-CURRENT-TIME-MM       PIC 9(02).
           03  WS-CURRENT-TIME-SS       PIC 9(02).
      *
       01  WS-POST-START-TIME      PIC 9(08).
       01  WS-POST-STOP-TIME       PIC 9(08).
      *
       01  WS-PRINT-LINE                PIC X(132) VALUE SPACES.
      *
       01  DATE-CONV-COB2. COPY WSDATEC2.
       01  TIME-CONV-COB2. COPY WSTIMEC2.
       01  WHEN-COMP-CONV. COPY WHENCMC2.
      *
      /
      ****************************************************************
      *  CONTROL REPORT HEADINGS
      ****************************************************************
      ****************************************************************  04090000
      * FORMATTED HEADINGS FOR CONTROL REPORT                           04120000
      ***************************************************************** 04130000
       01  CONTROL-LINE-1.                                              04140000
           03  FILLER                  PIC X(048) VALUE                 04150000
               'PROGRAM NAME: FGARD10 '.                                04160000
           03  FILLER                  PIC X(065) VALUE                 04170000
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.                   04180000
           03  FILLER                  PIC X(005) VALUE 'PAGE:'.        04190000
           03  CL1-PAGE-NMBR           PIC ZZZ9.                        04200000
                                                                        04210000
       01  CONTROL-LINE-2.                                              04220000
           03  FILLER                  PIC X(048) VALUE SPACES.         04230000
           03  FILLER                  PIC X(092) VALUE                 04240000
               '    IFIS POSTING UPDATE RESULTS REPORT   '.
                                                                        04260000
       01  CONTROL-LINE-3.                                              04270000
           03  FILLER                  PIC X(007) VALUE 'RUN ON'.       04280000
           03  CL3-RUN-DATE            PIC X(008).                      04290000
           03  FILLER                  PIC X(004) VALUE ' AT '.         04300000
           03  CL3-RUN-TIME-HH         PIC 9(002).                      04310000
           03  FILLER                  PIC X(001) VALUE ':'.            04320000
           03  CL3-RUN-TIME-MM         PIC 9(002).                      04330000
           03  FILLER                  PIC X(001) VALUE ':'.            04340000
           03  CL3-RUN-TIME-SS         PIC 9(002).                      04350000
           03  FILLER                  PIC X(032) VALUE SPACES.         04360000
           03  FILLER                  PIC X(073) VALUE                 04370000
               'CONTROL REPORT'.                                        04380000

       01  CONTROL-LINE-4.
           03  FILLER                  PIC X(31) VALUE
               '----D O C U M E N T----'.
                                                                        04390000
       01  CONTROL-LINE-5.
           03  FILLER                  PIC X(60) VALUE
               'TYPE  NUMBER  CHG COUNT POSTING RESULTS'.
           03  FILLER                  PIC X(54) VALUE
               'R4187  R4188  R4189  R4199  R4197  R4191  R4190  R4183'.
           03  FILLER                  PIC X(17) VALUE
               '    START    STOP'.
                                                                        04390000
       01  CONTROL-LINE-DETAIL.
           03  CLD-SEQ-NMBR            PIC X(05).
           03  CLD-DCMNT-NMBR          PIC X(09).
           03  CLD-CHNG-SEQ-NMBR       PIC X(04).
           03  CLD-TRANS-CTR           PIC Z(05)B.
           03  CLD-POSTING-MSG         PIC X(35).
           03  CLD-R4187-CTR           PIC Z(6)B.
           03  CLD-R4188-CTR           PIC Z(6)B.
           03  CLD-R4189-CTR           PIC Z(6)B.
           03  CLD-R4199-CTR           PIC Z(6)B.
           03  CLD-R4197-CTR           PIC Z(6)B.
           03  CLD-R4191-CTR           PIC Z(6)B.
           03  CLD-R4190-CTR           PIC Z(6)B.
           03  CLD-R4183-CTR           PIC Z(6)B.
           03  CLD-START-TIME          PIC Z(8)B.
           03  CLD-STOP-TIME           PIC Z(8)B.
                                                                        06020000
       01  NORMAL-END-LINE.                                             04220000
           03  FILLER                  PIC X(048) VALUE SPACES.         04230000
           03  FILLER                  PIC X(092) VALUE                 04240000
               '    NORMAL END OF PROGRAM                '.
                                                                        04260000
                                                                        06020000
      *
       01  FILLER                       PIC X(032)
           VALUE 'END WORKING STORAGE FOR FGARD10'.
      /
       PROCEDURE DIVISION.
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL CONTROL THE THREE MAJOR PARTS  *
      *  OF THIS PROGRAM:                                            *
      *    1) INITIALIZE                                             *
      *    2) PROCESS                                                *
      *    3) PERFORM END OF PROCESSING CLEANUP DUTIES               *
      ****************************************************************

       0000-CONTROL SECTION.

           OPEN INPUT  JVOUCHER-IN-FILE.
           OPEN OUTPUT PRINT-FILE.
           PERFORM 1500-PRINT-IT                                        07710000
              THRU 1500-EXIT
              UNTIL EOF-SW.

           PERFORM 9000-WS-END-OF-PROGRAM
              THRU 9000-EXIT.

      ****************************************************************
      *  GET SET UP
      ****************************************************************
       1000-INITIALIZE.

           ACCEPT WS-ACCEPT-DATE       FROM DATE.
           MOVE WS-ACCEPT-DATE-YY      TO WS-REPORT-DATE-YY.
           MOVE WS-ACCEPT-DATE-MM      TO WS-REPORT-DATE-MM.
           MOVE WS-ACCEPT-DATE-DD      TO WS-REPORT-DATE-DD.

           MOVE 56 TO WS-LINE-CTR.
           COPY PDTIMEC2.                                               06150000
                                                                        06160000
           MOVE WS-REPORT-DATE TO CL3-RUN-DATE.
           MOVE TIME-HH TO CL3-RUN-TIME-HH.                             06180000
           MOVE TIME-MM TO CL3-RUN-TIME-MM.                             06190000
           MOVE TIME-SS TO CL3-RUN-TIME-SS.                             06200000
                                                                        06220000
           PERFORM 8000-PRINT-HEADINGS                                  06240000
              THRU 8000-EXIT.                                           06240000
                                                                        06250000
           MOVE SPACES TO WS-PRINT-LINE.                                06260000
           MOVE WHEN-COMPILED TO COMPILED-DATA.                         06270000
           INSPECT COMPILED-DATA-TIME REPLACING ALL '.' BY ':'.         06280000
           STRING  'PROGRAM COMPILED ',                                 06290000
                   COMPILED-DATA-DATE,                                  06300000
                   ' AT ',                                              06310000
                   COMPILED-DATA-TIME                                   06320000
                   DELIMITED BY SIZE INTO WS-PRINT-LINE.                06330000
                                                                        06350000
           PERFORM 8100-WRITE-REPORT                                    06340000
              THRU 8100-EXIT.                                           06340000
                                                                        07390000
           PERFORM 8100-WRITE-REPORT
              THRU 8100-EXIT 2 TIMES.

           MOVE CONTROL-LINE-4 TO WS-PRINT-LINE.
           PERFORM 8100-WRITE-REPORT
              THRU 8100-EXIT.

           MOVE CONTROL-LINE-5 TO WS-PRINT-LINE.
           PERFORM 8100-WRITE-REPORT
              THRU 8100-EXIT 2 TIMES.

       1000-EXIT.
           EXIT.

       1500-PRINT-IT.                                                   07710000
                                                                        05980010
           READ JVOUCHER-IN-FILE INTO CONTROL-LINE-DETAIL
              AT END
                 MOVE 'Y' TO WS-EOF-SW
                 GO TO 1500-EXIT.

           IF FIRST-TIME2
              MOVE 'N' TO WS-FIRST-TIME-SW2
              PERFORM 1000-INITIALIZE
                 THRU 1000-EXIT.
           MOVE CONTROL-LINE-DETAIL TO WS-PRINT-LINE.
           PERFORM 8100-WRITE-REPORT THRU 8100-EXIT.

       1500-EXIT.
           EXIT.

       8000-PRINT-HEADINGS.

           ADD 1 TO WS-PAGE-CTR.
           MOVE 0 TO WS-LINE-CTR.

           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD BEFORE TOP-OF-PAGE.

           MOVE WS-PAGE-CTR TO CL1-PAGE-NMBR.
           WRITE PRINT-RECORD FROM CONTROL-LINE-1 BEFORE 1.
           WRITE PRINT-RECORD FROM CONTROL-LINE-2 BEFORE 1.
           WRITE PRINT-RECORD FROM CONTROL-LINE-3 BEFORE 2.
           ADD 3 TO WS-LINE-CTR.
           IF FIRST-TIME
               MOVE 'N' TO WS-FIRST-TIME-SW
               GO TO 8000-EXIT
           ELSE
               WRITE PRINT-RECORD FROM CONTROL-LINE-4 BEFORE 1
               WRITE PRINT-RECORD FROM CONTROL-LINE-5 BEFORE 2
               ADD 3 TO WS-LINE-CTR.

       8000-EXIT.
           EXIT.

       8100-WRITE-REPORT.

           WRITE PRINT-RECORD FROM WS-PRINT-LINE BEFORE 1.
           MOVE SPACES TO
                WS-PRINT-LINE
                PRINT-RECORD.
           ADD  1                  TO WS-LINE-CTR.

           IF WS-LINE-CTR > 55
               PERFORM 8000-PRINT-HEADINGS THRU 8000-EXIT
               MOVE 5   TO WS-LINE-CTR.

       8100-EXIT.
           EXIT.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL HANDLE END OF PROGRAM PROCESSING
      ****************************************************************
       9000-WS-END-OF-PROGRAM.

           IF FIRST-TIME2
              NEXT SENTENCE
           ELSE
              MOVE NORMAL-END-LINE TO WS-PRINT-LINE
              WRITE PRINT-RECORD FROM WS-PRINT-LINE BEFORE 1.

           CLOSE PRINT-FILE.
           CLOSE JVOUCHER-IN-FILE.
           STOP RUN.

       9000-EXIT.
           EXIT.
