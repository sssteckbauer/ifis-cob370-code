       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.    FTAP501D.                                         00020000
       AUTHOR.        ADCOM SYSTEMS.                                    00030000
       DATE-WRITTEN.  JULY 1992.                                        00040000
       DATE-COMPILED.                                                   00050000
      *REMARKS.                                                         00060000
      *                                                                 00070000
      *    BATCH COBOL PRINT SUBPROGRAM TO PRODUCE                      00080000
      *            TRAVEL ACCOUNTS NUMERICAL REPORT                     00090000
      *                                                                 00100000
      *    INPUT:                                                       00110000
      *            EXTRACT FILE  EXTR                                   00120000
      *                                                                 00130000
      *    OUTPUT:                                                      00140000
      *            PRINT FILE    FDPRNT01.                              00150000
      *                                                                 00160000
       ENVIRONMENT DIVISION.                                            00170000
       DATA DIVISION.                                                   00180000
       WORKING-STORAGE SECTION.                                         00190000
       COPY WSSTRTWS.                                                   00200000
      ****************************************************************  00210000
      *                    SWITCHES                                     00220000
      ****************************************************************  00230000
       01  SWITCHES.                                                    00240000
           03  TYPE-OF-DATA-FLAG               PIC X(001).              00250000
               88  BEG-OF-REPORT-DATA          VALUE LOW-VALUE.         00260000
               88  VALID-DATA                  VALUE SPACE.             00270000
               88  END-OF-REPORT-DATA          VALUE HIGH-VALUE.        00280000
                                                                        00290000
       01  WS-FIRST-TIME-FLAG                  PIC X(001).              00300000
           88  FIRST-TIME                                 VALUE 'Y'.    00310000
                                                                        00320000
      ****************************************************************  00330000
      *                    IO CONTROL AREA                              00340000
      ****************************************************************  00350000
           COPY FIOCNTRL.                                               00360000
      ****************************************************************  00370000
      *                    HOLD AREAS                                   00380000
      ****************************************************************  00390000
       01  HOLD-AREAS.                                                  00400000
                                                                        00410000
           03  PRINT-RECORD.                                            00420000
               05  PRN-SKIPX                   PIC X(001) VALUE SPACES. 00430000
               05  PRN-LINE1                   PIC X(132) VALUE SPACES. 00440000
                                                                        00450000
      ****************************************************************  00460000
      *               COUNTERS AND SUBSCRIPTS                           00470000
      ****************************************************************  00480000
                                                                        00490000
       01  MISC-AREAS.                                                  00500000
                                                                        00510000
           03  WS-PAGE                         PIC 9(004) VALUE ZERO.   00520000
           03  LINE-CNT                        PIC 9(002) VALUE 99.     00530000
           03  PRN-SKIPX-NUM                   PIC 9(001) VALUE ZERO.   00540000
                                                                        00550000
       01  BLOWUP-PARMS.                                                00560000
                                                                        00570000
           03  BAD-1-WS                PIC 9(03)     VALUE ZEROES.      00580000
           03  BAD-2-WS                PIC 9(03)     VALUE ZEROES.      00590000
           03  BAD-3-WS                PIC 9(03)     VALUE ZEROES.      00600000
                                                                        00610000
       01  WS-CONSTANTS.                                                00620000
                                                                        00630000
           03  WS-CONSTANT-Y                   PIC X(001) VALUE 'Y'.    00640000
           03  WS-CONSTANT-N                   PIC X(001) VALUE 'N'.    00650000
           03  WS-CONSTANT-TOP-OF-PAGE         PIC X(001) VALUE 'T'.    00660000
           03  WS-CONSTANT-1                   PIC X(001) VALUE '1'.    00670000
           03  WS-CONSTANT-2                   PIC X(001) VALUE '2'.    00680000
           03  WS-CONSTANT-3                   PIC X(001) VALUE '3'.    00690000
           03  WS-CONSTANT-OPEN                PIC X(006) VALUE 'OPEN'. 00700000
           03  WS-CONSTANT-READ                PIC X(006) VALUE 'READ'. 00710000
           03  WS-CONSTANT-WRITE               PIC X(006) VALUE 'WRITE'.00720000
           03  WS-CONSTANT-CLOSE               PIC X(006) VALUE 'CLOSE'.00730000
           03  WS-CONSTANT-MAX-LINE-CNTR       PIC 9(002) VALUE 56.     00740000
           03  WS-CONSTANT-PROG-NAME           PIC X(08)  VALUE         00750000
               'FTAP501D'.                                              00760000
           03  WS-CONSTANT-RPT-TITLE           PIC X(60)  VALUE         00770000
               '                 TRAVELER NUMERICAL LISTING'.           00780000
                                                                        00790000
                                                                        00800000
       COPY WSCTRHED.                                                   00810000
       COPY WSBANNER.                                                   00820000
       COPY WSHEAD1.                                                    00830000
       COPY WSHEAD2.                                                    00840000
                                                                        00850000
       01  HEADING-LINE-3.                                              00860000
           03  FILLER                      PIC X(59) VALUE SPACES.      00870000
           03  FILLER                      PIC X(06) VALUE 'AS OF'.     00880000
           03  RPT-AS-OF-DATE              PIC X(08) VALUE SPACES.      00890000
           03  FILLER                      PIC X(59) VALUE SPACES.      00900000
                                                                        00910000
       01  HEADING-LINE-4.                                              00920000
           03  FILLER                      PIC X(60) VALUE SPACES.      00930000
           03  RPT-REPORT-TYPE             PIC X(12) VALUE SPACES.      00940000
           03  FILLER                      PIC X(60) VALUE SPACES.      00950000
                                                                        00960000
       01  HEADING-LINE-5.                                              00970000
           03  FILLER                      PIC X(07) VALUE SPACES.      00980000
           03  FILLER                      PIC X(40) VALUE 'TRAVELER'.  00990000
           03  FILLER                      PIC X(08) VALUE 'EMPLOYEE'.  01000000
                                                                        01010000
       01  HEADING-LINE-6.                                              01020000
           03  FILLER                      PIC X(15) VALUE ' NUMBER'.   01030000
           03  FILLER                      PIC X(32) VALUE 'NAME'.      01040000
           03  FILLER                      PIC X(09) VALUE 'INDICATOR'. 01050000
                                                                        01060000
       01  HEADING-LINE-7.                                              01070000
           03  FILLER                      PIC X(09) VALUE '---------'. 01080000
           03  FILLER                      PIC X(02) VALUE SPACES.      01090000
           03  FILLER                      PIC X(35) VALUE              01100000
               '-----------------------------------'.                   01110000
           03  FILLER                      PIC X(01) VALUE SPACES.      01120000
           03  FILLER                      PIC X(09) VALUE '---------'. 01130000
                                                                        01140000
       01  HEADING-LINE-8.                                              01150000
           03  FILLER                      PIC X(18)  VALUE SPACES.     01160000
           03  FILLER                      PIC X(25)  VALUE             01170000
               'ADDRESS TYPE'.                                          01180000
           03  FILLER                      PIC X(34)  VALUE             01190000
               'STREET 1/CITY - STATE - ZIP'.                           01200000
           03  FILLER                      PIC X(27)  VALUE             01210000
               'STREET 2/COUNTRY'.                                      01220000
           03  FILLER                      PIC X(26)  VALUE             01230000
               'STREET 3/PHONE - EXTENSION'.                            01240000
                                                                        01250000
       01  HEADING-LINE-9.                                              01260000
           03  FILLER                      PIC X(09) VALUE SPACES.      01270000
           03  FILLER                      PIC X(31) VALUE              01280000
               '------------------------------ '.                       01290000
           03  FILLER                      PIC X(31) VALUE              01300000
               '------------------------------ '.                       01310000
           03  FILLER                      PIC X(31) VALUE              01320000
               '------------------------------ '.                       01330000
           03  FILLER                      PIC X(30) VALUE              01340000
               '------------------------------'.                        01350000
                                                                        01360000
       01  DETAIL-LINE-1.                                               01370000
           03  PRN-TRVL-ACCT               PIC X(09) VALUE SPACES.      01380000
           03  FILLER                      PIC X(02) VALUE SPACES.      01390000
           03  PRN-NAME-KEY                PIC X(35) VALUE SPACES.      01400000
           03  FILLER                      PIC X(05) VALUE SPACES.      01410000
           03  PRN-EMPLY-IND               PIC X(01) VALUE SPACES.      01420000
                                                                        01430000
       01  DETAIL-LINE-2.                                               01440000
           03  FILLER                      PIC X(09) VALUE SPACES.      01450000
           03  PRN-ADR-TYPE-DESC           PIC X(30) VALUE SPACES.      01460000
           03  FILLER                      PIC X(01) VALUE SPACES.      01470000
           03  PRN-LINE-ADRS-1             PIC X(30) VALUE SPACES.      01480000
           03  FILLER                      PIC X(01) VALUE SPACES.      01490000
           03  PRN-LINE-ADRS-2             PIC X(30) VALUE SPACES.      01500000
           03  FILLER                      PIC X(01) VALUE SPACES.      01510000
           03  PRN-LINE-ADRS-3             PIC X(30) VALUE SPACES.      01520000
                                                                        01530000
       01  DETAIL-LINE-3.                                               01540000
           03  FILLER                      PIC X(40) VALUE SPACES.      01550000
           03  PRN-CITY-NAME               PIC X(18) VALUE SPACES.      01560000
           03  FILLER                      PIC X(02) VALUE SPACES.      01570000
           03  PRN-STATE-CODE              PIC X(02) VALUE SPACES.      01580000
           03  FILLER                      PIC X(03) VALUE SPACES.      01590000
           03  PRN-ZIP-CODE                PIC X(05) VALUE SPACES.      01600000
           03  FILLER                      PIC X(01) VALUE SPACES.      01610000
           03  PRN-CNTRY-NAME              PIC X(30) VALUE SPACES.      01620000
           03  FILLER                      PIC X(08) VALUE SPACES.      01630000
           03  PRN-TLPHN-AREA-CODE         PIC X(03) VALUE SPACES.      01640000
           03  PRN-TLPHN-1-DASH            PIC X(01) VALUE SPACES.      01650000
           03  PRN-TLPHN-XCHNG-ID          PIC X(03) VALUE SPACES.      01660000
           03  PRN-TLPHN-2-DASH            PIC X(01) VALUE SPACES.      01670000
           03  PRN-TLPHN-SEQ-ID            PIC X(04) VALUE SPACES.      01680000
           03  FILLER                      PIC X(02) VALUE SPACES.      01690000
           03  PRN-TLPHN-XTNSN-ID          PIC X(04) VALUE SPACES.      01700000
                                                                        01710000
                                                                        01720000
           COPY FTA501.                                                 01730000
           COPY WSDONEWS.                                               01740000
       EJECT                                                            01750000
       LINKAGE SECTION.                                                 01760000
           COPY WSLKDATA.                                               01770000
       PROCEDURE DIVISION USING                                         01780000
                                PASSED-ARGS-TO-REPORTS.                 01790000
      ****************************************************************  01800000
      *  THE FOLLOWING PARAGRAPH WILL CONTROL THE PROCESSING OF THE  *  01810000
      *  EXTRACT RECORD PASSED TO THIS PROGRAM.                      *  01820000
      ****************************************************************  01830000
       0000-DRIVER.                                                     01840000
                                                                        01850000
           PERFORM 1000-INITIALIZE THRU 1000-EXIT.                      01860000
                                                                        01870000
           PERFORM 9100-READ-EXTRACT THRU 9100-EXIT.                    01880000
                                                                        01890000
           IF  END-OF-REPORT-DATA                                       01900000
               DISPLAY 'NO DATA FOR REPORT ', WS-CONSTANT-PROG-NAME     01910000
           ELSE                                                         01920000
               PERFORM 2000-MAINLINE THRU 2000-EXIT                     01930000
                            UNTIL END-OF-REPORT-DATA.                   01940000
                                                                        01950000
           DISPLAY 'REPORT PRINTED ', WS-CONSTANT-PROG-NAME.            01960000
           PERFORM 9999-EOJ.                                            01970000
                                                                        01980000
       0000-EXIT.                                                       01990000
           EXIT.                                                        02000000
                                                                        02010000
       1000-INITIALIZE.                                                 02020000
                                                                        02030000
      **   READ RECORD TYPE 1 TO GET INFO FOR BANNER PAGE *             02040000
      **   AND STANDARD HEADING 1.                                      02050000
                                                                        02060000
           PERFORM 9100-READ-EXTRACT THRU 9100-EXIT.                    02070000
                                                                        02080000
           IF  END-OF-REPORT-DATA                                       02090000
               PERFORM 8100-PRINT-NO-RPT-BANNER                         02100000
               PERFORM 9999-EOJ.                                        02110000
                                                                        02120000
           MOVE FTA501-USER-ID             TO WS-USER-ID.               02130000
           MOVE FTA501-USER-NAME           TO WS-USER-NAME.             02140000
           MOVE FTA501-INST-TITLE          TO WS-INSTITUTION.           02150000
           IF FTA501-REPORT-TYPE = 'A'                                  02160000
              MOVE '    ALL' TO RPT-REPORT-TYPE.                        02170000
           IF FTA501-REPORT-TYPE = 'E'                                  02180000
              MOVE '  EMPLOYEE' TO RPT-REPORT-TYPE.                     02190000
           IF FTA501-REPORT-TYPE = 'N'                                  02200000
              MOVE 'NON EMPLOYEE' TO RPT-REPORT-TYPE.                   02210000
           IF FTA501-REPORT-TYPE = 'U'                                  02220000
              MOVE ' UNIVERSITY' TO RPT-REPORT-TYPE.                    02230000
                                                                        02240000
           PERFORM 8000-PRINT-BANNER-PAGE.                              02250000
                                                                        02260000
           MOVE WS-DATE                    TO  SRH1-RUN-DATE.           02270000
           MOVE WS-TIME                    TO  SRH1-RUN-TIME.           02280000
                                                                        02290000
           MOVE 99                         TO LINE-CNT.                 02300000
           MOVE ZERO                       TO WS-PAGE.                  02310000
                                                                        02320000
           MOVE FTA501-AS-OF-DATE          TO RPT-AS-OF-DATE.           02330000
                                                                        02340000
       1000-EXIT.                                                       02350000
           EXIT.                                                        02360000
                                                                        02370000
      /                                                                 02380000
       2000-MAINLINE.                                                   02390000
                                                                        02400000
           IF LINE-CNT GREATER THAN 54                                  02410000
              PERFORM 7000-PRINT-HEADINGS THRU 7000-EXIT.               02420000
                                                                        02430000
           MOVE FTA501-TRVL-ACCT-LAST-NINE TO PRN-TRVL-ACCT.            02440000
           MOVE FTA501-TRVL-NAME-KEY       TO PRN-NAME-KEY.             02450000
           MOVE FTA501-EMPLY-IND           TO PRN-EMPLY-IND.            02460000
           MOVE DETAIL-LINE-1              TO PRN-LINE1.                02470000
           IF FIRST-TIME                                                02480000
              MOVE 'N' TO WS-FIRST-TIME-FLAG                            02490000
              MOVE WS-CONSTANT-1           TO PRN-SKIPX                 02500000
           ELSE                                                         02510000
              MOVE WS-CONSTANT-3           TO PRN-SKIPX.                02520000
           PERFORM 9200-WRITE THRU 9200-EXIT.                           02530000
                                                                        02540000
           MOVE HEADING-LINE-8             TO PRN-LINE1.                02550000
           MOVE WS-CONSTANT-2              TO PRN-SKIPX.                02560000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       02570000
           PERFORM 9250-ADD-TO-LINE-CNT THRU 9250-EXIT.                 02571000
           MOVE HEADING-LINE-9             TO PRN-LINE1.                02580000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                02590000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       02601000
           PERFORM 9250-ADD-TO-LINE-CNT THRU 9250-EXIT.                 02602000
                                                                        02610000
           MOVE FTA501-ADR-TYPE-DESC       TO PRN-ADR-TYPE-DESC.        02620000
           MOVE FTA501-LINE-ADRS-1         TO PRN-LINE-ADRS-1.          02630000
           MOVE FTA501-LINE-ADRS-2         TO PRN-LINE-ADRS-2.          02640000
           MOVE FTA501-LINE-ADRS-3         TO PRN-LINE-ADRS-3.          02650000
           MOVE DETAIL-LINE-2              TO PRN-LINE1.                02660000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                02670000
           PERFORM 9200-WRITE THRU 9200-EXIT.                           02680000
                                                                        02690000
           MOVE FTA501-CITY-NAME           TO PRN-CITY-NAME.            02700000
           MOVE FTA501-STATE-CODE          TO PRN-STATE-CODE.           02710000
           MOVE FTA501-ZIP-CODE            TO PRN-ZIP-CODE.             02720000
           MOVE FTA501-CNTRY-NAME          TO PRN-CNTRY-NAME.           02730000
           MOVE FTA501-TLPHN-AREA-CODE     TO PRN-TLPHN-AREA-CODE.      02731000
           MOVE FTA501-TLPHN-XCHNG-ID      TO PRN-TLPHN-XCHNG-ID.       02732000
           MOVE FTA501-TLPHN-SEQ-ID        TO PRN-TLPHN-SEQ-ID.         02733000
           MOVE FTA501-TLPHN-XTNSN-ID      TO PRN-TLPHN-XTNSN-ID.       02734000
           MOVE DETAIL-LINE-3              TO PRN-LINE1.                02740000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                02750000
           PERFORM 9200-WRITE THRU 9200-EXIT.                           02760000
                                                                        02770000
           PERFORM 9100-READ-EXTRACT THRU 9100-EXIT.                    02780000
                                                                        02790000
       2000-EXIT.                                                       02800000
           EXIT.                                                        02810000
                                                                        02820000
       7000-PRINT-HEADINGS.                                             02830000
                                                                        02840000
           MOVE 'Y'                        TO WS-FIRST-TIME-FLAG.       02850000
                                                                        02860000
           MOVE SPACES                     TO PRN-LINE1.                02870000
           MOVE SRH-HEADING-1              TO PRN-LINE1.                02880000
           MOVE WS-CONSTANT-TOP-OF-PAGE    TO PRN-SKIPX.                02890000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       02900000
                                                                        02910000
           ADD 1                           TO WS-PAGE.                  02920000
           MOVE WS-PAGE                    TO SRH2-PAGE-NMBR.           02930000
           MOVE SRH-HEADING-2              TO PRN-LINE1.                02940000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                02950000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       02961000
                                                                        02970000
           MOVE HEADING-LINE-3             TO PRN-LINE1.                02980000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                02990000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03001000
                                                                        03010000
           MOVE HEADING-LINE-4             TO PRN-LINE1.                03020000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                03030000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03041000
                                                                        03050000
           MOVE SPACES                     TO PRN-LINE1.                03060000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                03070000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03081000
                                                                        03090000
           MOVE HEADING-LINE-5             TO PRN-LINE1.                03100000
           MOVE WS-CONSTANT-2              TO PRN-SKIPX.                03110000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03121000
                                                                        03130000
           MOVE HEADING-LINE-6             TO PRN-LINE1.                03140000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                03150000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03161000
                                                                        03170000
           MOVE HEADING-LINE-7             TO PRN-LINE1.                03180000
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.                03190000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03201000
                                                                        03210000
           MOVE  9                         TO LINE-CNT.                 03220000
                                                                        03230000
       7000-EXIT.                                                       03240000
           EXIT.                                                        03250000
     /                                                                  03260000
      ****************************************************************  03330000
      ***              PRINT BANNER PAGE                           ***  03340000
      ****************************************************************  03350000
                                                                        03360000
           COPY PRTBANPG.                                               03370000
           COPY UNVRSHED.                                               03380000
                                                                        03390000
       9100-READ-EXTRACT.                                               03400000
           MOVE WS-CONSTANT-READ           TO IO-MOD-COMMAND.           03410000
           CALL 'FGIO01'                                                03420000
                           USING IO-MOD-CONTROL                         03430000
                                 FTA501-EXTRACT-O.                      03440000
                                                                        03450000
           IF  IO-MOD-OK                                                03460000
               MOVE FTA501-REC-TYPE       TO TYPE-OF-DATA-FLAG          03470000
           ELSE                                                         03480000
           IF  IO-MOD-EOF                                               03490000
               MOVE HIGH-VALUE            TO TYPE-OF-DATA-FLAG          03500000
           ELSE                                                         03510000
               DISPLAY 'ERROR READING EXTRACT FILE, STATUS = '          03520000
                    IO-MOD-RESPONSE-CODE                                03530000
               DIVIDE BAD-1-WS BY BAD-2-WS GIVING BAD-3-WS.             03540000
                                                                        03550000
       9100-EXIT.                                                       03551000
           EXIT.                                                        03552000
                                                                        03553000
       9200-WRITE.                                                      03560000
                                                                        03570000
           PERFORM 9300-PRN-LINE1 THRU 9300-EXIT.                       03580000
           PERFORM 9250-ADD-TO-LINE-CNT THRU 9250-EXIT.                 03590000
                                                                        03600000
       9200-EXIT.                                                       03601000
           EXIT.                                                        03602000
                                                                        03610000
       9250-ADD-TO-LINE-CNT.                                            03620000
           IF PRN-SKIPX  NUMERIC                                        03630000
              MOVE PRN-SKIPX               TO  PRN-SKIPX-NUM            03640000
              ADD PRN-SKIPX-NUM            TO  LINE-CNT                 03650000
           ELSE                                                         03660000
              MOVE 1                       TO  LINE-CNT.                03670000
                                                                        03680000
       9250-EXIT.                                                       03681000
           EXIT.                                                        03682000
                                                                        03690000
       9300-PRN-LINE1.                                                  03700000
                                                                        03710000
           MOVE WS-CONSTANT-WRITE          TO IO-MOD-COMMAND.           03720000
           CALL 'FGIO02'                                                03730000
                           USING IO-MOD-CONTROL                         03740000
                                 PRINT-RECORD.                          03750000
           IF IO-MOD-OK                                                 03760000
              NEXT SENTENCE                                             03770000
           ELSE                                                         03780000
              DISPLAY IO-MOD-CONTROL ' INVALID WRITE PRINT FILE'        03790000
               DIVIDE BAD-1-WS BY BAD-2-WS GIVING BAD-3-WS.             03800000
                                                                        03810000
       9300-EXIT.                                                       03811000
           EXIT.                                                        03812000
                                                                        03820000
       9999-EOJ.                                                        03830000
                                                                        03840000
           EXIT PROGRAM.                                                03850000
                                                                        03860000
                                                                        03870000
