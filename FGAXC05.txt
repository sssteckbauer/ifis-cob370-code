      *RETRIEVAL.
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FGAXC05.
       AUTHOR.     ADCOM SYSTEMS.
       DATE-WRITTEN.  FEBRUARY 1994.
       DATE-COMPILED.
      *REMARKS.
      ****************************************************************
      *
      *           GENERAL LEDGER ROLL CONSOLIDATION PROGRAM
      *
      *    THIS PROGRAM READS JOURNAL VOUCHER OUTPUT FILE AND
      *    ACCUMULATES ALL FUND BALANCE ACCOUNT ENTRIES WITHIN EACH
      *    DOCUMENT.
      *    IF THE NET BALANCE IS ZERO FOR THE FUND BALANCE ENTRIES THEY
      *    ARE DROPPED FROM THE DOCUMENT. IF THE NET BALANCE IS NOT ZERO
      *    FOR THE FUND BALANCE ENTRIES, A SINGLE SUMMARIZED TRANSACTION
      *    IS WRITTEN TO A NEW JOURNAL VOUCHER FILE.
      *    THE JOURNAL VOUCHER HEADER RECORD IS MODIFIED FOR EACH
      *    DOCUMENT TO REFRECT ANY CHANGES TO THE DOCUMENT TOTAL.
      *
      *    INPUT:
      *          INPUT-FILE:  STANDARD IFIS JOURNAL VOUCHER FILE
      *
      *    OUTPUT:
      *          OUTPUT-FILE: NEW IFIS JOURNAL VOUCHER CONSOLIDATED FILE
      *          PRINT-FILE : CONTROL REPORT.
      *
      ******************************************************************
      *     MODIFICATION LOG                                           *
      ******************************************************************
DEVBWS* ADMIN 05/31/01 BWS - REMOVE IDMS REFERENCES                    *
      ****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           C01 IS TOP-OF-PAGE.

       SOURCE-COMPUTER.
       OBJECT-COMPUTER.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT JVOUCHER-STANDARD-FILE    ASSIGN TO UT-S-UGAUA00.
           SELECT JVOUCHER-CONSLDT-FILE     ASSIGN TO UT-S-UGAUA00C.
           SELECT CONTROL-REPORT-FILE       ASSIGN TO UT-S-CONTROLR.

DEVBWS*IDMS-CONTROL SECTION.                                            00650036
DEVBWS*PROTOCOL. MODE IS BATCH.                                         00680036
                                                                        00680036
       DATA DIVISION.                                                   00690036
                                                                        00720010
       FILE SECTION.

      ****************************************************************  00000590
      *  CONTROL REPORT FILE                                            00000600
      ****************************************************************  00000610
       FD  CONTROL-REPORT-FILE                                          00000620
           RECORDING MODE IS F                                          00000630
           LABEL RECORDS ARE OMITTED                                    00000640
           BLOCK CONTAINS 0 RECORDS                                     00000650
           RECORD CONTAINS 132 CHARACTERS                               00000660
           DATA RECORD IS CONTROL-REPORT-RECORD.                        00000670
                                                                        00000680
       01  CONTROL-REPORT-RECORD               PIC X(132).              00000690
                                                                        00000710
      ******************************************************                  09
      *  STANDARD JVOUCHER EXTRACT FILE                                       09
      ****************************************************************  00410009
       FD  JVOUCHER-STANDARD-FILE                                             09
           RECORDING MODE IS F                                                09
           LABEL RECORDS ARE OMITTED                                          09
           BLOCK CONTAINS 0 RECORDS                                           09
           RECORD CONTAINS 250 CHARACTERS                                     09
           DATA RECORD IS JV-STANDARD-REC.                                    09
                                                                              09
       01  JV-STANDARD-REC                     PIC X(250).                    09
                                                                              09
      ******************************************************                  09
      *  NEW JVOUCHER EXTRACT CONSOLIDATED FILE                               09
      ****************************************************************  00410009
       FD  JVOUCHER-CONSLDT-FILE                                              09
           RECORDING MODE IS F                                                09
           LABEL RECORDS ARE OMITTED                                          09
           BLOCK CONTAINS 0 RECORDS                                           09
           RECORD CONTAINS 250 CHARACTERS                                     09
           DATA RECORD IS JV-CONSLDT-REC.                                     09
                                                                              09
       01  JV-CONSLDT-REC                      PIC X(250).                    09
                                                                              09
      /                                                                 01160036
       WORKING-STORAGE SECTION.                                         01170000
                                                                              09
       01  WS-END-OF-FILE              PIC X(001) VALUE 'N'.
           88  END-OF-FILE                        VALUE 'Y'.
       01  WS-FUND-BLNC-ACCT-FLAG      PIC X(001) VALUE ' '.
           88  FUND-BLNC-ACCT-FOUND               VALUE 'Y'.
       01  WS-OTHER-ACCT-FLAG          PIC X(001) VALUE ' '.
           88  OTHER-ACCT-FOUND                   VALUE 'Y'.

       01  WS-REPORTS-COUNTERS.
           03  WS-CNTR-PAGE-CNT        PIC 9(004) VALUE ZEROES.
           03  WS-CNTR-LINE-CNT        PIC 9(002) VALUE 56.

       01  WS-RECORDS-COUNTERS.
           03  WS-JVSTND-HDR-REC-READ  PIC 9(006) VALUE ZEROES.
           03  WS-JVSTND-DTL-REC-READ  PIC 9(006) VALUE ZEROES.
           03  WS-JVCNSL-HDR-REC-WR    PIC 9(006) VALUE ZEROES.
           03  WS-JVCNSL-DTL-REC-WR    PIC 9(006) VALUE ZEROES.
           03  WS-JVCNSL-TOT-REC-WR    PIC 9(006) VALUE ZEROES.

       01  WS-DCMNT-FUND-BLNC-TOT      PIC S9(011)V99 VALUE ZEROES.
       01  WS-DCMNT-AMT-ADJST-TOT      PIC 9(011)V99 VALUE ZEROES.

       01  WS-HOLD-UGAUA00I-HDR-REC.
           03  FILLER                      PIC X(062) VALUE SPACES.
           03  WS-UGAUA00I-DCMNT-AMT       PIC 9(10)V99 VALUE ZEROES.
           03  FILLER                      PIC X(176) VALUE SPACES.

       01  WS-UGAUA00I-DTL-REC.
           03  FILLER                      PIC X(027) VALUE SPACES.
           03  WS-UGAUA00I-TRANS-AMT       PIC 9(10)V99 VALUE ZEROES.
           03  FILLER                      PIC X(35) VALUE SPACES.
           03  WS-UGAUA00I-DEBIT-CRDT-IND  PIC X(01) VALUE SPACES.
           03  FILLER                      PIC X(175) VALUE SPACES.

       01  CONTROL-REPORT-DATA             PIC X(132) VALUE SPACES.

      ****************************************************************
      * HOLD AREAS AND FORMAT AREAS FOR CONTROL REPORT
      ****************************************************************

       01  DATE-CONV-COB2.  COPY WSDATEC2.

       01  TIME-CONV-COB2.  COPY WSTIMEC2.

       01  WHEN-COMP-CONV.  COPY WHENCMC2.

      /
      *****************************************************************
      * FORMATTED HEADINGS FOR CONTROL REPORT
      *****************************************************************
       01  CONTROL-LINE-1.
           03  FILLER                  PIC X(048) VALUE
               'PROGRAM: FGAXC05'.
           03  FILLER                  PIC X(075) VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           03  FILLER                  PIC X(005) VALUE 'PAGE:'.
           03  CL1-PAGE-NUMBER         PIC ZZZ9.

       01  CONTROL-LINE-2.
           03  FILLER                  PIC X(029) VALUE
               'REPORT:  CONTROLR'.
           03  FILLER                  PIC X(073) VALUE
               'IFIF GENERAL LEDGER  ROLL JOURNAL VOUCHER CONSOLIDATION
      -        'PROCESS'.
           03  FILLER                  PIC X(022) VALUE
               'RETENTION: SAVE UNTIL '.
           03  CL2-RETENTION-DATE      PIC X(008).

       01  CONTROL-LINE-3.
           03  FILLER                  PIC X(007) VALUE 'RUN ON'.
           03  CL3-RUN-DATE            PIC X(008).
           03  FILLER                  PIC X(004) VALUE ' AT '.
           03  CL3-RUN-TIME-HH         PIC 9(002).
           03  FILLER                  PIC X(001) VALUE ':'.
           03  CL3-RUN-TIME-MM         PIC 9(002).
           03  FILLER                  PIC X(001) VALUE ':'.
           03  CL3-RUN-TIME-SS         PIC 9(002).
           03  FILLER                  PIC X(032) VALUE SPACES.
           03  FILLER                  PIC X(073) VALUE
               'CONTROL REPORT'.

       01  WS-RETENT-DATE.
           03  WS-RETENT-DATE-MM      PIC X(002) VALUE SPACES.
           03  WS-RETENT-DATE-MM-NUM REDEFINES WS-RETENT-DATE-MM
                                      PIC 9(02).
           03  FILLER                 PIC X(001) VALUE '/'.
           03  WS-RETENT-DATE-DD      PIC X(002) VALUE SPACES.
           03  FILLER                 PIC X(001) VALUE '/'.
           03  WS-RETENT-DATE-YY      PIC X(002) VALUE SPACES.
           03  WS-RETENT-DATE-YY-NUM REDEFINES WS-RETENT-DATE-YY
                                      PIC 9(02).
           03  FILLER                 PIC X(001) VALUE '/'.

      /
      *****************************************************************
      *  RECORD FILE FOR STANDARD JOURNAL VOUHER FILE.
      *****************************************************************

       01  UGAUA00I-RECORD. COPY UGAUA00I.

      /
       PROCEDURE DIVISION.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL CONTROL THE THREE MAJOR PARTS
      *  OF THIS PROGRAM: *
      *    1) INITIALIZE INPUT/OUTPUT FILES
      *    2) PROCESS THE INPUT DATA FILE
      *    3) PERFORM ANY END OF PROCESSING
      ****************************************************************

       0000-MAINLINE SECTION.

           PERFORM 1000-INITIALIZE THRU 1000-EXIT.

           PERFORM 2000-MAIN-PROCESS THRU 2000-EXIT
              UNTIL END-OF-FILE.

           PERFORM 9000-END-OF-PROGRAM THRU 9000-EXIT.

           STOP RUN.

      /
      ****************************************************************
      *  THE FOLLOWING SECTION WILL BIND RUN UNITS, READY THE
      *  NECESSARY AREAS, PREPARE THE PROGRAM TO ABORT DUE AN IDMS
      *  ERROR, OPEN THE FILES.
      ****************************************************************

       1000-INITIALIZE.

           OPEN INPUT  JVOUCHER-STANDARD-FILE.
           OPEN OUTPUT JVOUCHER-CONSLDT-FILE
                       CONTROL-REPORT-FILE.

           COPY PDDATEC2.
           COPY PDTIMEC2.

           MOVE PRE-EDIT-MM TO WS-RETENT-DATE-MM.
           MOVE PRE-EDIT-DD TO WS-RETENT-DATE-DD.
           MOVE PRE-EDIT-YY TO WS-RETENT-DATE-YY.
           ADD 3            TO WS-RETENT-DATE-MM-NUM.
           IF WS-RETENT-DATE-MM-NUM > 12
              COMPUTE WS-RETENT-DATE-MM-NUM = WS-RETENT-DATE-MM-NUM - 12
              ADD 1 TO WS-RETENT-DATE-YY-NUM.
           MOVE WS-RETENT-DATE  TO CL2-RETENTION-DATE.

           MOVE GREGORIAN-DATE        TO CL3-RUN-DATE.
           MOVE TIME-HH               TO CL3-RUN-TIME-HH.
           MOVE TIME-MM               TO CL3-RUN-TIME-MM.
           MOVE TIME-SS               TO CL3-RUN-TIME-SS.
           PERFORM 8100-PRINT-CONTROL-RPT-HDLINE THRU 8100-EXIT.

           MOVE SPACES                TO CONTROL-REPORT-DATA.
           MOVE WHEN-COMPILED         TO COMPILED-DATA.
           INSPECT COMPILED-DATA-TIME REPLACING ALL '.' BY ':'.
           STRING  'PROGRAM COMPILED ',
                   COMPILED-DATA-DATE,
                   ' AT ',
                   COMPILED-DATA-TIME
                   DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           ADD   1  TO WS-CNTR-LINE-CNT.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           MOVE SPACES               TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.
           ADD   1  TO WS-CNTR-LINE-CNT.

       1000-EXIT.
           EXIT.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH READS INPUT FILE
      ****************************************************************
       1100-READ-INPUT-FILE.

           READ JVOUCHER-STANDARD-FILE INTO UGAUA00I-RECORD
              AT END
                 MOVE 'Y' TO WS-END-OF-FILE
                 GO TO 1100-EXIT.

           IF UGAUA00I-RCRD-TYPE = '1'
              MOVE SPACES          TO WS-HOLD-UGAUA00I-HDR-REC
              MOVE ZEROES          TO WS-UGAUA00I-DCMNT-AMT
              MOVE UGAUA00I-RECORD TO WS-HOLD-UGAUA00I-HDR-REC
              ADD 1 TO WS-JVSTND-HDR-REC-READ.
           IF UGAUA00I-RCRD-TYPE = '2'
              MOVE SPACES          TO WS-UGAUA00I-DTL-REC
              MOVE ZEROES          TO WS-UGAUA00I-TRANS-AMT
              MOVE UGAUA00I-RECORD TO WS-UGAUA00I-DTL-REC
              ADD 1 TO WS-JVSTND-DTL-REC-READ.

       1100-EXIT.
           EXIT.

      /
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH CONTAIN THE MAIN LOGIC OF THE PROGRAM
      ****************************************************************

       2000-MAIN-PROCESS.

           PERFORM 1100-READ-INPUT-FILE THRU 1100-EXIT.
           IF END-OF-FILE
              GO TO 2000-EXIT.

           IF UGAUA00I-RCRD-TYPE = '1'
              PERFORM 5000-REVIEW-DOCUMENT THRU 5000-EXIT.

           IF UGAUA00I-RCRD-TYPE = '2'
              IF UGAUA00I-ACCT-CODE = '400000'
                 MOVE 'Y' TO WS-FUND-BLNC-ACCT-FLAG
                 PERFORM 3000-ACCUMUL-DCMNT-AMT-ADJST  THRU 3000-EXIT
                 PERFORM 4000-ACCUMUL-FUND-BLNC-AMT    THRU 4000-EXIT
              ELSE
                 MOVE 'Y' TO WS-OTHER-ACCT-FLAG
                 PERFORM 7000-WRITE-DTL-RECORD THRU 7000-EXIT.

       2000-EXIT.
           EXIT.

      *******************************
       3000-ACCUMUL-DCMNT-AMT-ADJST.

           COMPUTE WS-DCMNT-AMT-ADJST-TOT =
                   WS-DCMNT-AMT-ADJST-TOT + UGAUA00I-TRANS-AMT.

       3000-EXIT.
           EXIT.

      ********************************
       4000-ACCUMUL-FUND-BLNC-AMT.

           IF UGAUA00I-DEBIT-CRDT-IND = 'C'
              COMPUTE WS-DCMNT-FUND-BLNC-TOT =
                      WS-DCMNT-FUND-BLNC-TOT + UGAUA00I-TRANS-AMT.

           IF UGAUA00I-DEBIT-CRDT-IND = 'D'
              COMPUTE WS-DCMNT-FUND-BLNC-TOT =
                      WS-DCMNT-FUND-BLNC-TOT - UGAUA00I-TRANS-AMT.

       4000-EXIT.
           EXIT.

      ***********************
       5000-REVIEW-DOCUMENT.

           IF FUND-BLNC-ACCT-FOUND
              IF WS-DCMNT-FUND-BLNC-TOT = ZERO
                 IF OTHER-ACCT-FOUND
                    PERFORM 7100-WRITE-HDR-RECORD THRU 7100-EXIT
                 ELSE
                    NEXT SENTENCE
              ELSE
                 PERFORM 7000-WRITE-DTL-RECORD THRU 7000-EXIT
                 PERFORM 7100-WRITE-HDR-RECORD THRU 7100-EXIT
           ELSE
              PERFORM 7100-WRITE-HDR-RECORD THRU 7100-EXIT.

           MOVE SPACES TO WS-FUND-BLNC-ACCT-FLAG
                          WS-OTHER-ACCT-FLAG.
           MOVE ZEROES TO WS-DCMNT-FUND-BLNC-TOT
                          WS-DCMNT-AMT-ADJST-TOT.

       5000-EXIT.
           EXIT.

      ************************
       7000-WRITE-DTL-RECORD.

           IF FUND-BLNC-ACCT-FOUND
              MOVE WS-DCMNT-FUND-BLNC-TOT TO WS-UGAUA00I-TRANS-AMT
              IF WS-DCMNT-FUND-BLNC-TOT > ZERO
                 MOVE 'C' TO WS-UGAUA00I-DEBIT-CRDT-IND
              ELSE
                 MOVE 'D' TO WS-UGAUA00I-DEBIT-CRDT-IND.

           MOVE SPACES TO JV-CONSLDT-REC.
           WRITE JV-CONSLDT-REC FROM WS-UGAUA00I-DTL-REC.
           ADD 1 TO WS-JVCNSL-DTL-REC-WR.
           ADD 1 TO WS-JVCNSL-TOT-REC-WR.

       7000-EXIT.
           EXIT.

      ************************
       7100-WRITE-HDR-RECORD.

           IF FUND-BLNC-ACCT-FOUND
              COMPUTE WS-UGAUA00I-DCMNT-AMT =
                      WS-UGAUA00I-DCMNT-AMT - WS-DCMNT-AMT-ADJST-TOT
              IF WS-DCMNT-FUND-BLNC-TOT > ZERO
                 COMPUTE WS-UGAUA00I-DCMNT-AMT =
                         WS-UGAUA00I-DCMNT-AMT + WS-DCMNT-FUND-BLNC-TOT
              ELSE
                 COMPUTE WS-UGAUA00I-DCMNT-AMT =
                         WS-UGAUA00I-DCMNT-AMT +
                                          (-1) * WS-DCMNT-FUND-BLNC-TOT.

           MOVE SPACES TO JV-CONSLDT-REC.
           WRITE JV-CONSLDT-REC FROM WS-HOLD-UGAUA00I-HDR-REC.
           ADD 1 TO WS-JVCNSL-HDR-REC-WR.
           ADD 1 TO WS-JVCNSL-TOT-REC-WR.

       7100-EXIT.
           EXIT.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL PRINT THE CONTROL REPORT.
      ****************************************************************

       8000-WRITE-CONTROL-REPORT.

           IF WS-CNTR-LINE-CNT > 55
              PERFORM 8100-PRINT-CONTROL-RPT-HDLINE THRU 8100-EXIT
              MOVE SPACES               TO CONTROL-REPORT-DATA
              WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA
              ADD 1 TO WS-CNTR-LINE-CNT.

           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.
           ADD 1 TO WS-CNTR-LINE-CNT.

       8000-EXIT.
           EXIT.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL PRINT THE CONTROL REPORT
      *  HEADINGS
      ****************************************************************

       8100-PRINT-CONTROL-RPT-HDLINE.

           ADD 1                     TO WS-CNTR-PAGE-CNT.
           MOVE WS-CNTR-PAGE-CNT     TO CL1-PAGE-NUMBER.

           MOVE CONTROL-LINE-1       TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM
                 CONTROL-REPORT-DATA AFTER TOP-OF-PAGE.

           MOVE CONTROL-LINE-2       TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           MOVE CONTROL-LINE-3       TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           MOVE 3                   TO WS-CNTR-LINE-CNT.

       8100-EXIT.
           EXIT.

      /
      ****************************************************************
      *  THE FOLLOWING SECTION WILL HANDLE END OF PROGRAM PROCESSING
      ****************************************************************

       9000-END-OF-PROGRAM.

           COPY PDTIMEC2.

           PERFORM 9100-PUBLISH-STATISTICS THRU 9100-EXIT.

           MOVE SPACES               TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           MOVE 'PROGRAM STATUS' TO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           STRING   'PROGRAM HAS BEEN SUCCESSFULLY COMPLETED AT ',
                    TIME-HH, ':',
                    TIME-MM, ':',
                    TIME-SS
                    DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           PERFORM 9900-CLOSE-FILES THRU 9900-EXIT.

       9000-EXIT.
           EXIT.

      ************************
       9100-PUBLISH-STATISTICS.

           MOVE  'PROCESS STATISTICS' TO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           STRING 'JOURNAL VOUCHERS HEADER RECORDS READ       = ',
                  WS-JVSTND-HDR-REC-READ
                  DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           STRING 'JOURNAL VOUCHERS DETAIL RECORDS READ       = ',
                  WS-JVSTND-DTL-REC-READ
                  DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           MOVE SPACES               TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           STRING 'JOURNAL VOUCHERS HEADER RECORDS WRITTEN    = ',
                  WS-JVCNSL-HDR-REC-WR
                  DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           STRING 'JOURNAL VOUCHERS DETAIL RECORDS WRITTEN    = ',
                  WS-JVCNSL-DTL-REC-WR
                  DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           MOVE SPACES               TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           STRING 'TOTAL RECORDS WRITTEN                      = ',
                  WS-JVCNSL-TOT-REC-WR
                  DELIMITED BY SIZE INTO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

       9100-EXIT.
           EXIT.

      ************************

       9900-CLOSE-FILES.

           MOVE SPACES               TO CONTROL-REPORT-DATA.
           WRITE CONTROL-REPORT-RECORD FROM CONTROL-REPORT-DATA.

           MOVE ALL 'END OF REPORT ' TO CONTROL-REPORT-DATA.
           PERFORM 8000-WRITE-CONTROL-REPORT THRU 8000-EXIT.

           CLOSE JVOUCHER-STANDARD-FILE
                 JVOUCHER-CONSLDT-FILE
                 CONTROL-REPORT-FILE.

       9900-EXIT.
           EXIT.

