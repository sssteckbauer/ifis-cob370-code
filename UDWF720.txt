000100 IDENTIFICATION DIVISION.
000200 PROGRAM-ID.    UDWF720.
000300 AUTHOR.        ADCOM SYSTEMS.
000400 DATE-WRITTEN.  MAY 1995.
000500 DATE-COMPILED.
000600******************************************************************
000700******************************************************************
000800**    EXTRACT IFIS OPERATING LEDGER ACCT PERIOD ZERO(BAL FORW     ARD)
000900**    OR PRIOR YEAR TO A SYBASE FILE.
001000******************************************************************
001500**                                                              **
001600** INPUT:                                                       **
001700**           R4199-EXT-RECROD FROM UDWF720A                     **
001800**                                                              **
001900** OUTPUT:                                                      **
002000** CUMBAL    EXTRACT FILE CONTAINING IFIS OPERATING LEDGER DATA.**
002100**                                                              **
002300**                                                              **
002400******************************************************************
002500**                   CHANGE HISTORY                             **
002600**                                                              **
002700**  REF  AUTH    DATE     DESCRIPTION                  CSR NO.  **
002800**  ---  ----   ------   ---------------------------  --------- **
002900**       DFR   05/26/95  ORIGINAL VERSION OF PGM.      CDS005   **
002900**       BAC   09/05/96  CHANGED WHAT DATES ARE USED   FFI044   **
      **                       TO LOOK UP THE FUND EFVTY
002900**       BAC   09/16/96  PLACE SOURCE ON ENDEAVOR       FI098   **
002900**       NG    03/04/97  Y2K MODIFICATIONS              GA314   **
002900**       SLT   07/17/97  Y2K FIX                        GA314   **
003310**       DMW   08/24/98  PROCESSING OF DWCB001 HAS CHANGED      **
003320**                       FOR THE PROCESSING SPEED       DW018   **
003000**                                                              **
003410**       STEP1. UDWF722-  EXTRACT DBKEYS & RETURN KEY OF R4199  **
003420**       STEP2. SORT   -  THE FILE BY FUND                      **
003430**       STEP3. UDWF722B - EXCLUDE RECORDS WHICH IS NOT         **
003440**                         GRANT                                **
003450**       STEP4. SORT   -  SORT BY DBKEY                         **
003460**       STEP5. UDWF720A  EXTRACT R4199 FOR BUDGET & FINALCIAL  **
003470**       STEP6. SORT      SORT BY AFOAP ORDER                   **
003480**       STEP7. UDWF720   SUMMARIZE RECORDS BY AFOAP            **
003490**
003491**                        ALSO, PUT A BREAK ON LOCATION CODE    **
003100******************************************************************
003200******************************************************************
003300     EJECT
003400 ENVIRONMENT DIVISION.
003500 CONFIGURATION SECTION.
003600 SOURCE-COMPUTER.  ES9000.
003700 OBJECT-COMPUTER.  ES9000.
003800
003900 INPUT-OUTPUT SECTION.
004000 FILE-CONTROL.
004100
004600     SELECT  R4199-EXTRACT-FILES ASSIGN TO R4199EXT.
004700     SELECT  CUMBAL-FILE         ASSIGN TO CUMBAL.
004500
004900
005000 DATA DIVISION.
005200
005500 FILE SECTION.
005600
005700******************************************************************
005800**   OPERATING LEDGER FILE                                      **
005900******************************************************************
006000
006400 FD  R4199-EXTRACT-FILES
006800     RECORDING  MODE    IS  F
006200     BLOCK CONTAINS 0 RECORDS
006300     LABEL RECORDS ARE STANDARD.
006400
006900 01  R4199-RECORD           PIC X(85).
006600
006700 FD  CUMBAL-FILE
006800     RECORDING  MODE    IS  F
006900     LABEL   RECORDS   ARE  STANDARD
007000     BLOCK   CONTAINS    0  RECORDS
007100     RECORD  CONTAINS   85  CHARACTERS
007200     DATA    RECORD     IS  CUMBAL-RECORD.
007300
007400 01  CUMBAL-RECORD                PIC  X(85).
007500
008800     EJECT
008900******************************************************************
009000 WORKING-STORAGE SECTION.
009100******************************************************************
009200     SKIP3
009600
009700******************************************************************
009800**   CUMBAL RECORD LAYOUT                                        *
009900******************************************************************
010000 01  WS-CUMBAL-RECORD.
013100     05  WS-FISCAL-YR-CB                PIC  9(002)  VALUE ZEROES.
011400     05     FILLER                      PIC  X(001)  VALUE '	'.
013200     05  WS-ACCT-INDX-CODE-CB           PIC  X(010)  VALUE SPACES.
013300     05     FILLER                      PIC  X(001)  VALUE '	'.
013400     05  WS-FUND-CODE-CB                PIC  X(006)  VALUE SPACES.
013500     05     FILLER                      PIC  X(001)  VALUE '	'.
013600     05  WS-ORGZN-CODE-CB               PIC  X(006)  VALUE SPACES.
013700     05     FILLER                      PIC  X(001)  VALUE '	'.
013800     05  WS-ACCT-CODE-CB                PIC  X(006)  VALUE SPACES.
013900     05     FILLER                      PIC  X(001)  VALUE '	'.
014000     05  WS-PRGRM-CODE-CB               PIC  X(006)  VALUE SPACES.
014100     05     FILLER                      PIC  X(001)  VALUE '	'.
014200     05  WS-LCTN-CODE-CB                PIC  X(006)  VALUE SPACES.
014300     05     FILLER                      PIC  X(001)  VALUE '	'.
011500     05     FILLER                      PIC  X(001)  VALUE '$'.
011600     05  WS-BDGT-ADJMT-AMT              PIC +9(010).99.
011700     05     FILLER                      PIC  X(001)  VALUE '	'.
011800     05     FILLER                      PIC  X(001)  VALUE '$'.
011900     05  WS-FINANCIAL-AMT               PIC +9(010).99.
011400     05     FILLER                      PIC  X(001)  VALUE '	'.
013100     05  WS-CC-FY-Y2K                   PIC  9(004)  VALUE ZEROES.
009700******************************************************************
009800**   CUMBAL RECORD HOLD                                          *
009900******************************************************************
010000 01  WS-CUMBAL-RECORD-HOLD.
013100     05  WS-FISCAL-YR-HOLD              PIC  9(002)  VALUE ZEROES.
013200     05  WS-ACCT-INDX-CODE-HOLD         PIC  X(010)  VALUE SPACES.
013400     05  WS-FUND-CODE-HOLD              PIC  X(006)  VALUE SPACES.
013600     05  WS-ORGZN-CODE-HOLD             PIC  X(006)  VALUE SPACES.
013800     05  WS-ACCT-CODE-HOLD              PIC  X(006)  VALUE SPACES.
014000     05  WS-PRGRM-CODE-HOLD             PIC  X(006)  VALUE SPACES.
014200     05  WS-LCTN-CODE-HOLD              PIC  X(006)  VALUE SPACES.
011600     05  WS-BDGT-ADJMT-AMT-HOLD      PIC S9(010)V99  VALUE ZEROS.
011900     05  WS-FINANCIAL-AMT-HOLD       PIC S9(010)V99  VALUE ZEROS.
014700     EJECT
018100******************************************************************
013602** INPUT RECORD LAYOUT
013603********************************************************************
013604 01  WS-4199-EXT-RECORD.
013605     05  FISCAL-YR-IN                   PIC  9(002).
013606     05     FILLER                      PIC  X(001).
013607     05  ACCT-INDX-CODE-IN              PIC  X(010).
013608     05     FILLER                      PIC  X(001).
013609     05  FUND-CODE-IN                   PIC  X(006).
013610     05     FILLER                      PIC  X(001).
013611     05  ORGZN-CODE-IN                  PIC  X(006).
013612     05     FILLER                      PIC  X(001).
013613     05  ACCT-CODE-IN                   PIC  X(006).
013614     05     FILLER                      PIC  X(001).
013615     05  PRGRM-CODE-IN                  PIC  X(006).
013616     05     FILLER                      PIC  X(001).
013617     05  LCTN-CODE-IN                   PIC  X(006).
013618     05     FILLER                      PIC  X(001).
013619     05     FILLER                      PIC  X(001).
013620     05  BDGT-ADJMT-AMT-IN              PIC  S9(010)V99.
013621     05     FILLER                      PIC  X(001).
013622     05     FILLER                      PIC  X(001).
013623     05  FINANCIAL-AMT-IN               PIC  S9(010)V99.
013624     05     FILLER                      PIC  X(001).
013625     05  CC-FY-Y2K-IN                   PIC  9(004).
013630
013700******************************************************************
018200**   PROGRAM SWITCHES                                           **
018300******************************************************************
018400 01  WS-SWITCHES.
018500     05  SW-ANY-MORE-CUMBALS             PIC  X(03)  VALUE 'YES'.
018600         88 NO-MORE-CUMBALS                          VALUE 'NO '.
018700     05  SW-CONTRACT-GRANT               PIC  X(03)  VALUE 'NO '.
018800         88 CONTRACT-GRANT                           VALUE 'YES'.
018700     05  SW-FIRST-TIME                   PIC  X(03)  VALUE 'YES'.
018800         88 FIRST-TIME                               VALUE 'YES'.
018900     SKIP3
019000******************************************************************
019100**   PROGRAM COUNTERS                                           **
019200******************************************************************
019300 01  WS-COUNTERS         COMP-3.
019600     05  CTR-R4199-READ                  PIC S9(07)     VALUE +0.
019700     05  CTR-CUMBAL-WRITTEN              PIC S9(07)     VALUE +0.
020000     05  CTR-R4199-USED                  PIC S9(07)     VALUE +0.
020000     05  CTR-TOTAL-BUDGET                PIC S9(15)V99  VALUE +0.
020000     05  CTR-TOTAL-FINANCIAL             PIC S9(15)V99  VALUE +0.
020100
020200 01  WS-COUNTERS-Z.
020500     05  CTR-R4199-READ-Z                PIC  ZZZZZZ9.
020600     05  CTR-CUMBAL-WRITTEN-Z            PIC  ZZZZZZ9.
020900     05  CTR-R4199-USED-Z                PIC  ZZZZZZ9.
021000**
021100******************************************************************
021200**   CURRENT TIME                                               **
021300******************************************************************
021400 01  WS-TIME.
021500     05  WS-TIME-HRS                     PIC  9(02)  VALUE ZEROES.
021600     05  WS-TIME-FLDS.
021700         10  WS-TIME-MM                  PIC  9(02)  VALUE ZEROES.
021800         10  WS-TIME-SS                  PIC  9(02)  VALUE ZEROES.
021900     05  WS-TIME-FLDS-N  REDEFINES  WS-TIME-FLDS
022000                                         PIC  9(04).
022100 01  WS-HOLD-TIME.
022200     05  WS-HOLD-TIME-HH                 PIC  X(02)  VALUE SPACES.
022300     05  WS-HOLD-TIME-MM                 PIC  X(02)  VALUE SPACES.
022400     05  WS-HOLD-TIME-SS                 PIC  X(02)  VALUE SPACES.
022500
022600******************************************************************
022700**   CURRENT DATE                                               **
022800******************************************************************
022900 01  WS-CURRENT-DATE.
023000     05  WS-CENTURY                      PIC  9(02)  VALUE ZEROES.
023100     05  WS-ACCT-DATE.
023200         10  WS-DATE-YEAR                PIC  9(02)  VALUE ZEROES.
023300         10  WS-DATE-MONTH               PIC  9(02)  VALUE ZEROES.
023400         10  WS-DATE-DAY                 PIC  9(02)  VALUE ZEROES.
023500
023600******************************************************************
023700**   HOLD FISCAL YEAR/ACCOUNTING PERIOD                    **
023800******************************************************************
023900 01  WS-END-FY.
006500     05 WS-FISCAL-CCYY.
006600        07 WS-FISCAL-CC-PARM       PIC 9(02)  VALUE ZEROES.
006700        07 WS-FISCAL-YR-PARM       PIC 9(02)  VALUE ZEROES.
006500     05 FILLER                     PIC 9(04)  VALUE 0630.
024300
024400******************************************************************
024500**   HOLD DBKEY.                                                **
024600******************************************************************
024700 01  WS-HOLD-DBKEYS.
024800     05  WS-HOLD-DBKEY     COMP          PIC S9(08)  VALUE +0.
024900     05  WS-HOLD-KEY.
025000         10  WS-HOLD-PAGE                PIC  9(08)  VALUE ZEROES.
025100         10  WS-HOLD-LINE                PIC  9(03)  VALUE ZEROES.
025200
025300******************************************************************
025400**   HOLD DBKEY IN SYBASE FORMAT                                **
025500******************************************************************
025600 01  WS-HOLD-DBKEY-SYBASE.
025700     05  WS-HOLD-KEY-SYBASE.
025800         10  WS-HOLD-SYBASE5             PIC  9(05)  VALUE ZEROES.
025900         10  WS-HOLD-SYBASE6             PIC  9(06)  VALUE ZEROES.
026000
026100******************************************************************
026200**   IDMS/R DICTIONARY NAME                                     **
026300******************************************************************
026400 01  HOLD-ACCT-DATA.
026500     05  HAD-DICTIONARY.
026600         10      FILLER                  PIC  X(02)  VALUE SPACES.
026700         10  HAD-DICT-NAME               PIC  X(08)  VALUE
026800                                                     'ISISDICT'.
026900     EJECT
027000******************************************************************
027100**   IDMS/R DICTIONARY RECORDS USED BY PROGRAM                  **
027200******************************************************************
027300
027500     EJECT
028400******************************************************************
028500 PROCEDURE DIVISION.
028600******************************************************************
028700
028800 MAIN-DRIVER.
028900
029000     DISPLAY  '>>>>  PROGRAM UDWF720 STARTING  <<<<'.
029100
029200     PERFORM  XX100-INITIALIZE.
029300
029400     PERFORM  AA100-MAINLINE   UNTIL   NO-MORE-CUMBALS.
029500
029600     PERFORM  ZZ100-END-OF-PROGRAM.
029700
029800     DISPLAY  '>>>>  PROGRAM UDWF720 FINISHED  <<<<'.
029900
030000     GOBACK.
030100     EJECT
030200******************************************************************
030300 AA100-MAINLINE.
030400******************************************************************
030500**   THIS PARAGRAPH IS THE MAIN PROCESSING PROGRAM DRIVER.      **
030600******************************************************************
030700
026400     PERFORM  AA120-WRITE-CUMBAL-R4199.
026410     PERFORM XX125-READ-DBKEY.

031400     IF NOT NO-MORE-CUMBALS
026450        ADD  +1                     TO  CTR-R4199-READ.
031600
044700******************************************************************
031800 AA120-WRITE-CUMBAL-R4199.
031900******************************************************************
032000**   THIS PARAGRAPH WILL MOVE THE R4199 INFO TO THE OUTPUT      **
032100**   RECORD AND WRITE THE RECORD.                               **
032200******************************************************************
032300
032300     IF FIRST-TIME
031700        MOVE  ACCT-INDX-CODE-IN     TO  WS-ACCT-INDX-CODE-HOLD
031800        MOVE  FUND-CODE-IN          TO  WS-FUND-CODE-HOLD
031900        MOVE  ORGZN-CODE-IN         TO  WS-ORGZN-CODE-HOLD
032000        MOVE  ACCT-CODE-IN          TO  WS-ACCT-CODE-HOLD
032100        MOVE  PRGRM-CODE-IN         TO  WS-PRGRM-CODE-HOLD
033400        MOVE  'NO '                 TO  SW-FIRST-TIME.


032500     IF ACCT-INDX-CODE-IN NOT = WS-ACCT-INDX-CODE-HOLD OR
032600        FUND-CODE-IN        NOT = WS-FUND-CODE-HOLD    OR
032700        ORGZN-CODE-IN       NOT = WS-ORGZN-CODE-HOLD   OR
032800        ACCT-CODE-IN        NOT = WS-ACCT-CODE-HOLD    OR
032900        PRGRM-CODE-IN       NOT = WS-PRGRM-CODE-HOLD   OR
032910        LCTN-CODE-IN        NOT = WS-LCTN-CODE-HOLD

032900        MOVE  WS-FISCAL-YR-PARM       TO  WS-FISCAL-YR-CB
032900        MOVE  WS-FISCAL-CCYY          TO  WS-CC-FY-Y2K
033000        MOVE  WS-ACCT-INDX-CODE-HOLD  TO WS-ACCT-INDX-CODE-CB
033100        MOVE  WS-FUND-CODE-HOLD       TO  WS-FUND-CODE-CB
033200        MOVE  WS-ORGZN-CODE-HOLD      TO  WS-ORGZN-CODE-CB
033300        MOVE  WS-ACCT-CODE-HOLD       TO  WS-ACCT-CODE-CB
033400        MOVE  WS-PRGRM-CODE-HOLD      TO  WS-PRGRM-CODE-CB
033500        MOVE  WS-LCTN-CODE-HOLD       TO  WS-LCTN-CODE-CB
              MOVE  WS-BDGT-ADJMT-AMT-HOLD  TO  WS-BDGT-ADJMT-AMT
              MOVE  WS-FINANCIAL-AMT-HOLD   TO  WS-FINANCIAL-AMT
034010
              IF    WS-BDGT-ADJMT-AMT-HOLD  = ZEROS AND
                    WS-FINANCIAL-AMT-HOLD   = ZEROS
                    NEXT SENTENCE
              ELSE
035900              WRITE  CUMBAL-RECORD    FROM  WS-CUMBAL-RECORD
                    ADD    +1               TO  CTR-CUMBAL-WRITTEN
                    ADD   WS-BDGT-ADJMT-AMT-HOLD TO CTR-TOTAL-BUDGET
                    ADD   WS-FINANCIAL-AMT-HOLD TO CTR-TOTAL-FINANCIAL
                    MOVE  ZEROS             TO  WS-BDGT-ADJMT-AMT-HOLD
                    MOVE  ZEROS             TO  WS-FINANCIAL-AMT-HOLD.


035300     MOVE  ACCT-INDX-CODE-IN     TO WS-ACCT-INDX-CODE-HOLD.
035400     MOVE  FUND-CODE-IN          TO  WS-FUND-CODE-HOLD.
035500     MOVE  ORGZN-CODE-IN         TO  WS-ORGZN-CODE-HOLD.
035600     MOVE  ACCT-CODE-IN          TO  WS-ACCT-CODE-HOLD.
035700     MOVE  PRGRM-CODE-IN         TO  WS-PRGRM-CODE-HOLD.
035800     MOVE  LCTN-CODE-IN          TO  WS-LCTN-CODE-HOLD.

034300     COMPUTE WS-BDGT-ADJMT-AMT-HOLD =
034300                                 WS-BDGT-ADJMT-AMT-HOLD +
036200                                 BDGT-ADJMT-AMT-IN
035500
034300     COMPUTE WS-FINANCIAL-AMT-HOLD =
034300                                 WS-FINANCIAL-AMT-HOLD +
039400                                 FINANCIAL-AMT-IN.
035500
053200******************************************************************
053300 XX100-INITIALIZE.
053400******************************************************************
053500**   THIS PARAGRAPH WILL OPEN THE OUTPUT FILES, BIND THE        **
053600**   DATABASE RECORDS, AND GET THE FIRST R4187 RECORD.          **
053700******************************************************************
053800
053900     ACCEPT WS-FISCAL-CCYY   FROM  SYSIN.
055800
055900     DISPLAY  '*****************************************'.
056000     DISPLAY  'PROCESSING CUMULATIVE BALANCE'.
056200     DISPLAY  '*****************************************'.
056300
043900     OPEN  INPUT  R4199-EXTRACT-FILES.
056500     OPEN  OUTPUT  CUMBAL-FILE.
056700
057100     PERFORM XX125-READ-DBKEY.
058800
059100     EJECT
059200******************************************************************
059400******************************************************************
059500**   THIS PARAGRAPH READS THE DBKEY FILE.                       **
059600******************************************************************
060500 XX125-READ-DBKEY.
060600
048400     READ R4199-EXTRACT-FILES INTO  WS-4199-EXT-RECORD
060800        AT END
060900             MOVE 'NO ' TO SW-ANY-MORE-CUMBALS.
061000
062000******************************************************************
062100 ZZ100-END-OF-PROGRAM.
062200******************************************************************
062300*  THE FOLLOWING PARAGRAPH WILL HANDLE END OF PROGRAM PROCESSING *
062400******************************************************************
062500
062400** WRITE OUT LAST CUM BAL RECORD    ******************************
030800     IF  CONTRACT-GRANT
032900       MOVE  WS-FISCAL-YR-PARM        TO  WS-FISCAL-YR-CB
032900       MOVE  WS-FISCAL-CCYY           TO  WS-CC-FY-Y2K
033000       MOVE  WS-ACCT-INDX-CODE-HOLD   TO WS-ACCT-INDX-CODE-CB
033100       MOVE  WS-FUND-CODE-HOLD        TO  WS-FUND-CODE-CB
033200       MOVE  WS-ORGZN-CODE-HOLD       TO  WS-ORGZN-CODE-CB
033300       MOVE  WS-ACCT-CODE-HOLD        TO  WS-ACCT-CODE-CB
033400       MOVE  WS-PRGRM-CODE-HOLD       TO  WS-PRGRM-CODE-CB
033500       MOVE  WS-LCTN-CODE-HOLD        TO  WS-LCTN-CODE-CB
             MOVE  WS-BDGT-ADJMT-AMT-HOLD   TO WS-BDGT-ADJMT-AMT
             MOVE  WS-FINANCIAL-AMT-HOLD    TO WS-FINANCIAL-AMT

             IF    WS-BDGT-ADJMT-AMT-HOLD   = ZEROS AND
                   WS-FINANCIAL-AMT-HOLD    = ZEROS
                      NEXT SENTENCE
             ELSE
035900            WRITE  CUMBAL-RECORD  FROM  WS-CUMBAL-RECORD
                  ADD   WS-BDGT-ADJMT-AMT-HOLD TO CTR-TOTAL-BUDGET
                  ADD   WS-FINANCIAL-AMT-HOLD TO CTR-TOTAL-FINANCIAL
                  ADD    +1             TO    CTR-CUMBAL-WRITTEN.


062600     CLOSE  CUMBAL-FILE
051800            R4199-EXTRACT-FILES.
062900
063200     MOVE  CTR-R4199-READ                TO  CTR-R4199-READ-Z.
063300     MOVE  CTR-CUMBAL-WRITTEN            TO
063400                                         CTR-CUMBAL-WRITTEN-Z.
063800     MOVE  CTR-R4199-USED                TO  CTR-R4199-USED-Z.
063900
064000     DISPLAY  '  '.
064100     DISPLAY  '*****************************************'.
064200     DISPLAY  'NBR OF R-4199-CUMBAL READ=====> '
064300                                            CTR-R4199-READ-Z.
064800     DISPLAY  '*****************************************'.
064900     DISPLAY  'NBR OF CUMBAL WRITTEN=========> '
065000                                      CTR-CUMBAL-WRITTEN-Z.
065300     DISPLAY  '*****************************************'.
064800     DISPLAY  '*****************************************'.
064900     DISPLAY  'TOTAL BUDGET AMOUNT===========> '
065000                                      CTR-TOTAL-BUDGET.
065300     DISPLAY  '*****************************************'.
064900     DISPLAY  'TOTAL FINANCIAL AMOUNT========> '
065000                                      CTR-TOTAL-FINANCIAL.
064800     DISPLAY  '*****************************************'.
066200
066400     EJECT
