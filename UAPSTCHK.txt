       IDENTIFICATION DIVISION.
       PROGRAM-ID. UAPSTCHK.
       AUTHOR.     M JEWETT.
       DATE-WRITTEN.  SEPT 1999.
       DATE-COMPILED.
      *REMARKS.
      *
      *    THIS PROGRAM WILL READ A FILE CONTAINING A CHECK ORIGIN CODE
      *    AND THE PROCESSING STATUS (GO, INACTIVE OR STOP). ANY
      *    CHECK ORIGIN WITHOUT AN INACTIVE STATUS ABENDS THIS PROGRAM.
      *
      *    THIS PROGRAM SHOULD RUN BEFORE PHASE 1 OF CHECKWRITE
      *
      *    INPUT:
      *          STATUS01      :  RECORD SHOWING ORIGIN AND STATUS
      *
      *    OUTPUT:
      *
      ******************************************************************
      * MODIFICATIONS:
      ******************************************************************

       ENVIRONMENT DIVISION.                                            00320000
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT STATUS01-FILE ASSIGN TO STATUS01.

       DATA DIVISION.

       FILE SECTION.
       FD  STATUS01-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 80 CHARACTERS
           DATA RECORD IS REPORT01-RECORD.

       01  STATUS01-RECORD               PIC X(80).

       WORKING-STORAGE SECTION.
      ****************************************************************
      *  HOLD FIELDS AND FLAGS                                       *
      ****************************************************************

       01  STATUS-RECORD.
           05  FILLER                     PIC X(06).
           05  WS-CHECK-ORGIN             PIC X(07).
           05  FILLER                     PIC X(06).
           05  WS-STATUS                  PIC X(08).
           05  FILLER                     PIC X(42).

      *****************************************************************
       PROCEDURE DIVISION.

       0000-START.

           PERFORM 2000-READ-STATUS    THRU 2000-EXIT.                  03170000

       9000-END-OF-JOB.

      *    FINISH.
           STOP RUN.

      *****************************************************************
      *                      SUBROUTINE  DEFINITION                   *
      *****************************************************************

       2000-READ-STATUS.

           OPEN INPUT STATUS01-FILE.

           READ STATUS01-FILE INTO STATUS-RECORD.

           DISPLAY 'WS-CHECK-ORGIN: ' WS-CHECK-ORGIN
                   ' STATUS: ' WS-STATUS.

           IF WS-STATUS NOT EQUAL 'INACTIVE'
              MOVE 0016 TO RETURN-CODE
              DISPLAY '    FOUND BAD STATUS-CANNOT START PROCESSING'
              GO TO 2000-EXIT.

           CLOSE STATUS01-FILE.

       2000-EXIT.
           EXIT.

