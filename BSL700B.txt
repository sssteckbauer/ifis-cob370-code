       IDENTIFICATION DIVISION.
       PROGRAM-ID.    BSL700B.
       AUTHOR.        ANNIE FLAGER.
       DATE-WRITTEN.  JULY 17, 1995.
       DATE-COMPILED.
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *                                                               *
      *                            BSL700B                            *
      *                                                               *
      *   BSL BUDGET CONTROL, BUDGET TABLES, AND BUDGET MASTER FILES  *
      *       EXTRACT FOR CLIENT/SERVER DATA WAREHOUSE (DARWIN)       *
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *                CHANGE LOG                                     *
      * 03/12/97 MLJ - GET VICE CHANCELLOR CODE                  BP073*
      * 03/19/99 IA  - CORRECT VICE CHANCELLOR CODE PROCESSING   BP093*
      * 10/05/02 DMW - EXTRACT SAU CODE                       ACR33449
      * 09/14/09 MJM - INCREASE VC-ORGN-TABLE SIZE FROM 600 - 900
      * 04/11/17 MLJ - INCREASE VC-ORGN-TABLE SIZE                    *
      *****************************************************************

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SOURCE-COMPUTER.               IBM-3090.
       OBJECT-COMPUTER.               IBM-3090.


       INPUT-OUTPUT SECTION.

       FILE-CONTROL.

           SELECT PRMCTL-FILE           ASSIGN TO UT-S-PRMCTLI.
           SELECT BUDCTLI-FILE          ASSIGN TO UT-S-BUDCTLI.
           SELECT BUDGET-TABLES-FILE    ASSIGN TO UT-S-BUDTBLI.
           SELECT BUDMSTI-FILE          ASSIGN TO UT-S-BUDMSTI.
           SELECT BUDCTLO-FILE          ASSIGN TO UT-S-BUDCTLO.
           SELECT BUDSCTO-FILE          ASSIGN TO UT-S-BUDSCTO.
           SELECT BUDSBTO-FILE          ASSIGN TO UT-S-BUDSBTO.
           SELECT BUDVCTO-FILE          ASSIGN TO UT-S-BUDVCTO.
           SELECT BUDMSTO-FILE          ASSIGN TO UT-S-BUDMSTO.
           SELECT BUDXTN-FILE           ASSIGN TO UT-S-BUDXTNO.
      /
       DATA DIVISION.

       FILE SECTION.

       FD  PRMCTL-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 80 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS PRMCTL-REC.

       01  PRMCTL-REC                     PIC  X(080).


       FD  BUDCTLI-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 80 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDCTLI-REC.

       01  BUDCTLI-REC                    PIC  X(080).


       FD  BUDGET-TABLES-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 80 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDTBL-REC.

       01  BUDTBL-REC                     PIC  X(080).


       FD  BUDMSTI-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 466 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDMSTI-REC.

       01  BUDMSTI-REC                    PIC  X(466).


       FD  BUDCTLO-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 34 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDCTLO-REC.

       01  BUDCTLO-REC                    PIC  X(034).


       FD  BUDSCTO-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 47 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDSCTO-REC.

       01  BUDSCTO-REC                    PIC  X(047).


       FD  BUDSBTO-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 48 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDSBTO-REC.

       01  BUDSBTO-REC                    PIC  X(048).


       FD  BUDVCTO-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 47 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDVCTO-REC.

       01  BUDVCTO-REC                    PIC  X(047).


       FD  BUDMSTO-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
DMW001     RECORD CONTAINS 472 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDMSTO-REC.

DMW001 01  BUDMSTO-REC                    PIC  X(472).


       FD  BUDXTN-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 106 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS BUDXTN-REC.

       01  BUDXTN-REC                     PIC  X(106).

      /
       WORKING-STORAGE SECTION.

       01  PARAMETER-CONTROL-RECORD.
           05  PRM-FISCAL-YEAR             PIC  9(002).
           05  FILLER                      PIC  X(078).
      /
       01  SYSTEM-CONTROL-RECORD.      COPY BUDSCNTL.
      /
       01  BUDGET-TABLES-RECORD.       COPY CPWSXBTB.
      /
       01  BUDGET-MASTER-RECORD        PIC X(466).
                                       COPY BUDMAST.
      /
       01  BUDGET-CONTROL-RECORD-OUT.
           05  BC-CURR-CYCLE-NO            PIC  9(003).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BC-EDIT-CYCLE-NO            PIC  9(003).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BC-CURR-PROC-DATE           PIC  9(008).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BC-FISCAL-YEAR              PIC  9(002).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BC-REFRESH-DATE-TIME        PIC  X(014).
      /
       01  SUB-CAMPUS-TABLE-RECORD.
           05  SCT-CODE                    PIC  X(001).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  SCT-TITLE                   PIC  X(030).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  SCT-REFRESH-DATE-TIME       PIC  X(014).
      /
       01  SUB-BUDGET-TABLE-RECORD.
           05  SBT-CODE                    PIC  X(002).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  SBT-TITLE                   PIC  X(030).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  SBT-REFRESH-DATE-TIME       PIC  X(014).
      /
       01  VC-DISTR-TABLE-RECORD.
           05  VCT-DISTR-CODE              PIC  X(001).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  VCT-DISTR-TITLE             PIC  X(030).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  VCT-REFRESH-DATE-TIME       PIC  X(014).
      /
       01  VC-ORGN-TABLE-RECORD.
DEVMLJ     05  VC-ORGN-TABLE               OCCURS 1100 TIMES.
               10  ORGN-FROM               PIC X(006).
               10  ORGN-TO                 PIC X(006).
               10  VC-DIS-CODE             PIC X(001).

DEVMLJ 01  VC-SUB                          PIC 9(004).

       01  BUDGET-MASTER-RECORD-OUT.
           05  BUDMST-ID                   PIC  9(007).
           05  FILLER                      PIC  X(001)  VALUE '	'.
DMW001     05  BM-SAU-CODE                 PIC  X(001)  VALUE ' '.
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BUDMST-ORIG-KEY.
               10  BM-SUB-CAMPUS           PIC  X(001).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-RECORD-TYPE          PIC  X(001).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-ACCOUNT              PIC  X(007).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-FUND                 PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-SUB-BUDGET           PIC  X(002).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BM-CYCLE-NO                 PIC  9(003).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BUDMST-ATTRIBUTES.
               10  BM-ATTRB-SUB-CAMPUS     PIC  X(001).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-ATTRB-FUND           PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-ATTRB-FUND-TYPE      PIC  X(001).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-ATTRB-SUB-BUDGET     PIC  X(002).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BUDMST-AMOUNTS.
               10  BM-INIT-AMOUNT          PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-PREV-YR-ADJTD-AMT    PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BUDMST-APPROPRIATION-FTE.
               10  BM-INIT-FTE             PIC +9(004).99.
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-PREV-YR-ADJTD-FTE    PIC +9(004).99.
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BM-ACCOUNT-TITLE            PIC  X(035).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BUDMST-IFIS-IFOAPAL.
               10  BM-IFIS-INDEX           PIC  X(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-FUND            PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-ORG             PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-ACCOUNT         PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-PROGRAM         PIC  X(006).
      **       10  FILLER                  PIC  X(001)  VALUE '	'.
      **       10  BM-IFIS-ACTIVITY        PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-LOCATION        PIC  X(006).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BUDMST-IFIS-TITLES.
               10  BM-IFIS-INDEX-TITLE     PIC  X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-FUND-TITLE      PIC  X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-ORG-TITLE       PIC  X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-ACCOUNT-TITLE   PIC  X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-PROGRAM-TITLE   PIC  X(035).
      **       10  FILLER                  PIC  X(001)  VALUE '	'.
      **       10  BM-IFIS-ACTIVITY-TITLE PIC   X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-IFIS-LOCATION-TITLE PIC   X(035).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BUDMST-SUMMARY-AMOUNTS.
               10  BM-SUMM-ADJ-AMOUNT      PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-SUMM-ADJTD-AMOUNT    PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-SUMM-INCRMTD-AMOUNT  PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BUDMST-SUMMARY-FTE.
               10  BM-SUMM-ADJ-FTE         PIC +9(004).99.
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-SUMM-ADJTD-FTE       PIC +9(004).99.
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BM-SUMM-INCRMTD-FTE     PIC +9(004).99.
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BM-VICE-CHNCLLR-DISTR-CODE  PIC  X(001).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BM-FISCAL-YEAR              PIC  9(002).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BM-REFRESH-DATE-TIME        PIC  X(014).
      /
       01  BUDGET-TRANSACTION-RECORD.
           05  BUDXTN-ID                   PIC  9(007).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BUDXTN-ORIG-KEY.
               10  BTR-DOCUMENT-NO         PIC  X(005).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BTR-DOCUMENT-SUFFIX     PIC  9(002).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BTR-TRANSACTION-CLASS   PIC  X(001).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BTR-TRANSACTION-TYPE    PIC  X(002).
               10  FILLER                  PIC  X(001)  VALUE '	'.
               10  BTR-DETAIL-AMOUNT       PIC +9(010).
               10  FILLER                  PIC  X(001)  VALUE '	'.
           05  BTR-DETAIL-FTE              PIC +9(004).99.
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BTR-DETAIL-TEXT             PIC  X(030).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BTR-TRANSACTION-DATE        PIC  9(008).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BTR-POSTING-DATE            PIC  9(008).
           05  FILLER                      PIC  X(001)  VALUE '	'.
           05  BTR-REFRESH-DATE-TIME       PIC  X(014).
      /
       01  MISC-FIELDS.
           05  FILLER                  PIC X(010)   VALUE 'MISC FLDS:'.
           05  W-DATE1.
               10  W-YY1               PIC 9(002).
               10  W-MM1               PIC 9(002).
               10  W-DD1               PIC 9(002).
           05  W-DATE2.
               10  W-CC2               PIC 9(002).
               10  W-YY2               PIC 9(002).
               10  W-MM2               PIC 9(002).
               10  W-DD2               PIC 9(002).
           05  W-TIME1.
               10  W-HRS1              PIC 9(002).
               10  W-MIN1              PIC 9(002).
               10  W-SEC1              PIC 9(002).
           05  W-TIME2.
               10  W-T2-SPC            PIC X(001).
               10  W-HRS2              PIC 9(002).
               10  W-T2-COLON          PIC X(001).
               10  W-MIN2              PIC 9(002).
           05  W-SYS-DATE-TIME-HOLD.
               10  W-SYS-DATE-HOLD     PIC 9(008).
               10  W-SYS-TIME-HOLD     PIC X(006).
           05  W-CYCLE-NO              PIC 9(003).
           05  W-ID.
               10  W-ID-FISCYR         PIC 9(002).
               10  W-ID-CALC-NO        PIC 9(005).
           05  W-BUDINT-KEY-HOLD       PIC X(021).
           05  W-PREV-BUDINT-KEY       PIC X(021).
           05  W-PREV-BUDXTN-REC       PIC X(222).

       01  FLAGS.
           05  FILLER                  PIC  X(006)   VALUE 'FLAGS:'.
           05  PRMCTLI-FLAG            PIC  X(001)   VALUE 'N'.
               88  END-OF-PRMCTLI                    VALUE 'Y'.
           05  BUDCTLI-FLAG            PIC  X(001)   VALUE 'N'.
               88  END-OF-BUDCTLI                    VALUE 'Y'.
           05  BUDTBLI-FLAG            PIC  X(001)   VALUE 'N'.
               88  END-OF-BUDTBLI                    VALUE 'Y'.
           05  BUDMSTI-FLAG            PIC  X(001)   VALUE 'N'.
               88  END-OF-BUDMSTI                    VALUE 'Y'.
           05  BUDMST-DATA-MOVED-FLAG  PIC  9(001)   VALUE 0.
               88  BUDINT-DATA-MOVED-ONLY            VALUE 1.
               88  BUDINT-AND-BUDSUM-DATA-MOVED      VALUE 2.
           05  VC-TABLE-MATCH-FLAG     PIC  X(001)   VALUE 'N'.
               88  VC-TABLE-MATCH                    VALUE 'Y'.

       01  COUNTERS.
           05  FILLER                  PIC  X(009)   VALUE 'COUNTERS:'.
           05  PRMCTL-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDCTLI-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDTBLI-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDMSTI-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDTBL-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDHDR-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDINT-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDSUM-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDCTLO-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDSCTO-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDSBTO-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDVCTO-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDMSTO-CNTR   COMP-3   PIC S9(007)   VALUE ZEROES.
           05  BUDXTN-CNTR    COMP-3   PIC S9(007)   VALUE ZEROES.

       01  EDIT-COUNTERS.
           05  FILLER                  PIC  X(014)   VALUE
                                                     'EDIT COUNTERS:'.
           05  PRMCTL-ECNTR            PIC  9(007)   VALUE ZEROES.
           05  BUDCTLI-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDTBLI-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDMSTI-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDTBL-ECNTR            PIC  9(007)   VALUE ZEROES.
           05  BUDHDR-ECNTR            PIC  9(007)   VALUE ZEROES.
           05  BUDINT-ECNTR            PIC  9(007)   VALUE ZEROES.
           05  BUDSUM-ECNTR            PIC  9(007)   VALUE ZEROES.
           05  BUDCTLO-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDSCTO-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDSBTO-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDVCTO-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDMSTO-ECNTR           PIC  9(007)   VALUE ZEROES.
           05  BUDXTN-ECNTR            PIC  9(007)   VALUE ZEROES.
      /
       PROCEDURE DIVISION.

       MAINLINE.

            PERFORM  A000-INIT.

            PERFORM  B000-PROCESS-BUDGET-CONTROL  THRU  B000-EXIT
                          UNTIL  END-OF-BUDCTLI.

            MOVE 1 TO VC-SUB.
            PERFORM  C000-PROCESS-BUDGET-TABLES   THRU  C000-EXIT
                          UNTIL  END-OF-BUDTBLI.

            PERFORM  D000-PROCESS-BUDGET-MASTER   THRU  D000-EXIT
                          UNTIL  END-OF-BUDMSTI.

            PERFORM  E000-EOJ.

            STOP RUN.
      /
       A000-INIT.

            OPEN  INPUT  PRMCTL-FILE
                         BUDCTLI-FILE
                         BUDGET-TABLES-FILE
                         BUDMSTI-FILE
                  OUTPUT BUDCTLO-FILE
                         BUDSCTO-FILE
                         BUDSBTO-FILE
                         BUDVCTO-FILE
                         BUDMSTO-FILE
                         BUDXTN-FILE.

            PERFORM  A100-PROCESS-PARAMETERS      THRU  A100-EXIT
                          UNTIL  END-OF-PRMCTLI.

            PERFORM F200-READ-BUDCTLI-REC.
            IF END-OF-BUDCTLI
                DISPLAY 'EMPTY BUDGET CONTROL FILE'
                GO TO Z000-ABEND-RTN.

            PERFORM F300-READ-BUDTBLI-REC.
            IF END-OF-BUDTBLI
                DISPLAY 'EMPTY BUDGET TABLES FILE'
                GO TO Z000-ABEND-RTN.

            PERFORM F400-READ-BUDMSTI-REC.
            IF END-OF-BUDMSTI
                DISPLAY 'EMPTY BUDGET MASTER FILE'
                GO TO Z000-ABEND-RTN.

            ACCEPT  W-DATE1  FROM DATE.
            PERFORM G000-FORMAT-DATE.
            MOVE  W-DATE2             TO W-SYS-DATE-HOLD.

            ACCEPT  W-TIME1  FROM TIME.
            PERFORM G100-FORMAT-TIME.
            MOVE  W-TIME2             TO W-SYS-TIME-HOLD.

            MOVE  ZEROES              TO W-ID.

            MOVE  SPACES              TO W-PREV-BUDINT-KEY,
                                         W-PREV-BUDXTN-REC.


       A100-PROCESS-PARAMETERS.

            PERFORM F100-READ-PRMCTLI-REC.

            IF PRM-FISCAL-YEAR NUMERIC
                NEXT SENTENCE
            ELSE
                DISPLAY 'INVALID FISCAL YEAR = ' PRM-FISCAL-YEAR
                GO TO Z000-ABEND-RTN.

       A100-EXIT.
            EXIT.
      /
       B000-PROCESS-BUDGET-CONTROL.

            PERFORM B100-PROCESS-BUDCTL-DATA.
            PERFORM F500-WRITE-BUDCTLO-REC.
            PERFORM F200-READ-BUDCTLI-REC.

       B000-EXIT.
            EXIT.


       B100-PROCESS-BUDCTL-DATA.

            MOVE  SYS-CURR-CYCLE-NO     TO BC-CURR-CYCLE-NO.
            MOVE  SYS-EDIT-CYCLE-NO     TO BC-EDIT-CYCLE-NO.

            MOVE  SYS-CURR-PROC-DATE    TO W-DATE1.
            PERFORM G000-FORMAT-DATE.
            MOVE  W-DATE2               TO BC-CURR-PROC-DATE.

            MOVE  PRM-FISCAL-YEAR       TO BC-FISCAL-YEAR.
            MOVE  W-SYS-DATE-TIME-HOLD  TO BC-REFRESH-DATE-TIME.
      /
       C000-PROCESS-BUDGET-TABLES.

            IF TAB-TABLE-NO = '011'
                PERFORM C100-PROCESS-BUDSCT-DATA
                PERFORM F600-WRITE-BUDSCTO-REC.

            IF TAB-TABLE-NO = '004'
                PERFORM C200-PROCESS-BUDSBT-DATA
                PERFORM F610-WRITE-BUDSBTO-REC.

            IF TAB-TABLE-NO = '040'
                PERFORM C300-PROCESS-BUDVCT-DATA
                PERFORM F620-WRITE-BUDVCTO-REC.

            IF TAB-TABLE-NO = '041'
                PERFORM C400-PROCESS-VCORGN-DATA.

      *     MOVE '041' TO TABLE-NO-ARG.
      *     PERFORM 9000-BUDGET-TABLE-LOAD.
      *     IF TABLE-LOAD-ERROR
      *        DISPLAY 'ERROR LOADING BUDGET TABLE 041'
      *        GO TO Z000-ABEND-RTN.

            PERFORM F300-READ-BUDTBLI-REC.

       C000-EXIT.
            EXIT.


       C100-PROCESS-BUDSCT-DATA.

            IF TAB-RANGE-FROM = TAB-RANGE-TO
                MOVE  TAB-RANGE-FROM    TO SCT-CODE.

            MOVE  TAB-CLASS-TITLE       TO SCT-TITLE.

            MOVE  W-SYS-DATE-TIME-HOLD  TO SCT-REFRESH-DATE-TIME.

            COMPUTE  BUDTBL-CNTR  =  BUDTBL-CNTR + 1.


       C200-PROCESS-BUDSBT-DATA.

            IF TAB-RANGE-FROM = TAB-RANGE-TO
                MOVE  TAB-RANGE-FROM    TO SBT-CODE.

            MOVE  TAB-CLASS-TITLE       TO SBT-TITLE.

            MOVE  W-SYS-DATE-TIME-HOLD  TO SBT-REFRESH-DATE-TIME.

            COMPUTE  BUDTBL-CNTR  =  BUDTBL-CNTR + 1.


       C300-PROCESS-BUDVCT-DATA.

            IF TAB-RANGE-FROM = TAB-RANGE-TO
                MOVE  TAB-RANGE-FROM    TO VCT-DISTR-CODE.

            MOVE  TAB-CLASS-TITLE       TO VCT-DISTR-TITLE.

            MOVE  W-SYS-DATE-TIME-HOLD  TO VCT-REFRESH-DATE-TIME.

            COMPUTE  BUDTBL-CNTR  =  BUDTBL-CNTR + 1.

       C400-PROCESS-VCORGN-DATA.

DEVMLJ      IF  VC-SUB > 1100
01IA             DISPLAY '******************************'
01IA             DISPLAY 'TABLE 041 OVERFLOW !!!!!!!!!!!'
01IA             DISPLAY 'VC-SUB = ' VC-SUB
01IA             DISPLAY '******************************'
01IA             GO TO Z000-ABEND-RTN.

            MOVE TAB-RANGE-FROM TO ORGN-FROM (VC-SUB).
            MOVE TAB-RANGE-TO   TO ORGN-TO (VC-SUB).
            MOVE TAB-CLASS-CODE TO VC-DIS-CODE (VC-SUB).
            ADD 1 TO VC-SUB.

       D000-PROCESS-BUDGET-MASTER.

            IF MST-HEAD-REC
                PERFORM D100-PROCESS-BUDHDR-DATA.

            IF MST-INITIAL-REC
                IF BUDINT-DATA-MOVED-ONLY
                    PERFORM F800-WRITE-BUDMST-REC
                    IF MST-KEY = W-PREV-BUDINT-KEY
                        DISPLAY 'DUPLICATE BUDINT RECORD: '
                                'PREV MST-KEY = ' W-PREV-BUDINT-KEY
                                ' MST-KEY = ' MST-KEY
                    ELSE
                        PERFORM D200-PROCESS-BUDINT-DATA
                ELSE
                    IF MST-KEY = W-PREV-BUDINT-KEY
                        DISPLAY 'DUPLICATE BUDINT RECORD: '
                                'PREV MST-KEY = ' W-PREV-BUDINT-KEY
                                ' MST-KEY = ' MST-KEY
                    ELSE
                        PERFORM D200-PROCESS-BUDINT-DATA.

            IF MST-TRANS-REC
                IF TRANSACTION-BUD-MST-REC = W-PREV-BUDXTN-REC
                    DISPLAY 'DUPLICATE BUDXTN RECORD; '
                            'DON"T FORGET - DUPLICATES ARE ALLOWED!:'
                            'PREV TRANS-REC = ' W-PREV-BUDXTN-REC
                            ' CURR TRANS-REC = '
                                              TRANSACTION-BUD-MST-REC
                    PERFORM D300-PROCESS-BUDXTN-DATA
                    PERFORM F700-WRITE-BUDXTN-REC
                ELSE
                    PERFORM D300-PROCESS-BUDXTN-DATA
                    PERFORM F700-WRITE-BUDXTN-REC.

            IF MST-SUM-REC
                IF MST-SUM-KEY = W-BUDINT-KEY-HOLD
                    PERFORM D400-PROCESS-BUDSUM-DATA
                    PERFORM F800-WRITE-BUDMST-REC
                ELSE
                    DISPLAY 'NO MATCHING BUDINT RECORD: '
                            'PREV MST-KEY = ' W-BUDINT-KEY-HOLD
                            ' MST-SUM-KEY = ' MST-SUM-KEY.

            PERFORM F400-READ-BUDMSTI-REC.

       D000-EXIT.
            EXIT.


       D100-PROCESS-BUDHDR-DATA.

            MOVE  MST-CYCLE-NO  TO W-CYCLE-NO.

            COMPUTE  BUDHDR-CNTR  =  BUDHDR-CNTR + 1.


       D200-PROCESS-BUDINT-DATA.

            MOVE  0                        TO BUDMST-DATA-MOVED-FLAG.

            MOVE  PRM-FISCAL-YEAR          TO W-ID-FISCYR.
            COMPUTE  W-ID-CALC-NO  =  W-ID-CALC-NO  +  1.
            MOVE  W-ID                     TO BUDMST-ID.

DMW001      MOVE  MST-SAU                  TO BM-SAU-CODE.
            MOVE  MST-SUB-CAMPUS-ID        TO BM-SUB-CAMPUS.
            MOVE  MST-TYPE                 TO BM-RECORD-TYPE.
            MOVE  MST-ACCOUNT-NO           TO BM-ACCOUNT.
            MOVE  MST-FUND-NO              TO BM-FUND.
      *  THE FOLLOWING FIELD BELOW CAN BE SPACES
            MOVE  MST-SUB-BUDGET-CD        TO BM-SUB-BUDGET.

            MOVE  W-CYCLE-NO               TO BM-CYCLE-NO.

            MOVE  MST-ATT-SUB-CAMPUS-ID    TO BM-ATTRB-SUB-CAMPUS.
      *  THE FOLLOWING FIELD BELOW CAN BE SPACES
            MOVE  MST-ATT-FUND-NO          TO BM-ATTRB-FUND.
            MOVE  MST-FUND-TYPE            TO BM-ATTRB-FUND-TYPE.
      *  THE FOLLOWING FIELD BELOW CAN BE SPACES
            MOVE  MST-ATT-SUB-BUDGET-CD    TO BM-ATTRB-SUB-BUDGET.

            MOVE  MST-INIT-AMT             TO BM-INIT-AMOUNT.
            MOVE  MST-PREV-YR-ADJ-AMT      TO BM-PREV-YR-ADJTD-AMT.

            MOVE  MST-INIT-FTE             TO BM-INIT-FTE.
            MOVE  MST-PREV-YR-ADJ-FTE      TO BM-PREV-YR-ADJTD-FTE.

            MOVE  MST-ACCOUNT-TITLE        TO BM-ACCOUNT-TITLE.

            MOVE  MST-INIT-IFIS-INDEX      TO BM-IFIS-INDEX.
            MOVE  MST-INIT-IFIS-FUND       TO BM-IFIS-FUND.
            MOVE  MST-INIT-IFIS-ORG        TO BM-IFIS-ORG.
            MOVE  MST-INIT-IFIS-ACCOUNT    TO BM-IFIS-ACCOUNT.
            MOVE  MST-INIT-IFIS-PROGRAM    TO BM-IFIS-PROGRAM.
      *  THE FOLLOWING TWO FIELDS BELOW CAN BE SPACES
      **    MOVE  MST-INIT-IFIS-ACTIVITY   TO BM-IFIS-ACTIVITY.
            MOVE  MST-INIT-IFIS-LOCATION   TO BM-IFIS-LOCATION.


            MOVE  MST-IFIS-INDEX-TITLE     TO BM-IFIS-INDEX-TITLE.
            MOVE  MST-IFIS-FUND-TITLE      TO BM-IFIS-FUND-TITLE.
            MOVE  MST-IFIS-ORG-TITLE       TO BM-IFIS-ORG-TITLE.
            MOVE  MST-IFIS-ACCOUNT-TITLE   TO BM-IFIS-ACCOUNT-TITLE.
            MOVE  MST-IFIS-PROGRAM-TITLE   TO BM-IFIS-PROGRAM-TITLE.
      *  THE FOLLOWING TWO FIELDS BELOW CAN BE NULLS OR SPACES
      **    IF MST-IFIS-ACTIVITY-TITLE IS ALPHABETIC
      **        MOVE  MST-IFIS-ACTIVITY-TITLE
      **                                   TO BM-IFIS-ACTIVITY-TITLE
      **    ELSE
      **        MOVE  SPACES               TO BM-IFIS-ACTIVITY-TITLE.

            IF MST-IFIS-LOCATION-TITLE IS ALPHABETIC
                MOVE  MST-IFIS-LOCATION-TITLE
                                           TO BM-IFIS-LOCATION-TITLE
            ELSE
                MOVE  SPACES               TO BM-IFIS-LOCATION-TITLE.

      *  NOTE: THIS FIELD BELOW IS FILLED IN BY SLPGB700 (WHICH
      *        SHOULD RUN IMMEDIATELY AFTER THIS PROGRAM)
      *
      *     MOVE  SPACES                   TO
      *                                    BM-VICE-CHNCLLR-DISTR-CODE.

            MOVE 'N' TO VC-TABLE-MATCH-FLAG.
            PERFORM D210-GET-VC-DISTRIBUTION-CODE THRU D210-EXIT
                   VARYING VC-SUB FROM 1 BY 1
DEVMLJ             UNTIL VC-SUB > 1100 OR
                         VC-TABLE-MATCH.

            IF VC-TABLE-MATCH-FLAG = 'N'
               DISPLAY 'ACCOUNT NUMBER NOT IN DISTRIBUTION FILE'
               DISPLAY 'ACCOUNT NUMBER = ' MST-ACCOUNT-NO
               MOVE SPACES        TO BM-VICE-CHNCLLR-DISTR-CODE.

            MOVE  PRM-FISCAL-YEAR          TO BM-FISCAL-YEAR.
            MOVE  W-SYS-DATE-TIME-HOLD     TO BM-REFRESH-DATE-TIME.

            MOVE  MST-KEY                  TO W-BUDINT-KEY-HOLD,
                                              W-PREV-BUDINT-KEY.

            COMPUTE  BUDINT-CNTR  =  BUDINT-CNTR + 1.

            MOVE  1                        TO BUDMST-DATA-MOVED-FLAG.


       D210-GET-VC-DISTRIBUTION-CODE.

           IF MST-ACCOUNT-NO NOT < ORGN-FROM (VC-SUB) AND
              MST-ACCOUNT-NO NOT > ORGN-TO   (VC-SUB)
                MOVE 'Y' TO VC-TABLE-MATCH-FLAG
                MOVE VC-DIS-CODE (VC-SUB)
                      TO BM-VICE-CHNCLLR-DISTR-CODE.

       D210-EXIT.
           EXIT.

       D300-PROCESS-BUDXTN-DATA.

            MOVE  W-ID                     TO BUDXTN-ID.

            MOVE  MST-TRANS-DOC-NO         TO BTR-DOCUMENT-NO.
            MOVE  MST-TRANS-DOC-SUF        TO BTR-DOCUMENT-SUFFIX.
            MOVE  MST-TRANS-CLASS          TO BTR-TRANSACTION-CLASS.
            MOVE  MST-TRANS-TYPE           TO BTR-TRANSACTION-TYPE.
            MOVE  MST-TRANS-AMT            TO BTR-DETAIL-AMOUNT.

            MOVE  MST-TRANS-FTE            TO BTR-DETAIL-FTE.
            MOVE  MST-TRANS-DESCR          TO BTR-DETAIL-TEXT.

            MOVE  MST-TRANS-DATE           TO W-DATE1.
            PERFORM G000-FORMAT-DATE.
            MOVE  W-DATE2                  TO BTR-TRANSACTION-DATE.

            MOVE  MST-POST-DATE            TO W-DATE1.
            PERFORM G000-FORMAT-DATE.
            MOVE  W-DATE2                  TO BTR-POSTING-DATE.

            MOVE  W-SYS-DATE-TIME-HOLD     TO BTR-REFRESH-DATE-TIME.

            MOVE  TRANSACTION-BUD-MST-REC  TO W-PREV-BUDXTN-REC.


       D400-PROCESS-BUDSUM-DATA.

            MOVE  MST-SUM-ADJ-AMT          TO BM-SUMM-ADJ-AMOUNT.
            MOVE  MST-SUM-ADJTD-AMT        TO BM-SUMM-ADJTD-AMOUNT.
            MOVE  MST-SUM-INCTD-AMT        TO BM-SUMM-INCRMTD-AMOUNT.

            MOVE  MST-SUM-ADJ-FTE          TO BM-SUMM-ADJ-FTE.
            MOVE  MST-SUM-ADJTD-FTE        TO BM-SUMM-ADJTD-FTE.
            MOVE  MST-SUM-INCTD-FTE        TO BM-SUMM-INCRMTD-FTE.

            COMPUTE  BUDSUM-CNTR  =  BUDSUM-CNTR + 1.

            MOVE  2                        TO BUDMST-DATA-MOVED-FLAG.
      /
       E000-EOJ.

            CLOSE  PRMCTL-FILE
                   BUDCTLI-FILE
                   BUDGET-TABLES-FILE
                   BUDMSTI-FILE
                   BUDCTLO-FILE
                   BUDSCTO-FILE
                   BUDSBTO-FILE
                   BUDVCTO-FILE
                   BUDMSTO-FILE
                   BUDXTN-FILE.

            MOVE  PRMCTL-CNTR   TO PRMCTL-ECNTR.
            MOVE  BUDCTLI-CNTR  TO BUDCTLI-ECNTR.
            MOVE  BUDTBLI-CNTR  TO BUDTBLI-ECNTR.
            MOVE  BUDMSTI-CNTR  TO BUDMSTI-ECNTR.
            MOVE  BUDTBL-CNTR   TO BUDTBL-ECNTR.
            MOVE  BUDHDR-CNTR   TO BUDHDR-ECNTR.
            MOVE  BUDINT-CNTR   TO BUDINT-ECNTR.
            MOVE  BUDSUM-CNTR   TO BUDSUM-ECNTR.
            MOVE  BUDCTLO-CNTR  TO BUDCTLO-ECNTR.
            MOVE  BUDSCTO-CNTR  TO BUDSCTO-ECNTR.
            MOVE  BUDSBTO-CNTR  TO BUDSBTO-ECNTR.
            MOVE  BUDVCTO-CNTR  TO BUDVCTO-ECNTR.
            MOVE  BUDMSTO-CNTR  TO BUDMSTO-ECNTR.
            MOVE  BUDXTN-CNTR   TO BUDXTN-ECNTR.

            DISPLAY  'INPUT: '.
            DISPLAY  '     PARAMETER CTRL REC COUNT : ' PRMCTL-ECNTR.
            DISPLAY  '     BUDGET CONTROL REC COUNT : ' BUDCTLI-ECNTR.
            DISPLAY  '     BUDGET TABLES REC COUNT  : ' BUDTBLI-ECNTR.
            DISPLAY  '     BUDGET MASTER REC COUNT  : ' BUDMSTI-ECNTR.
            DISPLAY  'PROCESSED: '.
            DISPLAY  '     BUDGET TABLES REC COUNT  : ' BUDTBL-ECNTR.
            DISPLAY  '     BUDGET HEADER REC COUNT  : ' BUDHDR-ECNTR.
            DISPLAY  '     BUDGET INITIAL REC COUNT : ' BUDINT-ECNTR.
            DISPLAY  '     BUDGET XACTN REC COUNT   : ' BUDXTN-ECNTR.
            DISPLAY  '     BUDGET SUMMARY REC COUNT : ' BUDSUM-ECNTR.
            DISPLAY  'OUTPUT: '.
            DISPLAY  '     BUDGET CONTROL REC COUNT : ' BUDCTLO-ECNTR.
            DISPLAY  '     BUDGET SUBCMPS REC COUNT : ' BUDSCTO-ECNTR.
            DISPLAY  '     BUDGET SUBBDGT REC COUNT : ' BUDSBTO-ECNTR.
            DISPLAY  '     BUDGET VC DIST REC COUNT : ' BUDVCTO-ECNTR.
            DISPLAY  '     BUDGET MASTER REC COUNT  : ' BUDMSTO-ECNTR.
            DISPLAY  '     BUDGET XACTN REC COUNT   : ' BUDXTN-ECNTR.
            DISPLAY  ' '.
            DISPLAY  'BSL700B NORMAL END OF JOB'.
      /
       F100-READ-PRMCTLI-REC.

            READ  PRMCTL-FILE   INTO PARAMETER-CONTROL-RECORD
                  AT END
                      MOVE  'Y'  TO  PRMCTLI-FLAG
                      GO TO  A100-EXIT.

            COMPUTE  PRMCTL-CNTR  =  PRMCTL-CNTR + 1.


       F200-READ-BUDCTLI-REC.

            READ  BUDCTLI-FILE  INTO SYSTEM-CONTROL-RECORD
                  AT END
                      MOVE  'Y'  TO  BUDCTLI-FLAG
                      GO TO  B000-EXIT.

            COMPUTE  BUDCTLI-CNTR  =  BUDCTLI-CNTR + 1.


       F300-READ-BUDTBLI-REC.

            READ  BUDGET-TABLES-FILE INTO BUDGET-TABLES-RECORD
                  AT END
                      MOVE  'Y'  TO  BUDTBLI-FLAG
                      GO TO  C000-EXIT.

            COMPUTE  BUDTBLI-CNTR  =  BUDTBLI-CNTR + 1.


       F400-READ-BUDMSTI-REC.

            READ  BUDMSTI-FILE  INTO BUDGET-MASTER-RECORD
                  AT END
                      MOVE  'Y'  TO  BUDMSTI-FLAG
                      GO TO  D000-EXIT.

            COMPUTE  BUDMSTI-CNTR  =  BUDMSTI-CNTR + 1.


       F500-WRITE-BUDCTLO-REC.

            WRITE  BUDCTLO-REC  FROM BUDGET-CONTROL-RECORD-OUT.

            COMPUTE  BUDCTLO-CNTR  =  BUDCTLO-CNTR + 1.


       F600-WRITE-BUDSCTO-REC.

            WRITE  BUDSCTO-REC  FROM SUB-CAMPUS-TABLE-RECORD.

            COMPUTE  BUDSCTO-CNTR  =  BUDSCTO-CNTR + 1.


       F610-WRITE-BUDSBTO-REC.

            WRITE  BUDSBTO-REC  FROM SUB-BUDGET-TABLE-RECORD.

            COMPUTE  BUDSBTO-CNTR  =  BUDSBTO-CNTR + 1.


       F620-WRITE-BUDVCTO-REC.

            WRITE  BUDVCTO-REC  FROM VC-DISTR-TABLE-RECORD.

            COMPUTE  BUDVCTO-CNTR  =  BUDVCTO-CNTR + 1.


       F700-WRITE-BUDXTN-REC.

            WRITE  BUDXTN-REC  FROM BUDGET-TRANSACTION-RECORD.

            COMPUTE  BUDXTN-CNTR  =  BUDXTN-CNTR + 1.


       F800-WRITE-BUDMST-REC.

            WRITE  BUDMSTO-REC  FROM BUDGET-MASTER-RECORD-OUT.

            COMPUTE  BUDMSTO-CNTR  =  BUDMSTO-CNTR + 1.
      /
       G000-FORMAT-DATE.

            MOVE  W-YY1  TO W-YY2.
            MOVE  W-MM1  TO W-MM2.
            MOVE  W-DD1  TO W-DD2.

            IF  W-YY2 > 80
                MOVE  19  TO W-CC2
            ELSE
                MOVE  20  TO W-CC2.


       G100-FORMAT-TIME.

            MOVE  W-HRS1  TO W-HRS2.
            MOVE  W-MIN1  TO W-MIN2.

            MOVE  ' '     TO W-T2-SPC.
            MOVE  ':'     TO W-T2-COLON.

       9000-BUDGET-TABLE-LOAD.         COPY CPPDXBTB.

      /
       Z000-ABEND-RTN.

            CLOSE  PRMCTL-FILE
                   BUDCTLI-FILE
                   BUDGET-TABLES-FILE
                   BUDMSTI-FILE
                   BUDCTLO-FILE
                   BUDSCTO-FILE
                   BUDSBTO-FILE
                   BUDVCTO-FILE
                   BUDMSTO-FILE
                   BUDXTN-FILE.

            DISPLAY  'BSL700B ABOUT TO ABEND (NON-IDMS)'.
            DISPLAY  '  '.
            DISPLAY  MISC-FIELDS.
            DISPLAY  FLAGS.

            MOVE  PRMCTL-CNTR   TO PRMCTL-ECNTR.
            MOVE  BUDCTLI-CNTR  TO BUDCTLI-ECNTR.
            MOVE  BUDTBLI-CNTR  TO BUDTBLI-ECNTR.
            MOVE  BUDMSTI-CNTR  TO BUDMSTI-ECNTR.
            MOVE  BUDTBL-CNTR   TO BUDTBL-ECNTR.
            MOVE  BUDHDR-CNTR   TO BUDHDR-ECNTR.
            MOVE  BUDINT-CNTR   TO BUDINT-ECNTR.
            MOVE  BUDSUM-CNTR   TO BUDSUM-ECNTR.
            MOVE  BUDCTLO-CNTR  TO BUDCTLO-ECNTR.
            MOVE  BUDSCTO-CNTR  TO BUDSCTO-ECNTR.
            MOVE  BUDSBTO-CNTR  TO BUDSBTO-ECNTR.
            MOVE  BUDVCTO-CNTR  TO BUDVCTO-ECNTR.
            MOVE  BUDMSTO-CNTR  TO BUDMSTO-ECNTR.
            MOVE  BUDXTN-CNTR   TO BUDXTN-ECNTR.
            DISPLAY  EDIT-COUNTERS.
