       IDENTIFICATION DIVISION.                                         00000100
       PROGRAM-ID.    FAPXX00.                                          00000110
       AUTHOR.        SARA TULLY .                                      00000120
       DATE-WRITTEN.  APR  2000.                                        00000130
       DATE-COMPILED.                                                   00000140
      ***************************************************************   00000590
      ** :UCSDLOCAL:  A LOCALLY MODIFIED PROGRAM                    *   00000600
      ***************************************************************   00000590
      *                                                                 00000180
      *                            ASSIGN TO PARMOUT.                   00000670
      *                            ASSIGN TO CONTROLR.                  00000700
      *                            ASSIGN TO ARCHECKI.                  00000700
      *                            ASSIGN TO ARCHECKO.                  00000700
      * PROGRAM CREATES A PARM RECORD TO BE PASSED ON TO TWO PROGRAMS
      * IF THE FIRST RECORD OF THE INPUT FILE HAS 'ADD TEXT' OR 'NO
      *  TEXT' A PARM RECORD IS CREATED STATING SUCH. WITH 'ADD TEXT'
      *  THE TEXT FOR THE CHECKS WILL FOLLOW.
      *    IF THERE IS NO PARM RECORD ATTACHED TO THE FRONT OF THE FILE
      * A 'NO TEXT' PARM RECORD IS ALSO WRITTEN (SO THERE WILL ALWAYS BE
      * ONE PARM RECORD).
      * THE REMAINING CHECK RECORDS ARE NOT CHANGED THEY ARE READ AND
      * WRITTEN AS THEY CAME IN.
      *                                                                 00000250
      *    INPUT :        ARCHECKI - CHECK FILE W/ HEADER RECORD STATING00000260
      *                            - IF TEXT S/BE ADDED OR NOT          00000270
      *    OUTPUT:        ARCHECKO - CHECK FILE W/OUT HEADER RECORD     00000260
      *                               ACTUAL TEXT TO BE ADDED TO CHECK  00000270
      *                   PARMOUT  -  ONE RECORD FILE W/ TEXT AND JOB   00000290
      *                               NAME ON IT OR (NO TEXT) ON REC    00000310
      *                   CONTROLR -  A REPORT DETAILING THE PROCESSING 00000320
      *                               STATISTICS FOR THE RUN OF THIS    00000330
      *                               EXTRACT PROGRAM.                  00000340
      *                                                                 00000350
      *    COPYLIB MEMBERS:                                             00000360
      *            WSLBLREC           LABEL RECORD LAYOUT               00000420
      *            WSCTLREC           STANDARD CONTROL REPORT HEADINGS  00000430
      *            WSHDREC            STANDARD PRINT REPORT HEADINGS    00000440
      *            WSCTRHED           HOLD AREAS FOR CENTERING HEADINGS 00000450
      *            CENTRHED           PROCEDURE TO CENTER HEADINGS      00000460
      ******************************************************************00000470
      *                    CHANGE HISTORY                              *00000480
      *                                                                *00000490
      * REF AUTH   DATE            DESCRIPTION                  CSR NO.*00000500
      * --- ---- -------- -----------------------------------  --------*00000510
      * 001 SLT  04/10/00 NEW PROGRAM                          SAR283
      ******************************************************************00000560
                                                                        00000570
       ENVIRONMENT DIVISION.                                            00000580
       CONFIGURATION SECTION.                                           00000590
       SPECIAL-NAMES.                                                   00000600
           C01 IS TOP-OF-PAGE.                                          00000610
       SOURCE-COMPUTER.                                                 00000620
       OBJECT-COMPUTER.                                                 00000630
       INPUT-OUTPUT SECTION.                                            00000640
       FILE-CONTROL.                                                    00000650
                                                                        00000660
           SELECT TEXT-FILE        ASSIGN TO PARMOUT.                   00000670
           SELECT CONTROL-FILE     ASSIGN TO CONTROLR.                  00000700
           SELECT AR-CHKS-FILE-IN  ASSIGN TO ARCHECKI.                  00000700
           SELECT AR-CHKS-FILE-OUT ASSIGN TO ARCHECKO.                  00000700
                                                                        00000730
       DATA DIVISION.                                                   00000770
       FILE SECTION.                                                    00000800
                                                                        00000940
      ****************************************************************  00001080
      *  OUTPUT FILE FOR EXTRACT DATA                                   00001090
      ****************************************************************  00001100
                                                                        00001110
                                                                        00001200
       FD  AR-CHKS-FILE-IN                                              00001120
           RECORDING MODE IS F                                          00001130
           LABEL RECORDS ARE OMITTED                                    00001140
           BLOCK CONTAINS 0 RECORDS                                     00001150
           RECORD CONTAINS 206 CHARACTERS                               00001160
           DATA RECORD IS AR-CHKS-RECORD-IN.                            00001170
                                                                        00001180
       01  AR-CHKS-RECORD-IN          PICTURE X(206).                   00001190
                                                                        00001200
      ****************************************************************  00001330
      *  OUTPUT FILE FOR CONTROL REPORT                                 00001340
      ****************************************************************  00001350
                                                                        00001360
       FD  AR-CHKS-FILE-OUT                                             00001120
           RECORDING MODE IS F                                          00001130
           LABEL RECORDS ARE OMITTED                                    00001140
           BLOCK CONTAINS 0 RECORDS                                     00001150
           RECORD CONTAINS 206 CHARACTERS                               00001160
           DATA RECORD IS AR-CHKS-RECORD-OUT.                           00001170
                                                                        00001180
       01  AR-CHKS-RECORD-OUT         PICTURE X(206).                   00001190

       FD  CONTROL-FILE                                                 00001370
           RECORDING MODE IS F                                          00001380
           LABEL RECORDS ARE OMITTED                                    00001390
           BLOCK CONTAINS 0 RECORDS                                     00001400
           RECORD CONTAINS 132 CHARACTERS                               00001410
           DATA RECORD IS CONTROL-RECORD.                               00001420
                                                                        00001430
       01  CONTROL-RECORD.                                              00001440
           03  CONTROL-DETAIL           PICTURE X(132).                 00001450
                                                                        00001460
      ****************************************************************  00001330
      *  OUTPUT FILE FOR SORTPARM RECORD FOR LATER SORT                 00001340
      ****************************************************************  00001350
                                                                        00001360
       FD  TEXT-FILE                                                    00001370
           RECORDING MODE IS F                                          00001380
           LABEL RECORDS ARE OMITTED                                    00001390
           BLOCK CONTAINS 0 RECORDS                                     00001400
           RECORD CONTAINS 080 CHARACTERS                               00001410
           DATA RECORD IS TEXT-RECORD.                                  00001420
                                                                        00001430
       01  TEXT-RECORD              PIC X(80).                          00001440
                                                                        00001460
      /                                                                 00001580
       WORKING-STORAGE SECTION.                                         00001590
       01  WS-PARMOUT-RECORD.                                           00001600
           03  WS-PARM1                PIC X(09).                       00001600
           03  WS-PARM-ORGN            PIC X(05).                       00001750
           03  WS-PARM-TEXT            PIC X(55).                       00001750
           03  PARM-JOB-NAME           PIC X(10).                       00001750

       01  WS-NO-TEXTS-RECORD          PIC X(80) VALUE 'NO TEXT'.
       01  WS-AR-CHKS-RECORD-IN.
           02  AR-PARM-REC.
             03  AR-PARM1              PIC X(09).
             03  AR-PARM-FILLER        PIC X(71).
           02 FILLER                   PIC X(126).

       01  SWITCHES.                                                    00001760
           03  EOF-FILE-SW              PICTURE X(001) VALUE 'N'.       00002360
               88  EOF-FILE                            VALUE 'Y'.       00002370
       COPY   WSHDREC.                                                  00002710
       COPY  WSCTRHED.                                                  00002710
       COPY  WSCTLREC.                                                  00002710
       COPY  WSLBLREC.                                                  00002710
       01  NUMERIC-EDIT                  PIC ZZZZ99.

       01  COUNTERS-ETC                  COMP-3.                        00003310
           03  AR-CHKS-WRITTEN-CTR       PIC 9(05) VALUE 0.
           03  AR-CHKS-READ-CTR          PIC 9(05) VALUE 0.
           03  AR-TEXT-HDR-CTR           PIC 9(05) VALUE 0.
                                                                        00003500
       01  HOLD-CONTROL-DETAIL      PICTURE X(132) VALUE SPACES.        00002980
       01  CONTROL-CC               PICTURE 9(001).                     00002740
       01  HOLD-TIME.                                                   00003590
           03  HOLD-TIME-HRS            PICTURE X(002) VALUE SPACES.    00003600
           03  HOLD-TIME-MINS           PICTURE X(002) VALUE SPACES.    00003610
           03  HOLD-COMM-DATE           PICTURE 9(008) VALUE ZERO.      00003100
           03  HOLD-COMM-DATE-R REDEFINES HOLD-COMM-DATE.               00003110
               05  HOLD-COMM-CC          PIC 9(002).                    00003120
               05  HOLD-COMM-DATE-6.                                    00003130
                   07  HOLD-COMM-YY      PIC 9(002).                    00003140
                   07  HOLD-COMM-MM      PIC 9(002).                    00003150
                   07  HOLD-COMM-DD      PIC 9(002).                    00003160
       01  WS-PARM-DATE.
           05  WS-PARM-CC                PIC XX.
           05  WS-PARM-YY                PIC XX.

       01  SAVE-PARM-REC-TYPE2           PIC XX.

      ****************************************************************  00002240
      *  IDMS/R DICTIONARY RECORDS USED BY PROGRAM                      00002340
      ****************************************************************  00002350
      /                                                                 00008760
       PROCEDURE DIVISION.                                              00008770
      ****************************************************************  00008780
      ****************************************************************  00008840
                                                                        00008850
       0000-CONTROL.                                                    00008860
                                                                        00008870
           DISPLAY '*** STARTING FAPXX00***'.                           00008880
                                                                        00008890
           PERFORM 1000-INITIALIZE     THRU 1000-EXIT.                  00008900

           MOVE ' '                 TO EOF-FILE-SW.
           PERFORM 2000-MAINLINE    THRU 2000-EXIT                      00008940
              UNTIL EOF-FILE.                                           00008950
           PERFORM 8000-WRITE-CONTROL-RPT.
           PERFORM 9000-END-OF-PROGRAM  THRU 9000-EXIT.                 00008970
                                                                        00008980
           DISPLAY '*** FINISHED FAPXX00***'.                           00008990
           STOP RUN.                                                    00009010
                                                                        00009020
      /                                                                 00009030
      ****************************************************************  00009040
      ****************************************************************  00009070
       1000-INITIALIZE.                                                 00009080
                                                                        00009100
                                                                        00009120
           OPEN INPUT                                                   00009170
                       AR-CHKS-FILE-IN.
           OPEN OUTPUT AR-CHKS-FILE-OUT                                 00009180
                       CONTROL-FILE                                     00009190
                       TEXT-FILE.
                                                                        00009220
           ACCEPT HOLD-COMM-DATE-6 FROM DATE.                           00009230
                                                                        00009240
           IF HOLD-COMM-DATE > 800000                                   00009250
               MOVE 19 TO HOLD-COMM-CC                                  00009260
           ELSE                                                         00009270
               MOVE 20 TO HOLD-COMM-CC.                                 00009280
                                                                        00009290
           DISPLAY 'TODAYS DATE: ' HOLD-COMM-DATE

            READ AR-CHKS-FILE-IN            INTO WS-AR-CHKS-RECORD-IN
               AT END
                  MOVE 'Y'                  TO EOF-FILE-SW.

            DISPLAY 'AR-PARM1 IS : ' AR-PARM1.

            IF AR-PARM1 = 'ADD TEXT' OR 'NO TEXT'
               MOVE AR-PARM-REC             TO WS-PARMOUT-RECORD
               ADD 1                        TO AR-TEXT-HDR-CTR
               WRITE TEXT-RECORD            FROM WS-PARMOUT-RECORD
            ELSE                                                        00016190
               WRITE TEXT-RECORD            FROM WS-NO-TEXTS-RECORD
               MOVE 0                       TO AR-TEXT-HDR-CTR
               WRITE AR-CHKS-RECORD-OUT     FROM WS-AR-CHKS-RECORD-IN
               ADD 1                        TO AR-CHKS-WRITTEN-CTR
               ADD 1                        TO AR-CHKS-READ-CTR.

            PERFORM 3000-READ-FILE          THRU 3000-EXIT.
                                                                        00023260
       1000-EXIT.  EXIT.                                                00009080
                                                                        00033210
       2000-MAINLINE.                                                   00033220
            WRITE AR-CHKS-RECORD-OUT        FROM WS-AR-CHKS-RECORD-IN
            ADD 1                           TO AR-CHKS-WRITTEN-CTR.
            PERFORM 3000-READ-FILE          THRU 3000-EXIT.             00033350
       2000-EXIT. EXIT.                                                 00033360
                                                                        00033380
       3000-READ-FILE.
            READ AR-CHKS-FILE-IN            INTO WS-AR-CHKS-RECORD-IN
               AT END
                  MOVE 'Y'                  TO EOF-FILE-SW
                  GO TO 3000-EXIT.
            ADD 1                           TO AR-CHKS-READ-CTR.

       3000-EXIT.   EXIT.

      ****************************************************************  00062260
      *  THE FOLLOWING PARAGRAPH WILL HANDLE END OF PROGRAM PROCESSING  00062270
      ****************************************************************  00062280
       8000-WRITE-CONTROL-RPT.                                          00059090
                                                                        00059150
           MOVE  HOLD-COMM-YY      TO WS-REPORT-DATE-YEAR.
           MOVE  HOLD-COMM-MM      TO WS-REPORT-DATE-MONTH.
           MOVE  HOLD-COMM-DD      TO WS-REPORT-DATE-DAY.
           MOVE WS-REPORT-DATE     TO WS-HED01-CUR-DTE.

           ACCEPT WS-ACCEPT-TIME FROM TIME.                             00028660
                                                                        00028670
           IF WS-ACCEPT-TIME-HRS > 12                                   00028680
               MOVE 'P' TO WS-HED01-AM-PM                               00028690
               SUBTRACT 12 FROM WS-ACCEPT-TIME-HRS                      00028700
               MOVE WS-ACCEPT-TIME-HRS TO WS-HED01-HRS                  00028710
           ELSE                                                         00028720
               MOVE 'A' TO WS-HED01-AM-PM                               00028730
               MOVE WS-ACCEPT-TIME-HRS TO WS-HED01-HRS.                 00028740
                                                                        00028750
           MOVE WS-ACCEPT-TIME-MINS                TO WS-HED01-MIN.     00028760
            MOVE '   STORING TEXT FOR EXTERNAL CHECKS    ' TO
                WS-HED02-RPT-TITLE.
           MOVE '*ISIS*'            TO WS-HOLD-HEADING-A.               00028810
           PERFORM 8999-CENTER-HEADING.                                 00028820
           MOVE WS-HOLD-HEADING-A TO WS-HED01-UNVRS-NAME.               00028830
           MOVE 'FAPXX00'          TO WS-HED01-PGM-ID.                  00059120
           MOVE WS-HEADING-LINE-01 TO CONTROL-DETAIL.                   00059130
           WRITE CONTROL-RECORD AFTER ADVANCING TOP-OF-PAGE.            00059140
           MOVE 1                  TO WS-HED02-TTL-PG-NO,               00059160
                                      WS-HED02-SCTN-PG-NO.              00059170
           MOVE 1                  TO WS-LINE-CTR.                      00059180
           MOVE 1                  TO CONTROL-CC.                       00059190
           MOVE WS-HEADING-LINE-02 TO CONTROL-DETAIL.                   00059200
           PERFORM 8050-PRINT-CONTROL-LINE.                             00059210
           MOVE PARM-JOB-NAME      TO WS-HEADING-LINE-03.               00059220
           MOVE WS-CNTRL-RPT-TITLE TO WS-HED03-RPT-TITLE.               00059230
           MOVE 1                  TO CONTROL-CC.                       00059240
           MOVE WS-HEADING-LINE-03 TO CONTROL-DETAIL.                   00059250
           PERFORM 8050-PRINT-CONTROL-LINE.                             00059260
                                                                        00059270
           MOVE AR-TEXT-HDR-CTR       TO NUMERIC-EDIT.
           MOVE SPACES                  TO CONTROL-DETAIL.
           STRING 'NUMBER OF CHECK HEADER TEXT RECORDS: '
              NUMERIC-EDIT
              DELIMITED BY SIZE
              INTO CONTROL-DETAIL.
           MOVE 3                  TO CONTROL-CC.
           PERFORM 8050-PRINT-CONTROL-LINE.

                                                                        00059270
           MOVE AR-CHKS-READ-CTR        TO NUMERIC-EDIT.
           MOVE SPACES                  TO CONTROL-DETAIL.
           STRING 'NUMBER OF CHECK RECORDS READ : '
              NUMERIC-EDIT
              DELIMITED BY SIZE
              INTO CONTROL-DETAIL.
           MOVE 3                  TO CONTROL-CC.
           PERFORM 8050-PRINT-CONTROL-LINE.
           MOVE AR-CHKS-WRITTEN-CTR     TO NUMERIC-EDIT.
           MOVE SPACES                  TO CONTROL-DETAIL.
           STRING 'NUMBER OF CHECK RECORDS WRITTEN: '
              NUMERIC-EDIT
              DELIMITED BY SIZE
              INTO CONTROL-DETAIL.
           MOVE 3                  TO CONTROL-CC.
           PERFORM 8050-PRINT-CONTROL-LINE.

           IF  AR-TEXT-HDR-CTR   = 0
              MOVE 'NO TEXT '           TO WS-PARM-TEXT.
           MOVE SPACES                  TO CONTROL-DETAIL.
           STRING 'ONE RECORD WRITTEN W/ TEXT INFO: '
              WS-PARM-TEXT
              DELIMITED BY SIZE
              INTO CONTROL-DETAIL.
           MOVE 3                  TO CONTROL-CC.
           PERFORM 8050-PRINT-CONTROL-LINE.

           MOVE SPACES                  TO CONTROL-DETAIL.
           STRING 'TEXT TO BE PRINTED ON CHECKS FOR THIS RUN: '
              WS-PARM-TEXT
              DELIMITED BY SIZE
              INTO CONTROL-DETAIL.
           MOVE 3                  TO CONTROL-CC.
           PERFORM 8050-PRINT-CONTROL-LINE.

           IF  SAVE-PARM-REC-TYPE2 = 'YN' OR 'YY'
              MOVE SPACES                  TO CONTROL-DETAIL
              STRING 'TEXT PRINTED ON CHECKS:  ',  WS-PARM-TEXT
                 DELIMITED BY SIZE
                 INTO CONTROL-DETAIL
              MOVE 3                  TO CONTROL-CC
              PERFORM 8050-PRINT-CONTROL-LINE.

           IF SAVE-PARM-REC-TYPE2 = 'NY'
              MOVE SPACES                  TO CONTROL-DETAIL
              STRING 'TEXT IS NOW SPACES - RESET FLAG FOR DESIRED TEXT'
                 DELIMITED BY SIZE
                 INTO CONTROL-DETAIL
              MOVE 3                  TO CONTROL-CC
              PERFORM 8050-PRINT-CONTROL-LINE.

           MOVE 1                  TO CONTROL-CC.                       00059240
           MOVE WS-HEADING-LINE-99 TO CONTROL-DETAIL.                   00059250
           PERFORM 8050-PRINT-CONTROL-LINE.                             00059260

       8000-EXIT.  EXIT.                                                00061380
       8050-PRINT-CONTROL-LINE.                                         00061390
                                                                        00061410
           IF WS-LINE-CTR > 60                                          00061420
               MOVE CONTROL-DETAIL TO HOLD-CONTROL-DETAIL               00061430
 *********     PERFORM 8100-REPEAT-CTL-HEADINGS                         00061440
               MOVE HOLD-CONTROL-DETAIL TO CONTROL-DETAIL.              00061450
                                                                        00061460
           WRITE CONTROL-RECORD AFTER ADVANCING CONTROL-CC.             00061470
           ADD CONTROL-CC TO WS-LINE-CTR.                               00061480
                                                                        00061490
       8050-EXIT.  EXIT.                                                00061500
                                                                        00061510
       9000-END-OF-PROGRAM.                                             00062300
                                                                        00062310
                                                                        00062320
           CLOSE                                                        00062330
                 TEXT-FILE,                                             00062340
                 AR-CHKS-FILE-IN                                        00062340
                 AR-CHKS-FILE-OUT                                       00062340
                 CONTROL-FILE.                                          00062350
                                                                        00062380
                                                                        00062410
                                                                        00062480
       9000-EXIT.                                                       00062490
           EXIT.                                                        00062500
                                                                        00062510
       COPY CENTRHED.                                                   00062290
