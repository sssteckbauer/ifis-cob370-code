       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UDCS399.
       AUTHOR.        SCOTT S. STECKBAUER (TIERRA SYSTEMS).
       DATE-WRITTEN.  JULY 1996.
       DATE-COMPILED.
      ******************************************************************
      ******************************************************************
      **                                                              **
      **  UDCS399....                                                 **
      **                                                              **
      **  THIS PROGRAMS DOES A TWO FILE MATCH.  THE OUTPUT IS A MIX   **
      **  OF BOTH FILES.  THE MATCH IS ON THE R-2117 RECORD'S DB-KEY. **
      **                                                              **
      **  THIS IS A GENERIC PROGRAM CAPABLE OF HANDLING THE OUTPUT OF **
      **  ANY NUMBER OF SWEEP PROGRAMS.                               **
      **                                                              **
      ******************************************************************
      **                                                              **
      **  INPUT:    GENERAL PERSON MATCH FILE                         **
      **            SWEEP FILE                                        **
      **                                                              **
      **  OUTPUT:   GENERAL PERSON SWEEP FILE                         **
      **                                                              **
      ******************************************************************
      **                                                              **
      **                   CHANGE HISTORY                             **
      **                                                              **
      **  REF  AUTH    DATE     DESCRIPTION                  CSR NO.  **
      **  ---  ----   ------   ---------------------------  --------- **
      **       SSS   07/04/96  ORIGINAL VERSION OF PGM.      CDS-012  **
      **                                                              **
      ******************************************************************
      ******************************************************************

           EJECT

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.
       OBJECT-COMPUTER.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT  SWEEP-FILE              ASSIGN  TO  SWEEP.

           SELECT  GEN-PERSON-FILE         ASSIGN  TO  GENPER.

           SELECT  GEN-PERSON-SWEEP-FILE   ASSIGN  TO  GPSWEEP.


       DATA DIVISION.

       FILE SECTION.

      ******************************************************************
      **   SWEEP  FILE                                                **
      ******************************************************************

       FD  SWEEP-FILE
           RECORDING  MODE     IS  F
           LABEL   RECORDS    ARE  STANDARD
           BLOCK   CONTAINS     0  RECORDS
           RECORD  CONTAINS   330  CHARACTERS.

       01  SWEEP-RECORD            PIC  X(330).


      ******************************************************************
      **   GENERAL PERSON FILE                                        **
      ******************************************************************

       FD  GEN-PERSON-FILE
           RECORDING  MODE     IS  F
           LABEL   RECORDS    ARE  STANDARD
           BLOCK   CONTAINS     0  RECORDS
           RECORD  CONTAINS   080  CHARACTERS.

       01  GEN-PERSON-RECORD       PIC  X(080).

           EJECT

      ******************************************************************
      **   GENERAL PERSON SWEEP FILE                                  **
      ******************************************************************

       FD  GEN-PERSON-SWEEP-FILE
           RECORDING  MODE     IS  F
           LABEL   RECORDS    ARE  STANDARD
           BLOCK   CONTAINS     0  RECORDS
           RECORD  CONTAINS   346  CHARACTERS.

       01  GEN-PERSON-SWEEP-RECORD     PIC  X(346).

           EJECT

      ******************************************************************
       WORKING-STORAGE SECTION.
      ******************************************************************

       01  WS-BEGINNING-DISPLAY.
           05  FILLER                         PIC  X(045)  VALUE
               '**  WORKING STORAGE BEGINS HERE            **'.

      ******************************************************************
      **   SWEEP RECORD LAYOUT                                        **
      ******************************************************************

       01  FILLER                             PIC  X(045)  VALUE
               '**  SWEEP RECORD BEGINS HERE               **'.

       01  SWEEP-REC.
           05  SWP-R2117-DB-KEY               PIC S9(008) COMP.
           05  SWP-REFRESH-DATE-TIME          PIC  X(014) VALUE SPACES.
           05  SWP-RECORD-TYPE                PIC  X(005) VALUE SPACES.
           05  SWP-INFO.
               10  SWP-DATA                   PIC  X(307) VALUE SPACES.

       01  FILLER                             PIC  X(045)  VALUE
               '**  SWEEP RECORD ENDS HERE                 **'.

           EJECT

      ******************************************************************
      **   GENERAL PERSON RECORD LAYOUT                               **
      ******************************************************************

       01  FILLER                             PIC  X(045)  VALUE
               '**  GENERAL PERSON RECORD BEGINS HERE      **'.

       01  GEN-PERSON-REC.
           05  GP-R2117-DB-KEY                PIC S9(08)  COMP.
           05  GP-INTRL-REF-ID-PKD            PIC S9(07)  COMP-3.
           05  GP-INTRL-REF-ID                PIC  9(07)  VALUE ZEROES.
           05  GP-PID                         PIC  X(09)  VALUE SPACES.
           05  FILLER                         PIC  X(56)  VALUE SPACES.

       01  FILLER                             PIC  X(045)  VALUE
               '**  GENERAL PERSON RECORD ENDS HERE        **'.

           EJECT

      ******************************************************************
      **   GENERAL PERSON SWEEP RECORD LAYOUT                         **
      ******************************************************************

       01  FILLER                             PIC  X(045)  VALUE
               '**  GENERAL PERSON SWEEP REC BEGINS HERE   **'.

       01  GEN-PERSON-SWEEP-REC.
           05  GS-INTRL-REF-ID-PKD            PIC S9(007) COMP-3.
           05  GS-INTRL-REF-ID                PIC  9(007) VALUE ZEROES.
           05  GS-PID                         PIC  X(009) VALUE SPACES.
           05  GS-REFRESH-DATE-TIME           PIC  X(014) VALUE SPACES.
           05  GS-INFO.
               10  GS-RECORD-TYPE             PIC  X(005) VALUE SPACES.
               10  GS-DATA                    PIC  X(307) VALUE SPACES.

       01  FILLER                             PIC  X(045)  VALUE
               '**  GENERAL PERSON SWEEP REC ENDS HERE     **'.

           EJECT

      ******************************************************************
      **   PROGRAM SWITCHES                                           **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  SWITCH WS AREA BEGINS HERE             **'.

       01  WS-SWITCHES.
           05  SW-NO-MORE-GP-RECS             PIC  X(01).
           05  SW-NO-MORE-SWEEP-RECS          PIC  X(01).

       01  FILLER                             PIC  X(45)  VALUE
               '**  SWITCH WS AREA ENDS HERE               **'.

      ******************************************************************
      **   PROGRAM COUNTERS                                           **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  COUNTER WS AREA BEGINS HERE            **'.

       01  WS-COUNTERS         COMP-3.
           05  CTR-GP-RECS-READ               PIC S9(07)     VALUE +0.
           05  CTR-SWEEP-RECS-READ            PIC S9(07)     VALUE +0.
           05  CTR-GP-SWEEP-RECS-WRTN         PIC S9(07)     VALUE +0.

       01  WS-COUNTERS-Z.
           05  CTR-GP-RECS-RESWP-Z            PIC  ZZZZZZZ9.
           05  CTR-SWEEP-RECS-RESWP-Z         PIC  ZZZZZZZ9.
           05  CTR-GP-SWEEP-RECS-WRTN-Z       PIC  ZZZZZZZ9.

       01  FILLER                             PIC  X(45)  VALUE
               '**  COUNTER WS AREA ENDS HERE              **'.

           EJECT

      ******************************************************************
      **   CURRENT TIME                                               **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT TIME WS AREA BEGINS HERE       **'.

       01  WS-TIME.
           05  WS-TIME-HRS                    PIC  9(02)  VALUE ZEROES.
           05  WS-TIME-FLDS.
               10  WS-TIME-MM                 PIC  9(02)  VALUE ZEROES.
               10  WS-TIME-SS                 PIC  9(02)  VALUE ZEROES.
               10  WS-TIME-HS                 PIC  9(02)  VALUE ZEROES.
           05  WS-TIME-FLDS-N  REDEFINES  WS-TIME-FLDS
                                              PIC  9(06).

       01  WS-TIME-EDIT.
           05  WS-TIME-HH-EDIT                PIC  9(02).
           05  FILLER                         PIC  X(01)  VALUE ':'.
           05  WS-TIME-MM-EDIT                PIC  9(02).

       01  WS-REFRESH-DATE-TIME.
           05  WS-REFRESH-DATE                PIC  9(08).
           05  FILLER                         PIC  X(01).
           05  WS-REFRESH-TIME                PIC  X(05).

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT TIME WS AREA ENDS HERE         **'.

      ******************************************************************
      **   CURRENT DATE                                               **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT DATE WS AREA BEGINS HERE       **'.

       01  WS-CURRENT-DATE.
           05  WS-CENTURY                     PIC  9(02)  VALUE ZEROES.
           05  WS-ACCEPT-DATE.
               10  WS-DATE-YEAR               PIC  9(02)  VALUE ZEROES.
               10  WS-DATE-MONTH              PIC  9(02)  VALUE ZEROES.
               10  WS-DATE-DAY                PIC  9(02)  VALUE ZEROES.

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT DATE WS AREA ENDS HERE         **'.

           EJECT


       01  WS-ENDING-DISPLAY.
           05  FILLER                         PIC  X(45)  VALUE
               '**  WORKING STORAGE ENDS HERE              **'.

           EJECT

      ******************************************************************
       PROCEDURE DIVISION.
      ******************************************************************

       MAIN-LINE.

           PERFORM  AA0000-INITIALIZE                THRU  AA0000-EXIT.

           PERFORM  DD0000-DRIVER                    THRU  DD0000-EXIT
               UNTIL  SW-NO-MORE-GP-RECS    =  'Y'  OR
                      SW-NO-MORE-SWEEP-RECS =  'Y'.

           PERFORM  ZZ0000-END-OF-PROGRAM            THRU  ZZ0000-EXIT.

           GOBACK.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES THE INITIALIZATION PROCESS...
      **   1) ACCEPT SYSTEM INFORMATION
      **   2) OPENING OF THE FILES
      **   3) INITIALIZING COUNTERS AND SWITCHES                       C
      **   4) PERFORM PARAGRAPHS FOR FURTHER INITIALIZATION            O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA0000-INITIALIZE.

           PERFORM  AA1000-DISP-INITS    THRU  AA1000-EXIT.

           PERFORM  AA2000-OPENS         THRU  AA2000-EXIT.

           PERFORM  AA3000-PRIME-READ    THRU  AA3000-EXIT.

       AA0000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES THE OPEING DISPLAYS AND INITIALIZES     C
      **   WORK FIELDS.                                                O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA1000-DISP-INITS.

           DISPLAY  '************************************************'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  BEGINNING PROGRAM UDCS399                 **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.

           ACCEPT  WS-ACCEPT-DATE     FROM  DATE.

           IF  WS-DATE-YEAR  >  90
               MOVE  19               TO  WS-CENTURY
           ELSE
               MOVE  20               TO  WS-CENTURY.

           DISPLAY  '**  CURRENT DATE IS -- '
                    WS-CURRENT-DATE
                    '               **'.
           DISPLAY  '**                                            **'.


           ACCEPT  WS-TIME            FROM  TIME.

           MOVE  WS-TIME-HRS          TO  WS-TIME-HH-EDIT.

           MOVE  WS-TIME-MM           TO  WS-TIME-MM-EDIT.


           MOVE  WS-CURRENT-DATE       TO  WS-REFRESH-DATE.

           MOVE  WS-TIME-EDIT          TO  WS-REFRESH-TIME.


           DISPLAY  '**  CURRENT TIME IS -- '
                    WS-TIME
                    '               **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**                                            **'.

       AA1000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   THIS PARAGRAPH DOES THE OPENS FOR THE FILES                 O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA2000-OPENS.

           OPEN  INPUT   SWEEP-FILE
                         GEN-PERSON-FILE.

           DISPLAY  '**  OPENED INPUT  -  SWEEP                    **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  OPENED INPUT  -  GENPER                   **'.
           DISPLAY  '**                                            **'.


           OPEN  OUTPUT  GEN-PERSON-SWEEP-FILE.

           DISPLAY  '**  OPENED OUTPUT -  GPSWEEP                  **'.
           DISPLAY  '**                                            **'.

       AA2000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   THIS PARAGRAPH DOES THE PRIME READ OF THE INPUT FILES.      O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA3000-PRIME-READ.

           PERFORM  IAA000-READ-SWEEP-FILE            THRU  IAA000-EXIT.

           PERFORM  IBB000-READ-GP-FILE               THRU  IBB000-EXIT.

       AA3000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   DRIVER...
      **   1) PERFORM INPUT RECORD PROCESSING                          C
      **   2) PERFORM READ NEXT INPUT RECORD                           O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       DD0000-DRIVER.

           IF  SWP-R2117-DB-KEY  =  GP-R2117-DB-KEY
               PERFORM  DD1000-MOVES                  THRU  DD1000-EXIT
               PERFORM  OAA000-WRITE-GP-SWEEP-REC     THRU  OAA000-EXIT
               PERFORM  IAA000-READ-SWEEP-FILE        THRU  IAA000-EXIT
           ELSE
               IF  SWP-R2117-DB-KEY  <  GP-R2117-DB-KEY
                   PERFORM  IAA000-READ-SWEEP-FILE    THRU  IAA000-EXIT
               ELSE
                   PERFORM  IBB000-READ-GP-FILE       THRU  IBB000-EXIT.

       DD0000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   MOVES THE FIELDS TO THE OUTPUT RECORD.                      O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       DD1000-MOVES.

           INITIALIZE  GEN-PERSON-SWEEP-REC.

           MOVE  GP-INTRL-REF-ID-PKD    TO  GS-INTRL-REF-ID-PKD.
           MOVE  GP-INTRL-REF-ID        TO  GS-INTRL-REF-ID.
           MOVE  GP-PID                 TO  GS-PID.

           MOVE  SWP-RECORD-TYPE        TO  GS-RECORD-TYPE.
           MOVE  SWP-DATA               TO  GS-DATA.
           MOVE  SWP-REFRESH-DATE-TIME  TO  GS-REFRESH-DATE-TIME.

       DD1000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   READ THE SWEEP FILE                                         O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       IAA000-READ-SWEEP-FILE.

           READ  SWEEP-FILE  INTO  SWEEP-REC
               AT  END
                   MOVE  'Y'  TO  SW-NO-MORE-SWEEP-RECS
                   GO  TO  IAA000-EXIT.

           ADD  +1  TO  CTR-SWEEP-RECS-READ.

       IAA000-EXIT.          EXIT.



      ******************************************************************
      **                                                               C
      **   READ THE GENERAL PERSON FILE                                O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       IBB000-READ-GP-FILE.

           READ  GEN-PERSON-FILE  INTO  GEN-PERSON-REC
               AT  END
                   MOVE  'Y'  TO  SW-NO-MORE-GP-RECS
                   GO  TO  IBB000-EXIT.

           ADD  +1  TO  CTR-GP-RECS-READ.

       IBB000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   WRITE THE GENERAL PERSON SWEEP RECORD.                      O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       OAA000-WRITE-GP-SWEEP-REC.

           WRITE  GEN-PERSON-SWEEP-RECORD  FROM  GEN-PERSON-SWEEP-REC.

           ADD  +1  TO  CTR-GP-SWEEP-RECS-WRTN.

       OAA000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES END OF PROGRAM FUNCTIONS....
      **   1) CLOSE THE FILES                                          C
      **   2) DISPLAY OF CONTROL COUNTS                                O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       ZZ0000-END-OF-PROGRAM.

           CLOSE  SWEEP-FILE
                  GEN-PERSON-FILE
                  GEN-PERSON-SWEEP-FILE.


           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  CLOSED INPUT  -  SWEEP                    **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  CLOSED INPUT  -  GENPER                   **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  CLOSED OUTPUT -  GPSWEEP                  **'.
           DISPLAY  '**                                            **'.

           MOVE  CTR-SWEEP-RECS-READ           TO
                 CTR-SWEEP-RECS-RESWP-Z.

           MOVE  CTR-GP-RECS-READ              TO
                 CTR-GP-RECS-RESWP-Z.

           MOVE  CTR-GP-SWEEP-RECS-WRTN        TO
                 CTR-GP-SWEEP-RECS-WRTN-Z.


           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------->>  COUNTS  <<---------------**'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  SWEEP RECORDS READ             ='
                                      CTR-SWEEP-RECS-RESWP-Z
                    '  **'.

           DISPLAY  '**                                            **'.
           DISPLAY  '**  GENERAL PERSON RECORDS READ    ='
                                      CTR-GP-RECS-RESWP-Z
                    '  **'.

           DISPLAY  '**                                            **'.
           DISPLAY  '**  GEN PERSON SWEEP RECS WRITTEN  ='
                                      CTR-GP-SWEEP-RECS-WRTN-Z
                    '  **'.


           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.


           DISPLAY  '**  SUCCESSFULLY ENDING PROGRAM UDCS399       **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '************************************************'.

       ZZ0000-EXIT.          EXIT.
