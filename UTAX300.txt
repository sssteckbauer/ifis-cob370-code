       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UTAX300.
       AUTHOR.        ADCOM SYSTEMS.
       DATE-WRITTEN.  FEB, 2000.
       DATE-COMPILED.

      *****************************************************************
      *               TRAVEL ADVANCE RECORD  LOADER
      *
      *  THE PROGRAM READS INPUT FILE FROM TRAVEL AGENCY.
      *  AND REFORMAT THE INPUT FILE
      *****************************************************************
      *
      * MM/DD/YY - XXX  XXXXX - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      *****************************************************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-4341.
       OBJECT-COMPUTER. IBM-4341.

       SPECIAL-NAMES.
           C01 IS TOP-OF-PAGE.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT TRAVEL-AGENCY-FILE-I ASSIGN TO AGENCYI.

           SELECT TRAVEL-AGENCY-FILE-O ASSIGN TO AGENCYO.



       DATA DIVISION.

       FILE SECTION.
       FD  TRAVEL-AGENCY-FILE-I
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 364 CHARACTERS
           DATA RECORD IS TRAVEL-AGENCY-REC-I.

       01  TRAVEL-AGENCY-REC-I.
           03  FD-RECORD-IND                   PIC X(01).
           03  FILLER                          PIC X(363).

       FD  TRAVEL-AGENCY-FILE-O
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 140 CHARACTERS
           DATA RECORD IS TRAVEL-AGENCY-RECORD-O.

       01  TRAVEL-AGENCY-RECORD-O               PIC X(140).

      /
       WORKING-STORAGE SECTION.

       01  WS-SWITCHES.
           03  WS-END-OF-FILE                   PIC X(01) VALUE ' '.
               88  END-OF-FILE                            VALUE 'Y'.


      ************* RESULTS OF REPORTS PROCESS ******************

       01  WS-DCMNT-HDR-REC-READ         PIC 9(06) VALUE ZEROES.
       01  WS-DCMNT-DTL-REC-READ         PIC 9(04) VALUE ZEROES.
       01  WS-DCMNT-ERR-REC-READ         PIC 9(04) VALUE ZEROES.
       01  WS-TOT-DCMNT-DTL-REC-READ     PIC 9(06) VALUE ZEROES.
       01  WS-DCMNT-REC-WRITTEN          PIC 9(06) VALUE ZEROES.

      ******************** HOLD FIELDS *********************************

       01  WS-ADVNC-NMBR.
           03 WS-ADVNC-FIRST-BYTE           PIC X(01).
           03 WS-ADVNC-SECOND-BYTE          PIC X(01).
           03 WS-ADVNC-REST-NMBR            PIC X(06).

       01  WS-SAVED-VALUES.
           03  WS-SAVED-AGENCY-ID            PIC X(09) VALUE SPACES.
           03  WS-SAVED-TOTAL-FARE-AMT       PIC 9(08)V99.
           03  WS-SAVED-TOTAL-SERV-FEE       PIC 9(08)V99.
           03  WS-SAVED-TOTAL-NMBR           PIC 9(10).
           03  WS-SAVED-VENDOR-NAME          PIC X(35).
           03  WS-TRANS-DATE.
               05  WS-TRANS-DATE-CC          PIC 9(02) VALUE ZEROES.
               05  WS-TRANS-DATE-YY          PIC 9(02) VALUE ZEROES.
               05  WS-TRANS-DATE-MM          PIC 9(02) VALUE ZEROES.
               05  WS-TRANS-DATE-DD          PIC 9(02) VALUE ZEROES.
           03  WS-TRANS-DATE-N REDEFINES WS-TRANS-DATE
                                             PIC 9(08).
           03  WS-HEADER-TRANS-DATE          PIC 9(08).
           03  WS-HEADER-TRANS-DATE-X
                REDEFINES WS-HEADER-TRANS-DATE.
               05  WS-HEADER-DATE-CC          PIC 9(02).
               05  WS-HEADER-DATE-YY          PIC 9(02).
               05  WS-HEADER-DATE-MM          PIC 9(02).
               05  WS-HEADER-DATE-DD          PIC 9(02).

           03  WS-SAVED-DCMNT-AMT            PIC S9(10)V99 VALUE ZEROES.
           03  WS-SAVED-SEQ-NMBR             PIC 9(01) VALUE ZEROES.

       01  WS-TOTAL-FARE-AMT.
           03  WS-TOTAL-FARE-DOLLAR          PIC 9(06).
           03  WS-TOTAL-FARE-CENTS           PIC 9(02).
       01  WS-TOTAL-FARE-AMT-X               PIC X(09) JUST RIGHT.

       01  WS-TOTAL-SERVICE-FEE.
           03  WS-TOTAL-SFEE-DOLLAR          PIC X(06).
           03  WS-TOTAL-SFEE-CENTS           PIC X(02).
       01  WS-TOTAL-SERVICE-FEE-X            PIC X(09) JUST RIGHT.

       01  WS-AIRFARE.
           03  WS-AIRFARE-DOLLAR             PIC X(06).
           03  WS-AIRFARE-CENTS              PIC X(02).
       01  WS-AIRFARE-X                      PIC X(09) JUST RIGHT.

       01  WS-SERVICE-FEE.
           03  WS-SFEE-DOLLAR                PIC X(06).
           03  WS-SFEE-CENTS                 PIC X(02).
       01  WS-SERVICE-FEE-X                  PIC X(09) JUST RIGHT.

       01  WS-WORK-AMT.
           03 WS-WORK-DOLLAR                 PIC 9(08).
           03 WS-WORK-CENTS                  PIC 9(02).
       01  WS-WORK-AMT-N REDEFINES WS-WORK-AMT
                                             PIC 9(10).
       01  WS-PASSENGER-NAME                 PIC X(132).
       01  WS-ROUTING                        PIC X(300).
       01  WS-REMAINING                      PIC X(200).
       01  WS-FSCL-YEAR                      PIC X(02).
       01  WS-SERVICE-TYPE-DESC              PIC X(20).

       01  WS-AGENCY-HDR-REC.                                           UTAU300I
               05  WS-REC-IND-HDR        PIC X(01).
               05  WS-FILE-TRANS-DATE    PIC X(10).
               05  WS-AGENCY-ID          PIC X(09).
               05  WS-TOTAL-NUMBER       PIC 9(05).
               05  WS-TOTAL-FARE-AMT-N   PIC 9(08).
               05  WS-TOTAL-SERVICE-FEE-N
                                           PIC 9(08).
       01  WS-AGENCY-DTL-REC.                                           UTAU300I
               05  WS-REC-IND-DTL        PIC X(01).
               05  WS-INVOICE-DATE       PIC X(10).
               05  WS-EVENT-NMBR         PIC X(08).
               05  WS-PASSENGER-NAME-X   PIC X(30).
               05  WS-VC                 PIC X(02).
               05  WS-DEPARTURE-DATE     PIC X(10).
               05  WS-ROUTING-X          PIC X(30).
               05  WS-D-OR-I             PIC X(01).
               05  WS-TICKET-NMBR        PIC X(10).
               05  WS-CC-TYPE            PIC X(02).
               05  WS-EKT-IND            PIC X(02).
               05  WS-AIRFARE-N          PIC X(08).
               05  WS-SERVICE-FEE-N      PIC X(08).
               05  WS-SERVICE-TYPE       PIC X(01).
               05  WS-POSTED-FLAG        PIC X(01).
               05  WS-POSTED-DATE        PIC X(08).
               05  WS-ADVNC-NMBR         PIC X(08).
      /
      *****************************************************************
      *  IDMS/R CONTROL RECORD AND DICTIONARY RECORDS USED BY PROGRAM
      *****************************************************************

      /
       PROCEDURE DIVISION.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL CONTROL THE THREE MAJOR PARTS
      *  OF THIS PROGRAM: *
      *    1) INITIALIZE OUTPUT FILES
      *    2) PROCESS THE DATABASE DATA
      *    3) PERFORM ANY END OF PROCESSING.
      ****************************************************************

       0000-MAINLINE SECTION.

           PERFORM 1000-INIT-PROCESS THRU 1000-EXIT.

           PERFORM 2000-MAIN-PROCESS THRU 2000-EXIT
              UNTIL END-OF-FILE.

           PERFORM 9000-END-OF-PROGRAM THRU 9000-EXIT.

           GOBACK.

      /
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL BIND RUN UNITS, READY THE
      *  NECESSARY AREAS, PREPARE THE PROGRAM TO ABORT DUE AN IDMS
      *  ERROR, OPEN THE FILES, INITIALIZE ARRAYS.
      ****************************************************************

       1000-INIT-PROCESS.

           OPEN INPUT  TRAVEL-AGENCY-FILE-I.
           OPEN OUTPUT
                       TRAVEL-AGENCY-FILE-O.

           PERFORM 1200-READ-TRANSACTION-FILE  THRU 1200-EXIT.

       1000-EXIT.
            EXIT.

       1200-READ-TRANSACTION-FILE.
           READ TRAVEL-AGENCY-FILE-I
              AT END
                 MOVE 'Y' TO WS-END-OF-FILE
                 GO TO 1200-EXIT.

           IF FD-RECORD-IND     = '1'
              ADD 1 TO WS-DCMNT-HDR-REC-READ.

           IF FD-RECORD-IND    = '2'
              ADD 1 TO WS-DCMNT-DTL-REC-READ
                       WS-TOT-DCMNT-DTL-REC-READ.

       1200-EXIT.
           EXIT.
       2000-MAIN-PROCESS.

           IF  FD-RECORD-IND = '1'
               PERFORM 3000-UNSTRING-HDR  THRU 3000-EXIT
           ELSE
               IF  FD-RECORD-IND = '2'
                     PERFORM 4000-UNSTRING-DTL THRU 4000-EXIT
               ELSE
                     ADD 1 TO WS-DCMNT-ERR-REC-READ.

           PERFORM 1200-READ-TRANSACTION-FILE THRU 1200-EXIT.

       2000-EXIT.
           EXIT.

      ****************************************************************
      *  THE FOLLOWING PARAGRAPH VALIDATES DOCUMENT HEADER RECORD
      *  ANY ERROR FROM HEADER IS WARNING
      ****************************************************************

       3000-UNSTRING-HDR.
            MOVE SPACES TO WS-AGENCY-HDR-REC.

            UNSTRING TRAVEL-AGENCY-REC-I DELIMITED BY ','
              INTO
                   WS-REC-IND-HDR
                   WS-FILE-TRANS-DATE
                   WS-AGENCY-ID
                   WS-TOTAL-NUMBER
                   WS-TOTAL-FARE-AMT-X
                   WS-TOTAL-SERVICE-FEE-X
                   ON OVERFLOW
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 3000-EXIT.

            MOVE ZEROS TO  WS-TOTAL-FARE-AMT.

            INSPECT WS-TOTAL-FARE-AMT-X CONVERTING ' ' TO '0'.
            UNSTRING WS-TOTAL-FARE-AMT-X DELIMITED BY '.'
              INTO
                   WS-TOTAL-FARE-DOLLAR
                   WS-TOTAL-FARE-CENTS
                   ON OVERFLOW
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 3000-EXIT.


           MOVE WS-TOTAL-FARE-AMT
                   TO WS-TOTAL-FARE-AMT-N.

           MOVE ZEROS TO  WS-TOTAL-SERVICE-FEE
           INSPECT WS-TOTAL-SERVICE-FEE-X CONVERTING ' ' TO '0'.
           UNSTRING WS-TOTAL-SERVICE-FEE-X DELIMITED BY '.'
              INTO
                   WS-TOTAL-SFEE-DOLLAR
                   WS-TOTAL-SFEE-CENTS
                   ON OVERFLOW
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 3000-EXIT.

            MOVE WS-TOTAL-SERVICE-FEE
                   TO WS-TOTAL-SERVICE-FEE-N.

            WRITE TRAVEL-AGENCY-RECORD-O FROM
                     WS-AGENCY-HDR-REC.
              ADD 1 TO WS-DCMNT-REC-WRITTEN.

       3000-EXIT.
            EXIT.

       4000-UNSTRING-DTL.
            UNSTRING TRAVEL-AGENCY-REC-I DELIMITED BY ','
              INTO
                   WS-REC-IND-DTL
                   WS-INVOICE-DATE
                   WS-EVENT-NMBR
                   WS-PASSENGER-NAME
                   WS-VC
                   WS-DEPARTURE-DATE
                   WS-ROUTING
                   WS-D-OR-I
                   WS-TICKET-NMBR
                   WS-CC-TYPE
                   WS-EKT-IND
                   WS-AIRFARE-X
                   WS-SERVICE-FEE-X
                   WS-SERVICE-TYPE
                   WS-REMAINING
                   ON OVERFLOW
                      DISPLAY 'ERROR EVENT# ' WS-EVENT-NMBR
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 4000-EXIT.

            MOVE ZEROS TO WS-AIRFARE.
            INSPECT  WS-AIRFARE-X  CONVERTING ' ' TO '0'.
            UNSTRING WS-AIRFARE-X  DELIMITED BY '.'
               INTO
                   WS-AIRFARE-DOLLAR
                   WS-AIRFARE-CENTS
                   ON OVERFLOW
                      DISPLAY 'ERROR EVENT# ' WS-EVENT-NMBR
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 4000-EXIT.

            MOVE WS-AIRFARE    TO WS-AIRFARE-N.

            MOVE ZEROS TO WS-SERVICE-FEE.
            INSPECT  WS-SERVICE-FEE-X  CONVERTING ' ' TO '0'.
            UNSTRING WS-SERVICE-FEE-X DELIMITED BY '.'
               INTO
                   WS-SFEE-DOLLAR
                   WS-SFEE-CENTS
                   ON OVERFLOW
                      ADD 1 TO WS-DCMNT-ERR-REC-READ
                      GO TO 4000-EXIT.

            MOVE WS-SERVICE-FEE  TO WS-SERVICE-FEE-N
            MOVE WS-PASSENGER-NAME TO WS-PASSENGER-NAME-X.
            MOVE WS-ROUTING        TO WS-ROUTING-X.

            WRITE TRAVEL-AGENCY-RECORD-O FROM
                     WS-AGENCY-DTL-REC.
            ADD 1 TO WS-DCMNT-REC-WRITTEN.
       4000-EXIT.
            EXIT.

       9000-END-OF-PROGRAM.

           DISPLAY 'TOTAL HEADER RECORDS READ..........: ',
                  WS-DCMNT-HDR-REC-READ
           DISPLAY ' ',

           DISPLAY 'TOTAL DETAIL RECORDS READ.....: ',
                  WS-DCMNT-DTL-REC-READ
           DISPLAY ' ',

           DISPLAY 'TOTAL ERROR RECORDS ....: ',
                  WS-DCMNT-ERR-REC-READ
           DISPLAY ' ',

           DISPLAY 'TOTAL SUCCESSFUL  RECORDS ....: ',
                  WS-DCMNT-REC-WRITTEN
           DISPLAY ' ',

           CLOSE TRAVEL-AGENCY-FILE-I
                 TRAVEL-AGENCY-FILE-O.

       9000-EXIT.
           EXIT.

