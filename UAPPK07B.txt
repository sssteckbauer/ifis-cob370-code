       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UAPPK07B.
       AUTHOR.        H.CARTWRIGHT - TIERRA SYSTEMS.
       DATE-WRITTEN.  MARCH 1992.
       DATE-COMPILED.
      *REMARKS.
      *    BATCH COBOL PRINT PROGRAM TO PRODUCE
      *            CHECKS THAT HAVE BEEN VOIDED
      *
      *    INPUT:
      *            UAPUK07O    : SORTED CHECK FILE
      *
      *    OUTPUT:
      *            PRINT FILE  : PRNTR01 - VOIDED CHECKS REPORT
      *
      /
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           C01 IS TOP-OF-PAGE.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINT-FILE     ASSIGN TO PRNTR01.
           SELECT READ-FILE      ASSIGN TO UAPUK07O.

      /
       DATA DIVISION.
       FILE SECTION.
      ****************************************************************
      *  FILE DEFINITION FOR PRINT-FILE
      ****************************************************************
       FD  PRINT-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 132 CHARACTERS
           DATA RECORD IS PRINT-RECORD.

       01  PRINT-RECORD.
           05  PRINT-DATA                  PIC X(132).

      ****************************************************************
      *  FILE DEFINITION FOR READ-FILE
      ****************************************************************
       FD  READ-FILE
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 285 CHARACTERS
           DATA RECORD IS UAPUK07O-RECORD.

      ******************************************************
      *  PRINT RECORD LAYOUT FOR UTILIZATION REPORT
      ******************************************************
       COPY UAPUK07O.
      /
       WORKING-STORAGE SECTION.

       COPY WSSTRTWS.
      ****************************************************************
      *                    SWITCHES
      ****************************************************************
       01  SWITCHES.
           03  TYPE-OF-DATA-FLAG            PIC X(001).
               88  BEG-OF-REPORT-DATA                 VALUE LOW-VALUE.
               88  VALID-DATA                         VALUE SPACE.
               88  END-OF-REPORT-DATA                 VALUE HIGH-VALUE.
           03  FIRST-TIME-SW                PIC X(01) VALUE 'Y'.
               88  FIRST-TIME                         VALUE 'Y'.
           03  WS-END-OF-INPUT              PIC X(01) VALUE 'N'.

      ****************************************************************
      *                    IO CONTROL AREA
      ****************************************************************
           COPY FIOCNTRL.
      ****************************************************************
      *                    HOLD AREAS
      ****************************************************************
       01  PRINT-LINE.
           03  PRN-SKIPX                       PIC 9(001) VALUE ZEROES.
           03  PRN-LINE1                       PIC X(132) VALUE SPACES.

       01  HOLD-AREAS.
           03  SAVE-PRINT-RECORD.
               05  FILLER                      PIC X(001) VALUE SPACES.
               05  FILLER                      PIC X(132) VALUE SPACES.
           03  SAVE-RUN-DATE                   PIC X(08)  VALUE SPACES.
           03  SAVE-RUN-TIME                   PIC X(04)  VALUE SPACES.
           03  WS-NUM-DATE                     PIC 9(08)  VALUE ZEROES.
           03  WS-NUM-DATE-R REDEFINES WS-NUM-DATE.
               05  WS-NUM-CC                   PIC XX.                        36
               05  WS-NUM-YY                   PIC XX.                        36
               05  WS-NUM-MM                   PIC XX.                        36
               05  WS-NUM-DD                   PIC XX.                        36
           03  SAVE-KEY.
               05  SAVE-CHECK-NMBR             PIC X(08) VALUE SPACES.
               05  SAVE-ORGN-CODE              PIC X(04) VALUE SPACES.
               05  SAVE-BANK-ACCT-CODE         PIC X(02) VALUE SPACES.
               05  WS-PREV-CHECK-NMBR          PIC X(08) VALUE SPACES.
           03  HOLD-FILE-CODE                  PIC X(04) VALUE 'XXP2'.
           03  HOLD-EOF-FLAG                   PIC X(01) VALUE SPACES.
           03  HOLD-BAD-WRITE-FLAG             PIC X(01) VALUE SPACES.

      ****************************************************************
      *               TOTALS AREA
      ****************************************************************

       01  WORK-TOTALS.
           03  WS-CHECK-TOT-AMT               PIC S9(10)V99 VALUE ZERO.
           03  WS-BANK-TOT-AMT                PIC S9(10)V99 VALUE ZERO.
           03  WS-ORGN-TOT-AMT                PIC S9(10)V99 VALUE ZERO.
           03  WS-CHECK-TOTAL                 PIC S9(10)V99 VALUE ZERO.
           03  WS-BANK-TOTAL                  PIC S9(10)V99 VALUE ZERO.
           03  WS-ORGN-TOTAL                  PIC S9(10)V99 VALUE ZERO.

      ****************************************************************
      *               COUNTERS AND SUBSCRIPTS
      ****************************************************************

       01  MISC-AREAS.
           03  WS-PAGE                         PIC 9(004) VALUE ZERO.
           03  LINE-CNT                        PIC 9(002) VALUE ZEROES.
           03  WS-PRN-SKIPX-NUM                PIC 9(001) VALUE ZERO.
       01  BLOWUP-PARMS.
           03  BAD-1-WS                        PIC 9(03) VALUE ZEROES.
           03  BAD-2-WS                        PIC 9(03) VALUE ZEROES.
           03  BAD-3-WS                        PIC 9(03) VALUE ZEROES.
       01  EDIT-DATE.                                                         36
           03  EDIT-YY                         PIC X(002).                    36
           03  EDIT-MM                         PIC X(002).                    36
           03  EDIT-DD                         PIC X(002).                    36
       01  EDITED-DATE.                                                       36
           03  EDITED-MM                       PIC X(002).                    36
           03  FILLER                          PIC X(001)  VALUE '/'.
           03  EDITED-DD                       PIC X(002).                    36
           03  FILLER                          PIC X(001)  VALUE '/'.
           03  EDITED-YY                       PIC X(002).                    36
       01  EDITED-TIME.                                                       36
           03  EDIT-HOURS                      PIC 9(002).                    36
           03  EDIT-MIN                        PIC 9(002).                    36

       01  CONSTANTS.                                                         36
           03  WS-CONSTANT-A                   PIC X(001) VALUE 'A'.          36
           03  WS-CONSTANT-B                   PIC X(001) VALUE 'B'.          36
           03  WS-CONSTANT-C                   PIC X(001) VALUE 'C'.          36
           03  WS-CONSTANT-N                   PIC X(001) VALUE 'N'.          36
           03  WS-CONSTANT-Y                   PIC X(001) VALUE 'Y'.          36
           03  WS-CONSTANT-1                   PIC X(001) VALUE '1'.          36
           03  WS-CONSTANT-2                   PIC X(001) VALUE '2'.          36
           03  WS-CONSTANT-3                   PIC X(001) VALUE '3'.          36
           03  WS-CONSTANT-4                   PIC X(001) VALUE '4'.          36
           03  WS-CONSTANT-5                   PIC X(001) VALUE '5'.          36
           03  WS-CONSTANT-6                   PIC X(001) VALUE '6'.          36
           03  WS-CONSTANT-7                   PIC X(001) VALUE '7'.          36
           03  WS-CONSTANT-8                   PIC X(001) VALUE '8'.          36
           03  WS-CONSTANT-9                   PIC X(001) VALUE '9'.          36
           03  WS-CONSTANT-BLANK               PIC X(001) VALUE '1'.          36
           03  WS-CONSTANT-PLUS                PIC X(001) VALUE '2'.          36
           03  WS-CONSTANT-DASH                PIC X(001) VALUE '3'.          36
           03  WS-CONSTANT-TOP-OF-PAGE         PIC X(001) VALUE 'T'.          36
           03  WS-CONSTANT-START               PIC X(006) VALUE 'START'.      36
           03  WS-CONSTANT-END                 PIC X(006) VALUE 'END '.       36
           03  WS-CONSTANT-OPEN                PIC X(006) VALUE 'OPEN'.       36
           03  WS-CONSTANT-READ                PIC X(006) VALUE 'READ'.       36
           03  WS-CONSTANT-WRITE               PIC X(006) VALUE 'WRITE'.      36
           03  WS-CONSTANT-CLOSE               PIC X(006) VALUE 'CLOSE'.      36
           03  WS-CONSTANT-MAX-LIN-CNT         PIC 9(002) VALUE 56.           36
           03  WS-CONSTANT-MAX-MINUS-2         PIC 9(002) VALUE 54.           36
           03  WS-CONSTANT-MAX-MINUS-5         PIC 9(002) VALUE 51.           36
           03  WS-CONSTANT-PROG-NAME           PIC X(008) VALUE
               'UAPPK07B'.
           03  WS-CONSTANT-PROG-START          PIC X(034) VALUE
               '*** PROGRAM:  UAPPK07B STARTED ***'.
           03  WS-CONSTANT-PROG-END            PIC X(034) VALUE
               '*** PROGRAM:  UAPPK07B ENDED   ***'.
           03  WS-CONSTANT-RPT-TITLE           PIC X(046) VALUE
               '                CHECKS OVER $50,000          '.

       01  WS-STANDARD-RPT-HEADING-1.
           02  SRH-HEADING-1.
           03  FILLER                      PIC X(01) VALUE SPACES.
           03  FILLER                      PIC X(15) VALUE
               'REPORT UAPPK07B'.
           03  FILLER                      PIC X(35) VALUE SPACES.
           03  FILLER                      PIC X(35) VALUE
               'UNIVERSITY OF CALIFORINA, SAN DIEGO'.
           03  FILLER                      PIC X(09) VALUE SPACES.
           03  FILLER                      PIC X(10) VALUE
               'RUN DATE: '.
           03  SRH1-RUN-DATE               PIC X(08) VALUE SPACES.
           03  FILLER                      PIC X(02) VALUE SPACES.
           03  FILLER                      PIC X(10) VALUE
               'RUN TIME: '.
           03  SHR1-RUN-TIME               PIC X(08) VALUE SPACES.
       01  WS-STANDARD-RPT-HEADING-2.
           02  SRH-HEADING-2.
           03  FILLER                      PIC X(01) VALUE SPACES.
           03  FILLER                      PIC X(45) VALUE SPACES.
           03  FILLER                      PIC X(35) VALUE
               '           VOIDED CHECKS REPORT    '.
           03  FILLER                      PIC X(15) VALUE SPACES.
           03  FILLER                      PIC X(23) VALUE SPACES.
           03  FILLER                      PIC X(07) VALUE
               'PAGE: '.
           03  SRH2-PAGE-NMBR              PIC Z(05).
           03  FILLER                      PIC X(04) VALUE SPACES.
       01  WS-HEADING-AS-OF-DATE.
           03  FILLER                      PIC X(59) VALUE SPACES.
           03  FILLER                      PIC X(06) VALUE
               'AS OF '.
           03  WS-PRN-AS-OF-DATE           PIC X(08) VALUE SPACES.
           03  FILLER                      PIC X(59) VALUE SPACES.
       01  WS-HEADING-3.
           03  FILLER                      PIC X(44) VALUE
               '   CHECK                    VENDOR/PAYEE    '.
           03  FILLER                      PIC X(44) VALUE
               '                              CHECK         '.
       01  WS-HEADING-4.
           03  FILLER                      PIC X(44) VALUE
               '   NUMBER    ORIGIN   NUMBER          NAME  '.
           03  FILLER                      PIC X(44) VALUE
               '                              AMOUNT        '.
       01  WS-HEADING-5.
           03  FILLER                      PIC X(44) VALUE
               '  --------   ------   ----------------------'.
           03  FILLER                      PIC X(44) VALUE
               '-----------------------   -------------     '.
      *******************************************************
      *  WS-HEADING-6 GET PRINTED B/4 CHECK TOTAL LINE PRINTS
      *******************************************************
       01  WS-HEADING-6.
           03  FILLER                      PIC X(088) VALUE SPACES.
           03  FILLER                      PIC X(44) VALUE
               '                              --------------'.
       01  WS-CHECK-TOTAL-LINE.
           03  FILLER                      PIC X(53) VALUE SPACES.
           03  FILLER                      PIC X(15) VALUE
               'CHECK TOTAL =  '.
           03  WS-TOT-CHECK-GROSS-AMT      PIC $$$,$$$,$$$.99-.
      ******************************************************
      *  WS-HEADING-7 GET PRINTED B/4 BANK TOTAL LINE PRINTS
      ******************************************************
       01  WS-HEADING-7.
           03  FILLER                      PIC X(68) VALUE SPACES.
           03  FILLER                      PIC X(18) VALUE
               '=================='.
       01  WS-BANK-TOTAL-LINE.
           03  FILLER                      PIC X(66) VALUE SPACES.
           03  FILLER                      PIC X(12) VALUE
               'BANK TOTAL  '.
           03  FILLER                      PIC X(35) VALUE SPACES.
           03  WS-TOT-BANK-GROSS-AMT       PIC $$$,$$$,$$$,$$$.99-.
       01  WS-ORGN-TOTAL-LINE.
           03  FILLER                      PIC X(66) VALUE SPACES.
           03  FILLER                      PIC X(12) VALUE
               'ORIGIN TOTAL'.
           03  FILLER                      PIC X(05) VALUE SPACES.
           03  WS-ORGN-CODE                PIC X(05) VALUE SPACES.
           03  FILLER                      PIC X(25) VALUE SPACES.
           03  WS-PRN-ORGN-GROSS-AMT       PIC $$$,$$$,$$$,$$$.99-.
       01  WS-PRN-DETAIL.                                                     99
           03  FILLER                      PIC X(02) VALUE SPACES.
           03  WS-PRN-CHK-NMBR             PIC X(08) VALUE SPACES.
           03  FILLER                      PIC X(04) VALUE SPACES.            99
           03  WS-PRN-ORIGIN-CODE          PIC X(04) VALUE SPACES.
           03  FILLER                      PIC X(04) VALUE SPACES.            99
           03  WS-PRN-VNDR-CODE            PIC X(09) VALUE SPACES.
           03  FILLER                      PIC X(01) VALUE SPACES.            99
           03  WS-PRN-VNDR-NAME            PIC X(35) VALUE SPACES.
           03  FILLER                      PIC X(04) VALUE SPACES.            99
           03  WS-PRN-CHECK-AMT            PIC $$$$$,$$$.99-.                 99
           COPY WSDONEWS.                                               01180010
       COPY WSLKDATA.
      /
       PROCEDURE DIVISION.

       1000-DRIVER.

           DISPLAY WS-CONSTANT-PROG-START.
           OPEN OUTPUT PRINT-FILE.
           OPEN INPUT  READ-FILE.

           IF FIRST-TIME
              ACCEPT WS-NUM-DATE   FROM DATE                                  36
              ACCEPT EDITED-TIME   FROM TIME                                  36
              MOVE WS-NUM-YY         TO EDITED-YY                             36
              MOVE WS-NUM-MM         TO EDITED-MM                             36
              MOVE WS-NUM-DD         TO EDITED-DD                             36
              MOVE EDITED-DATE       TO SAVE-RUN-DATE SRH1-RUN-DATE
                                        WS-PRN-AS-OF-DATE
              MOVE EDITED-TIME       TO SAVE-RUN-TIME SHR1-RUN-TIME
              PERFORM 7000-PRINT-HEAD-1-2-3-4-5.

           MOVE 'N' TO WS-END-OF-INPUT.
           READ READ-FILE AT END
               MOVE 'Y' TO WS-END-OF-INPUT.

           PERFORM 3000-MAINLINE  THRU  3000-EXIT
               UNTIL WS-END-OF-INPUT = 'Y'.

           MOVE SPACES                     TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9200-WRITE.

           MOVE WS-HEADING-7               TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9200-WRITE.

           MOVE WS-CHECK-TOTAL             TO WS-TOT-CHECK-GROSS-AMT.
           MOVE WS-CHECK-TOTAL-LINE        TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9200-WRITE.

           CLOSE       PRINT-FILE.
           CLOSE       READ-FILE.

           DISPLAY WS-CONSTANT-PROG-END.
           STOP RUN.

       1000-EXIT.
           EXIT.

       3000-MAINLINE.

           PERFORM 4100-PRINT-CHECK-RECORD.

           READ READ-FILE AT END
               MOVE 'Y' TO WS-END-OF-INPUT.

       3000-EXIT.
           EXIT.

      /
       4100-PRINT-CHECK-RECORD.

           ADD UAPUK07O-CHECK-AMT-B       TO WS-CHECK-TOTAL.

           MOVE SPACES                    TO WS-PRN-DETAIL.

           MOVE UAPUK07O-ORGN-CODE-B      TO WS-PRN-ORIGIN-CODE.
           MOVE UAPUK07O-CHECK-NMBR-B     TO WS-PRN-CHK-NMBR.
           MOVE UAPUK07O-VNDR-CODE-B      TO WS-PRN-VNDR-CODE.
           MOVE UAPUK07O-VNDR-NAME-B      TO WS-PRN-VNDR-NAME.
           MOVE UAPUK07O-CHECK-AMT-B      TO WS-PRN-CHECK-AMT.

           IF LINE-CNT GREATER THAN WS-CONSTANT-MAX-LIN-CNT                   36
              PERFORM 7000-PRINT-HEAD-1-2-3-4-5.

           MOVE WS-PRN-DETAIL             TO PRN-LINE1.
           MOVE 2                         TO PRN-SKIPX.
           PERFORM 9200-WRITE.

       4100-EXIT.
           EXIT.

       7000-PRINT-HEAD-1-2-3-4-5.

           MOVE SPACES                     TO PRN-LINE1.
           MOVE ZEROES                     TO LINE-CNT.
           MOVE SRH-HEADING-1              TO PRN-LINE1.
           PERFORM 9350-PRN-TOP-OF-PAGE.

           ADD 1                           TO WS-PAGE.
           MOVE WS-PAGE                    TO SRH2-PAGE-NMBR.
           MOVE SRH-HEADING-2              TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE WS-HEADING-AS-OF-DATE      TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE SPACES                     TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE WS-HEADING-3               TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE WS-HEADING-4               TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE WS-HEADING-5               TO PRN-LINE1.
           MOVE WS-CONSTANT-1              TO PRN-SKIPX.
           PERFORM 9300-PRN-LINE1.

           MOVE 8                          TO LINE-CNT.

       7000-EXIT.
           EXIT.

      /
       9200-WRITE.
           MOVE PRN-LINE1        TO PRINT-DATA.
           WRITE PRINT-RECORD AFTER ADVANCING PRN-SKIPX.
           ADD 1                 TO  LINE-CNT.

       9300-PRN-LINE1.
           MOVE PRN-LINE1        TO PRINT-DATA.
           WRITE PRINT-RECORD AFTER ADVANCING PRN-SKIPX.
           ADD 1                 TO  LINE-CNT.

       9350-PRN-TOP-OF-PAGE.
           MOVE PRN-LINE1        TO PRINT-DATA.
           WRITE PRINT-RECORD AFTER ADVANCING TOP-OF-PAGE.
           ADD 1                 TO  LINE-CNT.


