       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UUTX100.
       AUTHOR.        SCOTT S. STECKBAUER (TIERRA SYSTEMS).
       DATE-WRITTEN.  JANUARY 1996.
       DATE-COMPILED.
      ******************************************************************
      ******************************************************************
      **                                                              **
      **  UUTX100....                                                 **
      **                                                              **
      **  OBTAINS THE SYSTEM DATE AND TIME.  THEN IT READS A FLAT     **
      **  FILE AND MOVES THE DATE AND TIME TO THE END OF THE RECORD   **
      **  AND WRITES IT OUT.                                          **
      **                                                              **
      ******************************************************************
      **                                                              **
      **  INPUT:    GENERIC INPUT FILE                                **
      **                                                              **
      **  OUTPUT:   GENERIC OUTPUT FILE                               **
      **                                                              **
      ******************************************************************
      **                                                              **
      **                   CHANGE HISTORY                             **
      **                                                              **
      **  REF  AUTH    DATE     DESCRIPTION                  CSR NO.  **
      **  ---  ----   ------   ---------------------------  --------- **
      **       SSS   01/23/96  ORIGINAL VERSION OF PGM.               **
      **                                                              **
      ******************************************************************
      ******************************************************************
           EJECT
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.
       OBJECT-COMPUTER.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT  INPUT-FILE    ASSIGN  TO  INFILE.

           SELECT  OUTPUT-FILE   ASSIGN  TO  OUTFILE.


       DATA DIVISION.

       FILE SECTION.

      ******************************************************************
      **   INPUT FILE                                                 **
      ******************************************************************

       FD  INPUT-FILE
           RECORDING  MODE     IS  F
           LABEL   RECORDS    ARE  STANDARD
           BLOCK   CONTAINS     0  RECORDS
           RECORD  CONTAINS  1000  CHARACTERS.

       01  INPUT-RECORD      PIC  X(1000).


      ******************************************************************
      **   OUTPUT FILE                                                **
      ******************************************************************

       FD  OUTPUT-FILE
           RECORDING  MODE     IS  F
           LABEL   RECORDS    ARE  STANDARD
           BLOCK   CONTAINS     0  RECORDS
           RECORD  CONTAINS  1000  CHARACTERS.

       01  OUTPUT-RECORD     PIC  X(1000).

           EJECT

      ******************************************************************
       WORKING-STORAGE SECTION.
      ******************************************************************

       01  WS-BEGINNING-DISPLAY.
           05  FILLER                         PIC  X(045)  VALUE
               '**  WORKING STORAGE BEGINS HERE            **'.

      ******************************************************************
      **   INPUT RECORD LAYOUT                                        **
      ******************************************************************

       01  FILLER                             PIC  X(045)  VALUE
               '**  INPUT RECORD BEGINS HERE               **'.

       01  INPUT-REC.
           05  INPUT-BASE-REC                 PIC  X(986)  VALUE SPACES.
           05                 FILLER          PIC  X(014)  VALUE SPACES.


       01  FILLER                             PIC  X(045)  VALUE
               '**  INPUT RECORD ENDS HERE                 **'.

           EJECT

      ******************************************************************
      **   OUTPUT RECORD LAYOUT                                       **
      ******************************************************************

       01  FILLER                             PIC  X(045)  VALUE
               '**  OUTPUT RECORD BEGINS HERE              **'.

       01  OUTPUT-REC.
           05  OUTPUT-BASE-REC                PIC  X(986)  VALUE SPACES.
           05  OUTPUT-DATE                    PIC  X(008)  VALUE SPACES.
           05  FILLER                         PIC  X(001)  VALUE SPACES.
           05  OUTPUT-TIME                    PIC  X(005)  VALUE SPACES.

       01  FILLER                             PIC  X(045)  VALUE
               '**  OUTPUT RECORD ENDS HERE                **'.

           EJECT

      ******************************************************************
      **   PROGRAM SWITCHES                                           **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  SWITCH WS AREA BEGINS HERE             **'.

       01  WS-SWITCHES.
           05  SW-NO-MORE-INPUT-RECS          PIC  X(01).

       01  FILLER                             PIC  X(45)  VALUE
               '**  SWITCH WS AREA ENDS HERE               **'.

      ******************************************************************
      **   PROGRAM COUNTERS                                           **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  COUNTER WS AREA BEGINS HERE            **'.

       01  WS-COUNTERS         COMP-3.
           05  CTR-INPUT-RECS-READ            PIC S9(07)     VALUE +0.
           05  CTR-OUTPUT-RECS-WRTN           PIC S9(07)     VALUE +0.

       01  WS-COUNTERS-Z.
           05  CTR-INPUT-RECS-READ-Z          PIC  ZZZZZZZ9.
           05  CTR-OUTPUT-RECS-WRTN-Z         PIC  ZZZZZZZ9.

       01  FILLER                             PIC  X(45)  VALUE
               '**  COUNTER WS AREA ENDS HERE              **'.

           EJECT

      ******************************************************************
      **   CURRENT TIME                                               **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT TIME WS AREA BEGINS HERE       **'.

       01  WS-TIME.
           05  WS-TIME-HRS                    PIC  9(02)  VALUE ZEROES.
           05  WS-TIME-FLDS.
               10  WS-TIME-MM                 PIC  9(02)  VALUE ZEROES.
               10  WS-TIME-SS                 PIC  9(02)  VALUE ZEROES.
               10  WS-TIME-HS                 PIC  9(02)  VALUE ZEROES.
           05  WS-TIME-FLDS-N  REDEFINES  WS-TIME-FLDS
                                              PIC  9(06).

       01  WS-TIME-EDIT.
           05  WS-TIME-HH-EDIT                PIC  9(02).
           05  FILLER                         PIC  X(01)  VALUE ':'.
           05  WS-TIME-MM-EDIT                PIC  9(02).


       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT TIME WS AREA ENDS HERE         **'.

      ******************************************************************
      **   CURRENT DATE                                               **
      ******************************************************************

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT DATE WS AREA BEGINS HERE       **'.

       01  WS-CURRENT-DATE.
           05  WS-CENTURY                     PIC  9(02)  VALUE ZEROES.
           05  WS-ACCEPT-DATE.
               10  WS-DATE-YEAR               PIC  9(02)  VALUE ZEROES.
               10  WS-DATE-MONTH              PIC  9(02)  VALUE ZEROES.
               10  WS-DATE-DAY                PIC  9(02)  VALUE ZEROES.

       01  FILLER                             PIC  X(45)  VALUE
               '**  CURRENT DATE WS AREA ENDS HERE         **'.

           EJECT


       01  WS-ENDING-DISPLAY.
           05  FILLER                         PIC  X(45)  VALUE
               '**  WORKING STORAGE ENDS HERE              **'.

           EJECT

      ******************************************************************
       PROCEDURE DIVISION.
      ******************************************************************

       MAIN-LINE.

           PERFORM  AA0000-INITIALIZE                THRU  AA0000-EXIT.

           PERFORM  DD0000-DRIVER                    THRU  DD0000-EXIT
               UNTIL  SW-NO-MORE-INPUT-RECS  =  'Y'.

           PERFORM  ZZ0000-END-OF-PROGRAM            THRU  ZZ0000-EXIT.

           GOBACK.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES THE INITIALIZATION PROCESS...
      **   1) ACCEPT SYSTEM INFORMATION
      **   2) OPENING OF THE FILES
      **   3) INITIALIZING COUNTERS AND SWITCHES                       C
      **   4) PERFORM PARAGRAPHS FOR FURTHER INITIALIZATION            O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA0000-INITIALIZE.

           PERFORM  AA1000-DISP-INITS    THRU  AA1000-EXIT.

           PERFORM  AA2000-OPENS         THRU  AA2000-EXIT.

           PERFORM  AA3000-PRIME-READ    THRU  AA3000-EXIT.

       AA0000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES THE OPEING DISPLAYS AND INITIALIZES     C
      **   WORK FIELDS.                                                O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA1000-DISP-INITS.

           DISPLAY  '************************************************'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  BEGINNING PROGRAM UUTX100                 **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.

           ACCEPT  WS-ACCEPT-DATE     FROM  DATE.

           IF  WS-DATE-YEAR  >  90
               MOVE  19               TO  WS-CENTURY
           ELSE
               MOVE  20               TO  WS-CENTURY.

           DISPLAY  '**  CURRENT DATE IS -- '
                    WS-CURRENT-DATE
                    '               **'.
           DISPLAY  '**                                            **'.


           ACCEPT  WS-TIME            FROM  TIME.

           MOVE  WS-TIME-HRS          TO  WS-TIME-HH-EDIT.

           MOVE  WS-TIME-MM           TO  WS-TIME-MM-EDIT.


           DISPLAY  '**  CURRENT TIME IS -- '
                    WS-TIME
                    '               **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**                                            **'.

       AA1000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   THIS PARAGRAPH DOES THE OPENS FOR THE FILES                 O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA2000-OPENS.

           OPEN  INPUT   INPUT-FILE.

           DISPLAY  '**  OPENED INPUT  -  INPUT-FILE               **'.
           DISPLAY  '**                                            **'.


           OPEN  OUTPUT  OUTPUT-FILE.

           DISPLAY  '**  OPENED OUTPUT -  OUTPUT-FILE              **'.
           DISPLAY  '**                                            **'.

       AA2000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   THIS PARAGRAPH DOES THE PRIME READ OF THE INPUT FILE.       O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       AA3000-PRIME-READ.

           PERFORM  IAA000-READ-INPUT-FILE       THRU  IAA000-EXIT.

       AA3000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   DRIVER...
      **   1) PERFORM INPUT RECORD PROCESSING                          C
      **   2) PERFORM READ NEXT INPUT RECORD                           O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       DD0000-DRIVER.

           PERFORM  DD1000-PROCESS-INPUT-REC     THRU  DD1000-EXIT.

           PERFORM  IAA000-READ-INPUT-FILE       THRU  IAA000-EXIT.

       DD0000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   PROCESS THE INPUT RECORD                                    O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       DD1000-PROCESS-INPUT-REC.

           INITIALIZE  OUTPUT-REC.


           MOVE  INPUT-BASE-REC        TO  OUTPUT-BASE-REC.

           MOVE  WS-CURRENT-DATE       TO  OUTPUT-DATE.

           MOVE  WS-TIME-EDIT          TO  OUTPUT-TIME.

           PERFORM  OAA000-WRITE-OUTPUT-REC           THRU  OAA000-EXIT.

       DD1000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   READ THE INPUT FILE                                         O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       IAA000-READ-INPUT-FILE.

           READ  INPUT-FILE  INTO  INPUT-REC
               AT  END
                   MOVE  'Y'  TO  SW-NO-MORE-INPUT-RECS
                   GO  TO  IAA000-EXIT.

           ADD  +1  TO  CTR-INPUT-RECS-READ.

       IAA000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **                                                               C
      **   WRITE THE OUTPUT RECORD                                     O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       OAA000-WRITE-OUTPUT-REC.

           WRITE  OUTPUT-RECORD  FROM  OUTPUT-REC.

           ADD  +1  TO  CTR-OUTPUT-RECS-WRTN.

       OAA000-EXIT.          EXIT.

           EJECT

      ******************************************************************
      **
      **   THIS PARAGRAPH DOES END OF PROGRAM FUNCTIONS....
      **   1) CLOSE THE FILES                                          C
      **   2) DISPLAY OF CONTROL COUNTS                                O
      **                                                               L
      *A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-2

       ZZ0000-END-OF-PROGRAM.

           CLOSE  INPUT-FILE
                  OUTPUT-FILE.


           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  CLOSED INPUT  -  INPUT-FILE               **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  CLOSED OUTPUT -  OUTPUT-FILE              **'.
           DISPLAY  '**                                            **'.


           MOVE  CTR-INPUT-RECS-READ           TO
                 CTR-INPUT-RECS-READ-Z.

           MOVE  CTR-OUTPUT-RECS-WRTN          TO
                 CTR-OUTPUT-RECS-WRTN-Z.


           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------->>  COUNTS  <<---------------**'.
           DISPLAY  '**                                            **'.
           DISPLAY  '**  INPUT RECORDS READ             ='
                                      CTR-INPUT-RECS-READ-Z
                    '  **'.

           DISPLAY  '**                                            **'.
           DISPLAY  '**  OUTPUT RECORDS WRITTEN         ='
                                      CTR-OUTPUT-RECS-WRTN-Z
                    '  **'.


           DISPLAY  '**                                            **'.
           DISPLAY  '**--------------------------------------------**'.
           DISPLAY  '**                                            **'.


           DISPLAY  '**  SUCCESSFULLY ENDING PROGRAM UUTX100       **'.
           DISPLAY  '**                                            **'.
           DISPLAY  '************************************************'.

       ZZ0000-EXIT.          EXIT.
