       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UGAP540.
       DATE-WRITTEN.  DEC 1988.
       DATE-COMPILED.
       REMARKS.
           B A L T A P E    B A L A N C E R :
           THIS PROGRAM IS THE IBM VERSION OF THE BURROUGHS BALTAPE
           BALANCER PROGRAM. THIS PROGRAM IS EXECUTED AGAINST THE
           BALTAPE TAPE SENT TO SYSTEMWIDE. IT VERIFIES THE BALTAPE
           IS IN BALANCE. THE PROGRAM CODE HAS NOT BEEN CHANGED MUCH
           FROM THE BURROUGHS VERSION.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT  BALTAPE       ASSIGN TO UT-S-BALTAPE.
           SELECT  CONTROLREPORT ASSIGN TO UT-S-CONTROLR.
       DATA DIVISION.
       FILE SECTION.
       FD  BALTAPE
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS ARE OMITTED.
       01  BALTAPE-RCD                 PIC X(120).


       FD  CONTROLREPORT
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS CONTROL-REPORT-RECORD.
       01  CONTROL-REPORT-RECORD       PIC X(133).
       EJECT
       WORKING-STORAGE SECTION.
       01  WS-RECORD-COUNT                 PIC 9(08) VALUE 0.
       01  CONTROL-RECORD                  PIC X(70).
       01  BALTAPE-EXHAUSTED-FLAG          PIC 9(13)  VALUE 0.
           88 BALTAPE-EXHAUSTED       VALUE 1.
       77  ONFLAG                          PIC 9 VALUE 1.
       77  OFFLAG                          PIC 9 VALUE 0.

       01  CNTL-RPT-RECORD.
           03  CONTROL-REPORT-CC       PIC X(01).
           03  CONTROL-REPORT.
               05  FILLER              PIC X(32).
               05  APPROP-BALANCE      PIC ZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-.
               05  FILLER              PIC X(02).
               05  EXPEND-BALANCE      PIC ZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-.
               05  FILLER              PIC X(02).
               05  ENCUMB-BALANCE      PIC ZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-.
               05  FILLER              PIC X(02).
               05  CURRENT-DETAIL      PIC ZZ,ZZZ,ZZZ,ZZZ,ZZZ.99-.
               05  FILLER              PIC X(06).

       01  ACCOUNT-RANGE-TOTALS.
           03  ACCOUNT-LOCATIONS OCCURS 2 TIMES
               INDEXED BY RL.
           04  ACCOUNT-RANGES OCCURS 7 TIMES
               INDEXED BY RI, RT.
           05  ACCOUNT-TOTALS COMP.
               10  APPROP-BALANCE      PIC S9(13)V99.
               10  EXPEND-BALANCE      PIC S9(13)V99.
               10  ENCUMB-BALANCE      PIC S9(13)V99.
               10  CURRENT-DETAIL      PIC S9(13)V99.
               10  RECORD-COUNT        PIC 9(11).

       01  RECORD-TYPE-TOTALS.
           03  TYPE-LOCATIONS OCCURS 2 TIMES
               INDEXED BY TL.
           04  RECORD-TYPES OCCURS 9 TIMES
               INDEXED BY TI, TT.
           05  TYPE-TOTALS COMP.
               10  APPROP-BALANCE      PIC S9(13)V99.
               10  EXPEND-BALANCE      PIC S9(13)V99.
               10  ENCUMB-BALANCE      PIC S9(13)V99.
               10  CURRENT-DETAIL      PIC S9(13)V99.
               10  RECORD-COUNT        PIC 9(11).

       01  BALTAPE-RECORD.
           03  UC-LOCATION-1           PIC X(02).
           03  UC-LOCATION-2-ALPHA     PIC X(01).
           03  UC-LOCATION-2    REDEFINES UC-LOCATION-2-ALPHA
                                       PIC 9(01).
           03  ACCOUNT-ALPHA           PIC X(06).
           03  ACCOUNT          REDEFINES ACCOUNT-ALPHA
                                       PIC 9(06).
               88  PLANT-FUNDS-BALANCESHEET VALUE 0 THRU 101999.
               88  OTHER-BALANCESHEET VALUE 102000 THRU 199999.
               88  REVENUE-ACCOUNTS VALUE 200000 THRU 299999.
               88  EXPENDITURE-ACCOUNTS VALUE 300000 THRU 899999.
               88  PLANT-ACCOUNTS VALUE 900000 THRU 999997.
               88  ENCUMB-CONTROL-ACCOUNTS VALUE 999998.
           03  FILLER                  PIC X(01).
           03  FUND                    PIC X(05).
           03  FILLER                  PIC X(01).
           03  SUB                     PIC X(02).
           03  RECORDTYPE              PIC X(02).
           03  FILLER                  PIC X(20).
           03  APPROP-BALANCE          PIC S9(13)V99.
           03  EXPEND-BALANCE          PIC S9(13)V99.
           03  ENCUMB-BALANCE          PIC S9(13)V99.
           03  CURRENT-DETAIL          PIC S9(13)V99.
           03  FILLER                  PIC X(20).

       01  BALTAPE-HEADER REDEFINES BALTAPE-RECORD.
           03  FILLER                  PIC X(26).
           03  DESCRIPTION             PIC X(10).
               88  HEADER-RECORD VALUE 'BALANCE'.
           03  FILLER                  PIC X(84).

       01  PAD-RECORD REDEFINES BALTAPE-RECORD.
           05  PAD-RECORD-INDICATOR    PIC X(3).
               88  PADDING-RECORD      VALUE '999'.
           05  FILLER                  PIC X(117).

       01  CONTROL-REPORT-TITLE.
           03  FILLER                  PIC X(24)  VALUE
                                       'CORPORATE BALTAPE TOTALS'.
           03  FILLER                  PIC X(13)  VALUE SPACES.
           03  FILLER                  PIC X(14)  VALUE
                                       'APPROPRIATIONS'.
           03  FILLER                  PIC X(12)  VALUE SPACES.
           03  FILLER                  PIC X(12)  VALUE
                                       'EXPENDITURES'.
           03  FILLER                  PIC X(12)  VALUE SPACES.
           03  FILLER                  PIC X(12)  VALUE
                                       'ENCUMBRANCES'.
           03  FILLER                  PIC X(11)  VALUE SPACES.
           03  FILLER                  PIC X(13)  VALUE
                                       'CURRENT MONTH'.
           03  FILLER                  PIC X(09)  VALUE SPACES.

       01  CURRENT-TIME                PIC 9(06).
       01  CURRENT-TIME-REDF REDEFINES CURRENT-TIME.
           03  CURRENT-TIME-HH         PIC 9(02).
           03  CURRENT-TIME-MM         PIC 9(02).
           03  CURRENT-TIME-SS         PIC 9(02).

       EJECT
       PROCEDURE DIVISION.
       PROGRAM-FLOW.
           PERFORM OPEN-FILES.
           PERFORM BALTAPE-PROCESSING
              THRU BALTAPE-PROCESSING-EXIT.
           PERFORM PUT-BALANCE-REPORT
              THRU PUT-BALANCE-REPORT-EXIT.
           PERFORM CLOSE-FILES.
           STOP RUN.

       OPEN-FILES.
           OPEN    INPUT  BALTAPE
                   OUTPUT CONTROLREPORT.
           SET     RT TO 7.
           SET     TT TO 9.

      ******************************************************************
      *   INITIALIZE THE ARRAYS BEFORE USING THEM.                     *
      ******************************************************************
           SET RI TO 1.
           SET TI TO 1.
           PERFORM INIT-ACCOUNT-TOTALS
             UNTIL RI > 7.
           PERFORM INIT-TYPE-TOTALS
             UNTIL TI > 9.
       EJECT

       INIT-ACCOUNT-TOTALS.
           MOVE ZEROS TO  APPROP-BALANCE OF ACCOUNT-TOTALS (1, RI)
                          EXPEND-BALANCE OF ACCOUNT-TOTALS (1, RI)
                          ENCUMB-BALANCE OF ACCOUNT-TOTALS (1, RI)
                          CURRENT-DETAIL OF ACCOUNT-TOTALS (1, RI)
                          RECORD-COUNT   OF ACCOUNT-TOTALS (1, RI)
                          APPROP-BALANCE OF ACCOUNT-TOTALS (2, RI)
                          EXPEND-BALANCE OF ACCOUNT-TOTALS (2, RI)
                          ENCUMB-BALANCE OF ACCOUNT-TOTALS (2, RI)
                          CURRENT-DETAIL OF ACCOUNT-TOTALS (2, RI)
                          RECORD-COUNT   OF ACCOUNT-TOTALS (2, RI).
           SET RI UP BY  1.

       INIT-TYPE-TOTALS.
           MOVE ZEROS TO  APPROP-BALANCE OF TYPE-TOTALS (1, TI)
                          EXPEND-BALANCE OF TYPE-TOTALS (1, TI)
                          ENCUMB-BALANCE OF TYPE-TOTALS (1, TI)
                          CURRENT-DETAIL OF TYPE-TOTALS (1, TI)
                          RECORD-COUNT   OF TYPE-TOTALS (1, TI)
                          APPROP-BALANCE OF TYPE-TOTALS (2, TI)
                          EXPEND-BALANCE OF TYPE-TOTALS (2, TI)
                          ENCUMB-BALANCE OF TYPE-TOTALS (2, TI)
                          CURRENT-DETAIL OF TYPE-TOTALS (2, TI)
                          RECORD-COUNT   OF TYPE-TOTALS (2, TI).
           SET TI UP BY  1.

       CLOSE-FILES.
           CLOSE   CONTROLREPORT
                   BALTAPE.

       EJECT
       BALTAPE-PROCESSING.
           PERFORM GET-BALTAPE-RCD.
           IF      BALTAPE-EXHAUSTED
              GO TO BALTAPE-PROCESSING-EXIT.
           SET     RL TO UC-LOCATION-2.
           SET     TL TO UC-LOCATION-2.
           IF      PLANT-FUNDS-BALANCESHEET
               SET     RI TO 1.
           IF      OTHER-BALANCESHEET
               SET     RI TO 2.
           IF      REVENUE-ACCOUNTS
               SET     RI TO 3.
           IF      EXPENDITURE-ACCOUNTS
               SET     RI TO 4.
           IF      PLANT-ACCOUNTS
               SET     RI TO 5.
           IF      RECORDTYPE = 00
               SET     TI TO 1.
           IF      RECORDTYPE = 10
               SET     TI TO 2.
           IF      RECORDTYPE = 11
               SET     TI TO 3.
           IF      RECORDTYPE = 20
               SET     TI TO 4.
           IF      RECORDTYPE = 30
               IF      EXPENDITURE-ACCOUNTS
                   SET     TI TO 5
               ELSE
               IF      PLANT-ACCOUNTS
                   SET     TI TO 6
               ELSE
               IF      ENCUMB-CONTROL-ACCOUNTS
                   SET     TI TO 7.
           IF      RECORDTYPE = 31
               SET     TI TO 8.
           PERFORM ACCUM-BALTAPE-TOTALS.
           GO TO BALTAPE-PROCESSING.

       BALTAPE-PROCESSING-EXIT.
           EXIT.
       EJECT

       GET-BALTAPE-RCD.
           READ    BALTAPE INTO BALTAPE-RECORD
               AT END
                MOVE ONFLAG  TO BALTAPE-EXHAUSTED-FLAG.

           IF BALTAPE-EXHAUSTED-FLAG = ZERO
              IF HEADER-RECORD
                 GO TO GET-BALTAPE-RCD
              ELSE
                 PERFORM GET-BALTAPE-RCD-PROCESS.

       GET-BALTAPE-RCD-PROCESS.
           ADD     1 TO WS-RECORD-COUNT.
           IF      PADDING-RECORD
               MOVE    ONFLAG TO BALTAPE-EXHAUSTED-FLAG.


       EJECT
       ACCUM-BALTAPE-TOTALS.
      ******************************************************************
      * BEFORE ACCUMULATING AMOUNT TOTALS, EDIT ALL AMOUNT FIELDS.     *
      * CHANGE ANY NON-NUMERIC AMOUNT FIELDS TO ZEROS TO AVOID DATA    *
      * EXCEPTION ERRORS.                                              *
      ******************************************************************
           IF APPROP-BALANCE               OF BALTAPE-RECORD NOT NUMERIC
              MOVE ZEROS TO APPROP-BALANCE OF BALTAPE-RECORD.
           IF EXPEND-BALANCE               OF BALTAPE-RECORD NOT NUMERIC
              MOVE ZEROS TO EXPEND-BALANCE OF BALTAPE-RECORD.
           IF ENCUMB-BALANCE               OF BALTAPE-RECORD NOT NUMERIC
              MOVE ZEROS TO ENCUMB-BALANCE OF BALTAPE-RECORD.
           IF CURRENT-DETAIL               OF BALTAPE-RECORD NOT NUMERIC
              MOVE ZEROS TO CURRENT-DETAIL OF BALTAPE-RECORD.

           IF      RECORDTYPE = 00 OR 11 OR 31
               NEXT SENTENCE
           ELSE
               ADD CORRESPONDING
                   BALTAPE-RECORD TO
                   ACCOUNT-TOTALS(RL, RI)
               ADD 1 TO
                   RECORD-COUNT OF ACCOUNT-TOTALS(RL, RI)
               ADD CORRESPONDING
                   BALTAPE-RECORD TO
                   ACCOUNT-TOTALS(RL, RT)
               ADD 1 TO
                   RECORD-COUNT OF ACCOUNT-TOTALS(RL, RT)
               ADD     CORRESPONDING
                       BALTAPE-RECORD TO
                       TYPE-TOTALS(TL, TT)
               ADD     1 TO
                       RECORD-COUNT OF TYPE-TOTALS(TL, TT).

           ADD     CORRESPONDING
                   BALTAPE-RECORD TO
                   TYPE-TOTALS(TL, TI).

           ADD     1 TO
                   RECORD-COUNT OF TYPE-TOTALS(TL, TI).

       EJECT
       PUT-BALANCE-REPORT.
           MOVE '1'                     TO CONTROL-REPORT-CC.
           MOVE    CONTROL-REPORT-TITLE TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT 2 TIMES.
           MOVE    'BALANCESHEET ACCOUNTS' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '    PLANT FUND' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 1) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 1).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 1) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 1).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 1) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 1).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 1) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 1).

           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '    OTHER' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 2) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 2).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  SUBTOTAL' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 1) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 2) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 1) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 1) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 1) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 2) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 2).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 2) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 1) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 2) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 2).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT 2 TIMES.
           MOVE    'REVENUE ACCOUNTS' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 3) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 3).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 3) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 3).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 3) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 3).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 3) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 3).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'EXPENDITURE ACCOUNTS' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 4) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 4).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 4) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 4).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 4) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 4).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 4) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 4).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'PLANT EXPENDITURE ACCOUNTS' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 5) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 5).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 5) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 5).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 5) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 5).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 5) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 5).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'ENCUMBRANCE CONTROL ACCOUNTS' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 6) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 6).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 6) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 6).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 6) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 6).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 6) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 6).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT 2 TIMES.
           MOVE    'CAMPUS TOTAL' TO CONTROL-REPORT.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF ACCOUNT-TOTALS(1, 7) +
                   APPROP-BALANCE OF ACCOUNT-TOTALS(2, 7).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(1, 7) +
                   EXPEND-BALANCE OF ACCOUNT-TOTALS(2, 7).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(1, 7) +
                   ENCUMB-BALANCE OF ACCOUNT-TOTALS(2, 7).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(1, 7) +
                   CURRENT-DETAIL OF ACCOUNT-TOTALS(2, 7).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT 2 TIMES.
           MOVE    ALL '=' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'LOCAL' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           SET     TL TO 1.
           SET     TI TO 2.
           MOVE    '  BALANCE SHEET' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 4.
           MOVE    '  REVENUE' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 5.
           MOVE    '  EXPENDITURE' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 6.
           MOVE    '  PLANT ACCOUNTS' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 7.
           MOVE    '  ENCUMBRANCE CONTROL' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 9.
           MOVE    '    SUBTOTAL' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'UNIVERSITYWIDE' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           SET     TL TO 2.
           SET     TI TO 2.
           MOVE    '  BALANCE SHEET' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 4.
           MOVE    '  REVENUE' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 5.
           MOVE    '  EXPENDITURE' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 6.
           MOVE    '  PLANT ACCOUNTS' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 7.
           MOVE    '  ENCUMBRANCE CONTROL' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           SET     TI TO 9.
           MOVE    '    SUBTOTAL' TO CONTROL-REPORT.
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    'CAMPUS TOTAL' TO CONTROL-REPORT.
           PERFORM PUT-CAMPUS-TOTAL.

           MOVE    'OBJECT SUMMARY' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  LOCAL' TO CONTROL-REPORT.
           SET     TL TO 1.
           SET     TI TO 8.
           PERFORM FORMAT-REPORT.
           MOVE    '  UNIVERSITYWIDE' TO CONTROL-REPORT.
           SET     TL TO 2.
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  CAMPUS TOTAL' TO CONTROL-REPORT.
           PERFORM PUT-CAMPUS-TOTAL.
           MOVE    'FUND BALANCE ADJUSTMENTS' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  LOCAL' TO CONTROL-REPORT.
           SET     TL TO 1.
           SET     TI TO 3.
           MOVE    0 TO APPROP-BALANCE OF TYPE-TOTALS(TL, TI).
           PERFORM FORMAT-REPORT.
           MOVE    '  UNIVERSITYWIDE' TO CONTROL-REPORT.
           SET     TL TO 2.
           MOVE    0 TO APPROP-BALANCE OF TYPE-TOTALS(TL, TI).
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  CAMPUS TOTAL' TO CONTROL-REPORT.
           PERFORM PUT-CAMPUS-TOTAL.
           MOVE    'BALANCE FORWARD TOTALS' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  LOCAL' TO CONTROL-REPORT.
           SET     TL TO 1.
           SET     TI TO 1.
           PERFORM FORMAT-REPORT.
           MOVE    '  UNIVERSITYWIDE' TO CONTROL-REPORT.
           SET     TL TO 2.
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.
           MOVE    '  CAMPUS TOTAL' TO CONTROL-REPORT.
           PERFORM PUT-CAMPUS-TOTAL.

       PUT-BALANCE-REPORT-EXIT.
           EXIT.

       EJECT
       FORMAT-REPORT.
           MOVE    CORRESPONDING
                   TYPE-TOTALS (TL, TI) TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.

       PUT-CAMPUS-TOTAL.
           COMPUTE APPROP-BALANCE OF CONTROL-REPORT =
                   APPROP-BALANCE OF TYPE-TOTALS(1, TI) +
                   APPROP-BALANCE OF TYPE-TOTALS(2, TI).
           COMPUTE EXPEND-BALANCE OF CONTROL-REPORT =
                   EXPEND-BALANCE OF TYPE-TOTALS(1, TI) +
                   EXPEND-BALANCE OF TYPE-TOTALS(2, TI).
           COMPUTE ENCUMB-BALANCE OF CONTROL-REPORT =
                   ENCUMB-BALANCE OF TYPE-TOTALS(1, TI) +
                   ENCUMB-BALANCE OF TYPE-TOTALS(2, TI).
           COMPUTE CURRENT-DETAIL OF CONTROL-REPORT =
                   CURRENT-DETAIL OF TYPE-TOTALS(1, TI) +
                   CURRENT-DETAIL OF TYPE-TOTALS(2, TI).
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT 2 TIMES.
           MOVE    ALL '=' TO CONTROL-REPORT.
           PERFORM WRITE-CONTROLREPORT
              THRU WRITE-CONTROLREPORT-EXIT.

       WRITE-CONTROLREPORT.
           WRITE   CONTROL-REPORT-RECORD FROM CNTL-RPT-RECORD.
           MOVE    SPACES TO CNTL-RPT-RECORD.
           GO TO WRITE-CONTROLREPORT-EXIT.

       WRITE-CONTROLREPORT-EXIT.
           EXIT.
